package com.dev.codingpixel.i_launched.activity.home.side_menu;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.CompanyAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.static_function.Constants;

import java.util.ArrayList;

import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyScreenFragment extends Fragment implements CompanyAdapter.ItemClickListener {
    public static final String TAG = "CompanyScreenFragment";
    private RecyclerView profile_fragment_recycler;
    private BackInterface callBack;
    private ImageView not_found;
    private TopBarData topBarData;
    RelativeLayout edit_about;

    public CompanyScreenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_company, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        InitRecyclerView(view);
        Listener();
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            edit_about.setVisibility(View.VISIBLE);
        } else {
            edit_about.setVisibility(View.GONE);
        }
//        if (SharedPrefrences.getBool(Constants.IS_COMPANY_KEY, view.getContext())) {
//            edit_about.setVisibility(View.VISIBLE);
//        } else {
//            edit_about.setVisibility(View.GONE);
//        }
        setTopView();
    }

    private void Listener() {
        edit_about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomToast.ShowCustomToast(v.getContext(), "Workin on it!!", Gravity.CENTER);
            }
        });
    }

    public void setTopView() {
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_BACK_KEY, true);
            bundle.putBoolean(Constants.IS_TAG_KEY, true);
            bundle.putString(Constants.TAG_KEY, TAG);
            topBarData.setData("General Motors"
                    , "info@generalmotors.com \n+1 832 777 4774"
                    , true
                    , bundle);
        }
    }

    private void FindId(View view) {
        profile_fragment_recycler = view.findViewById(R.id.profile_fragment_recycler);
        edit_about = view.findViewById(R.id.edit_about);
    }

    public void InitRecyclerView(final View view) {
//        drawer_recyler = (RecyclerView) findViewById(R.id.i_launcher_side_me_recycle);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        profile_fragment_recycler.setLayoutManager(layoutManager);
        CompanyAdapter recyler_adapter = new CompanyAdapter(view.getContext(), profile_data_set());
        recyler_adapter.setClickListener(CompanyScreenFragment.this);
        profile_fragment_recycler.setAdapter(recyler_adapter);
    }

    private ArrayList<DataModel> profile_data_set() {
        ArrayList<DataModel> data = new ArrayList<>();
        data.add(new DataModel("Category Name",
                "Idea Heading Here",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
                "Abubaker Butt",
                "5min ago"));

        data.add(new DataModel("Category Name",
                "Idea Heading Here",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
                "Jawad Ahmad",
                "5min ago"));
        data.add(new DataModel("Category Name",
                "Idea Heading Here",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
                "Ali",
                "5min ago"));
        data.add(new DataModel("Category Name",
                "Idea Heading Here",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
                "Abubaker Butt",
                "5min ago"));
        data.add(new DataModel("Category Name",
                "Idea Heading Here",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.",
                "Abubaker Butt",
                "5min ago"));

        return data;
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }
}
