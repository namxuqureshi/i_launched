package com.dev.codingpixel.i_launched.activity.application;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.dev.codingpixel.i_launched.onesignal.OneSignalNotificationOpenedHandler;
import com.dev.codingpixel.i_launched.onesignal.OneSignalNotificationReceivedHandler;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class I_LaunchedApplication extends Application {
    private static Context context;

    public static Context getContext() {
        return context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Foreground.init(this);
        OneSignal.startInit(this)
                .filterOtherGCMReceivers(true)
                .autoPromptLocation(false) // default call promptLocation later
                .setNotificationReceivedHandler(new OneSignalNotificationReceivedHandler())
                .setNotificationOpenedHandler(new OneSignalNotificationOpenedHandler())

                .init();
        context = getApplicationContext();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
