package com.dev.codingpixel.i_launched.activity.home.bottom_fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.NDACompanyDialouge;
import com.dev.codingpixel.i_launched.adapters.HomeRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.CompanyDataModel;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.mark_as_ilaunched;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_IDEA_DETAIL_FRAGMENT;
import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeIdeaFragment extends Fragment implements HomeRecyclerAdapter.ItemClickListener, APIResponseListner, HomeRecyclerAdapter.ItemClickListenerPositionore, ILaunchedDialouge.OnDialogFragmentClickListener, NDACompanyDialouge.OnDialogFragmentClickListener {

    private RecyclerView home_fragment_recycler;
    private BackInterface callBack;
    private ImageView search_ic;
    private LinearLayout not_found;
    private TopBarData topBarData;
    RelativeLayout search_categories;
    Bundle bundleItem = new Bundle();
    EditText search_text_auto;
    ArrayList<DataModel> data = new ArrayList<>();
    HomeRecyclerAdapter recycler_adapter;
    TextView text;

    public HomeIdeaFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        InitRecyclerView(view);
        setTopView();
        Listener();
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            search_categories.setVisibility(View.VISIBLE);
        } else {
            search_categories.setVisibility(View.GONE);
        }

    }

    /**
     * Listener Set HERE
     */
    private void Listener() {
        search_text_auto.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    HideKeyboard((Activity) v.getContext());
                    callSearchIdeaByCategoryKeyWordApi(search_text_auto.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });
        search_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard((Activity) v.getContext());
                callSearchIdeaByCategoryKeyWordApi(search_text_auto.getText().toString().trim());
            }
        });
        search_text_auto.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (hasFocus) {
                    search_text_auto.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (s.toString().trim().length() == 0) {
                                HideKeyboard((Activity) v.getContext());
                                setTopView();
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * Set Top Views
     */
    public void setTopView() {
        if (topBarData != null) {
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
//                topBarData.setData("Favorite Ideas", "List of ideas you love", false, new Bundle());
                topBarData.setData("All Ideas", "Full list of ideas", false, new Bundle());
            } else {
                topBarData.setData("My Idea", "Full list of my ideas", false, new Bundle());
            }

        }
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            if (bundle.getBoolean(Constants.IS_SEARCH_CAT_KEY, false)) {
                callSearchIdeaByCategoryApi(bundle.getInt(Constants.ID_KEY));
            } else if (bundle.getBoolean(Constants.IS_ALL_IDEAS_KEY, false)) {
                callAllIdeasListApi("");
            } else if (bundle.getBoolean(Constants.IS_FAV_ONLY_KEY, false)) {
                callMyFaveroiteApi();
                topBarData.setData("Favorite Ideas", "List of ideas you love", false, new Bundle());
            } else if (bundle.getBoolean(Constants.IS_SEARCH_BY_KEYWORD_KEY, false)) {
                callSearchIdeaByCategoryKeyWordApi(bundle.getString(Constants.SEARCH_KEY, ""));
            } else {
                callMyCategoryApi();
            }
        } else {
            callMyCategoryApi();
        }

    }

    /**
     * Api Call For Faveroite For Company
     */
    private void callMyFaveroiteApi() {

        new VollyAPICall(getContext()
                , true
                , URL.my_favourite
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , HomeIdeaFragment.this, APIActions.ApiActions.my_favourite);

    }

    /**
     * @param search by text search data
     *               Call For Company Side
     */
    private void callAllIdeasListApi(String search) {
        try {
            new VollyAPICall(getContext()
                    , true
                    , URL.search_all_ideas
                    , new JSONObject().put("search", search)
                    , userLoggedIn.getSessionToken()
                    , POST
                    , HomeIdeaFragment.this, APIActions.ApiActions.search_all_ideas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        http://139.162.37.73/ilaunched/api/search_all_ideas
    }

    /**
     * Call for user only display user added ideas categories
     */
    void callMyCategoryApi() {
        new VollyAPICall(getContext()
                , true
                , URL.my_ideas
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , HomeIdeaFragment.this, APIActions.ApiActions.my_ideas);
    }

    /**
     * @param id var for passing id to which ideas will be displayed
     *           for company side
     */
    void callSearchIdeaByCategoryApi(int id) {
        new VollyAPICall(getContext()
                , true
                , URL.ideas_by_category_id + "/" + id
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , HomeIdeaFragment.this, APIActions.ApiActions.ideas_by_category_id);
    }

    void callSignNda(int id, int isNdaSign) {
        try {
            new VollyAPICall(getContext()
                    , false
                    , URL.mark_as_digital_sign
                    , new JSONObject().put("idea_id", id)
                    , userLoggedIn.getSessionToken()
                    , POST
                    , HomeIdeaFragment.this, APIActions.ApiActions.mark_as_digital_sign);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param keyword with search functionality to which ideas will be displayed
     *                for company side
     */
    void callSearchIdeaByCategoryKeyWordApi(String keyword) {
        try {
            HideKeyboard(getActivity());
            new VollyAPICall(getContext()
                    , true
                    , URL.search_by_category
                    , new JSONObject().put("search", keyword)
                    , userLoggedIn.getSessionToken()
                    , POST
                    , HomeIdeaFragment.this, APIActions.ApiActions.search_all_ideas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param view by getting and setting ID's
     */
    private void FindId(View view) {
        home_fragment_recycler = view.findViewById(R.id.home_fragment_recycler);
        search_categories = view.findViewById(R.id.search_categories);
        not_found = view.findViewById(R.id.not_found);
        text = view.findViewById(R.id.text);
        not_found.setVisibility(View.GONE);

        search_text_auto = view.findViewById(R.id.search_text_auto);
        search_ic = view.findViewById(R.id.search_ic);

    }

    /**
     * @param view setting recycler view
     */
    public void InitRecyclerView(final View view) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        home_fragment_recycler.setLayoutManager(layoutManager);
        recycler_adapter = new HomeRecyclerAdapter(view.getContext(), data);
        recycler_adapter.setClickListener(HomeIdeaFragment.this);
        recycler_adapter.setClickListenerPosition(HomeIdeaFragment.this);
        home_fragment_recycler.setAdapter(recycler_adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (userLoggedIn.getType() == Constants.TYPE_USER) {
            if (topBarData != null) {

                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    bundleItem.putBoolean(Constants.IS_FAV_KEY, true);
                } else {
                    bundleItem.putBoolean(Constants.IS_FAV_KEY, false);
                }
                DataModel e = data.get(position);
                bundleItem.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, e);
                topBarData.callTransaction(bundleItem, LOAD_IDEA_DETAIL_FRAGMENT);
            }
        } else {
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                bundleItem.putBoolean(Constants.IS_FAV_KEY, true);
            } else {
                bundleItem.putBoolean(Constants.IS_FAV_KEY, false);
            }
            DataModel e = data.get(position);
            bundleItem.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, e);
            if (e.getIsNdaSign() == 0) {
                NDACompanyDialouge ndaCompanyDialouge = NDACompanyDialouge.newInstance(HomeIdeaFragment.this, e.getUser_name(), e);
                ndaCompanyDialouge.show(getChildFragmentManager(), "");
            } else {
                if (topBarData != null) {
                    topBarData.callTransaction(bundleItem, LOAD_IDEA_DETAIL_FRAGMENT);
                }
            }
        }
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == APIActions.ApiActions.my_ideas || apiActions == APIActions.ApiActions.ideas_by_category_id || apiActions == APIActions.ApiActions.search_all_ideas || apiActions == APIActions.ApiActions.my_favourite) {
            try {
                JSONObject object = new JSONObject(response);
                data.clear();
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        JSONArray jsonArray = object.getJSONArray("successData");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            DataModel e = new DataModel();
                            e.setUserName(userLoggedIn.getUsername());
                            e.setCategoryTitle(jsonArray.getJSONObject(i).getJSONObject("get_category").getString("category_title"));
                            if (jsonArray.getJSONObject(i).isNull("get_used_sub_category")) {
                                e.setCategorySubTitle("null");
                            } else {
                                e.setCategorySubTitle(jsonArray.getJSONObject(i).getJSONObject("get_used_sub_category").getString("sub_title"));
                            }
                            e.setCan_share(jsonArray.getJSONObject(i).getInt("can_share"));
                            e.setId(jsonArray.getJSONObject(i).getInt("id"));
                            e.setUserId(jsonArray.getJSONObject(i).getInt("user_id"));
                            e.setIdeaTitle(jsonArray.getJSONObject(i).getString("title"));
                            e.setShortDescription(jsonArray.getJSONObject(i).getString("short_description"));
                            e.setDetailDescription(jsonArray.getJSONObject(i).getString("detailed_description"));
                            e.setIsNdaSign(jsonArray.getJSONObject(i).getInt("is_nda_sign"));
                            if (!jsonArray.getJSONObject(i).isNull("created_at")) {
                                e.setCreated_at(jsonArray.getJSONObject(i).getString("created_at"));
                            } else {
                                e.setCreated_at("2018-02-07 13:05:51");
                            }
                            if (jsonArray.getJSONObject(i).has("get_posted_user")) {
                                e.setUserName(jsonArray.getJSONObject(i).getJSONObject("get_posted_user").getString("username"));
                            } else {
                                e.setUserName(userLoggedIn.getUsername());
                            }
                            if (jsonArray.getJSONObject(i).has("is_my_favourite_idea")) {
                                if (!jsonArray.getJSONObject(i).isNull("is_my_favourite_idea") && jsonArray.getJSONObject(i).getInt("is_my_favourite_idea") == 1) {
                                    e.setFavorite(true);
                                } else {
                                    e.setFavorite(false);
                                }
                            } else {
                                e.setFavorite(false);
                            }
                            if (jsonArray.getJSONObject(i).has("is_launch_idea")) {
                                if (!jsonArray.getJSONObject(i).isNull("is_launch_idea") && jsonArray.getJSONObject(i).getInt("is_launch_idea") == 1) {
                                    e.setIdeaStatus("3");
                                    e.setIsLaunched(jsonArray.getJSONObject(i).getInt("is_launch_idea"));
                                    e.setCompany_id(jsonArray.getJSONObject(i).getJSONArray("idea_status").getJSONObject(0).getInt("company_id"));

                                } else {
                                    e.setIsLaunched(0);
                                    e.setIdeaStatus("false");
                                    e.setCompany_id(0);
                                }
                            } else {
                                e.setIsLaunched(0);
                                e.setCompany_id(0);
                                e.setIdeaStatus("false");
                            }
//                            e.setDays(jsonArray.getJSONObject(i).getInt("Days"));
//                            e.setIdeaStatus(jsonArray.getJSONObject(i).getString("status"));
                            data.add(e);
                        }
                        recycler_adapter.notifyDataSetChanged();
                        if (data.size() > 0) {
                            not_found.setVisibility(View.GONE);
                            home_fragment_recycler.setVisibility(View.VISIBLE);
                        } else {
                            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                not_found.setVisibility(View.VISIBLE);
                                text.setText("No Ideas found");
//                                CustomToast.ShowCustomToast(getContext(), "No Idea found right now!!", Gravity.CENTER);
                            } else {
                                not_found.setVisibility(View.VISIBLE);
                                text.setText("Companies are waiting for you ideas!");
                            }

                            home_fragment_recycler.setVisibility(View.GONE);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiActions == mark_as_ilaunched) {
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (!object.isNull("status")) {

                        if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                            CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                        } else {
                            CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else if (apiActions == APIActions.ApiActions.mark_as_digital_sign) {
                Log.d("onRequestSuccess: ", response);
            } else {
                JSONObject object = null;
                try {
                    object = new JSONObject(response);
                    if (!object.isNull("status")) {

                        if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                            JSONArray jsonObject = object.getJSONArray("successData");
                            ArrayList<CompanyDataModel> data = new ArrayList<>();
                            for (int i = 0; i < jsonObject.length(); i++) {
                                data.add(new CompanyDataModel(jsonObject.getJSONObject(i).getString("username"), jsonObject.getJSONObject(i).getInt("id"), false));
                            }
                            ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(HomeIdeaFragment.this, data, this.data.get(position).getId());
                            dialouge.show(getChildFragmentManager(), "dialog");
                        } else {
                            ArrayList<CompanyDataModel> data = new ArrayList<>();
                            ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(HomeIdeaFragment.this, data, this.data.get(position).getId());
                            dialouge.show(getChildFragmentManager(), "dialog");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //
    int position = 0;

    @Override
    public void onItemClickPos(View view, int position) {
        if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
//        topBarData.callSubscriptionDialogue();
            this.position = position;
            try {
                new VollyAPICall(view.getContext()
                        , true
                        , URL.mark_as_ilaunched
                        , new JSONObject().put(Constants.IDEA_ID_KEY, data.get(position).getId())
                        , userLoggedIn.getSessionToken()
                        , POST
                        , HomeIdeaFragment.this
                        , mark_as_ilaunched);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            topBarData.callSubscriptionDialogue();
        }
    }

    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {
        data.get(position).setIdeaStatus("3");
        recycler_adapter.notifyItemChanged(position);
        recycler_adapter.notifyDataSetChanged();
    }

    @Override
    public void onNDACompanyDialougeClick(NDACompanyDialouge dialog) {
        dialog.dismiss();
        if (topBarData != null) {
            topBarData.callTransaction(bundleItem, LOAD_IDEA_DETAIL_FRAGMENT);
        }
    }

    @Override
    public void onNDACompanyDialougeClick(NDACompanyDialouge dialog, DataModel e) {
        dialog.dismiss();
        callSignNda(e.getId(), 1);
        if (topBarData != null) {
            topBarData.callTransaction(bundleItem, LOAD_IDEA_DETAIL_FRAGMENT);
        }
    }
}
