package com.dev.codingpixel.i_launched.models;

/**
 * Created by incubasyss on 13/02/2018.
 */

import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetIdea {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GetIdea withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public GetIdea withTitle(String title) {
        this.title = title;
        return this;
    }

}