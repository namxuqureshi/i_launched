package com.dev.codingpixel.i_launched.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedCompanyDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class ProfileRecyclerAdapter extends RecyclerView.Adapter<ProfileRecyclerAdapter.ViewHolder> implements ILaunchedDialouge.OnDialogFragmentClickListener, ILaunchedCompanyDialouge.OnDialogFragmentClickListener, APIResponseListner {
    private LayoutInflater mInflater;
    Context mContext;
    ArrayList<DataModel> mData = new ArrayList<>();
    ArrayList<DataModel> filterData = new ArrayList<>();
    private ProfileRecyclerAdapter.ItemClickListener mClickListener;
    private ItemClickListenerPositionore mClickListenerPos;

    public ProfileRecyclerAdapter(Context context, ArrayList<DataModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        mContext = context;
        filterData.addAll(data);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.profile_fragment_item, parent, false);

        ProfileRecyclerAdapter.ViewHolder viewHolder = new ProfileRecyclerAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {


        if (!filterData.get(position).getStatus().equalsIgnoreCase("false")) {
            holder.social_share.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setImageDrawable(mContext.getDrawable(R.drawable.ic_launched_btn));
            holder.btween_view.setVisibility(View.VISIBLE);
            holder.btn_fb_share.setVisibility(View.GONE);
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                if (filterData.get(position).getCompany_id() == userLoggedIn.getId()) {
                    holder.btn_fb_share.setVisibility(View.VISIBLE);
                } else {
                    holder.btn_fb_share.setVisibility(View.GONE);
                }
            } else {
                if (filterData.get(position).getCan_share() == 1) {
                    holder.btn_fb_share.setVisibility(View.VISIBLE);
                } else {
                    holder.btn_fb_share.setVisibility(View.GONE);
                }
            }
        } else {
            holder.social_share.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setImageDrawable(mContext.getDrawable(R.drawable.i_launched_non));
            holder.btween_view.setVisibility(View.VISIBLE);
            holder.btn_fb_share.setVisibility(View.GONE);

        }
        if (filterData.get(position).getSub_title().equalsIgnoreCase("null")) {
            holder.company_name.setText(filterData.get(position).getCategory_title());
        } else {
            holder.company_name.setText(filterData.get(position).getSub_title());
        }
//        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
//            if (filterData.get(position).getCompany_id() == userLoggedIn.getId()) {
//                holder.btn_fb_share.setVisibility(View.VISIBLE);
//            } else {
//                holder.btn_fb_share.setVisibility(View.GONE);
//            }
//        } else {
//            if (filterData.get(position).getCan_share() == 1) {
//                holder.btn_fb_share.setVisibility(View.VISIBLE);
//            } else {
//                holder.btn_fb_share.setVisibility(View.GONE);
//            }
//        }
        holder.company_name_text.setText(filterData.get(position).getShortDescription());
        holder.user_name.setText(filterData.get(position).getUser_name());
        holder.description.setText(filterData.get(position).getDescription());
        holder.date.setText(DateConverter.getPrettyTime(filterData.get(position).getCreated_at()));
        holder.btn_ilaunched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (filterData.get(position).getIsLaunched() == 0 || userLoggedIn.getUserStatus() < Constants.PAID_USER) {
//                    if (mClickListenerPos != null) {
//                        mClickListenerPos.onItemClickPos(v, position);
//                    }
//                }
                if (filterData.get(position).getIsLaunched() == 0) {
                    if (mClickListenerPos != null) {
                        mClickListenerPos.onItemClickPos(v, position);
                    }
                }

            }
        });
        holder.btn_fb_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if (userLoggedIn.getType() == Constants.TYPE_COMPANY && filterData.get(position).getCan_share() == 0) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("You want to allow your innovator to share this Idea on Facebook?")
                            .setCustomImage(R.drawable.share_ic)
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    try {
                                        new VollyAPICall(v.getContext()
                                                , false
                                                , URL.can_share_on_fb
                                                , new JSONObject().put("idea_id", filterData.get(position).getId()).put("can_share", 1)
                                                , userLoggedIn.getSessionToken()
                                                , POST
                                                , ProfileRecyclerAdapter.this, APIActions.ApiActions.can_share_on_fb);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    filterData.get(position).setCan_share(1);
                                    notifyItemChanged(position);
                                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                                            .setContentTitle("Idea Launched using i-Launched Application!!")
                                            .setContentDescription("Idea Launched using i-Launched Application!!")
                                            .setRef("Idea Launched using i-Launched Application!!")
                                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                                            .build();
                                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                                }
                            }).setCancelText("No")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    try {
                                        new VollyAPICall(v.getContext()
                                                , false
                                                , URL.can_share_on_fb
                                                , new JSONObject().put("idea_id", filterData.get(position).getId()).put("can_share", 2)
                                                , userLoggedIn.getSessionToken()
                                                , POST
                                                , ProfileRecyclerAdapter.this, APIActions.ApiActions.can_share_on_fb);
                                        filterData.get(position).setCan_share(2);
                                        notifyItemChanged(position);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                                            .setContentTitle("Idea Launched using i-Launched Application!!")
                                            .setContentDescription("Idea Launched using i-Launched Application!!")
                                            .setRef("Idea Launched using i-Launched Application!!")
                                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                                            .build();
                                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);

                                }
                            }).show();
                } else if (userLoggedIn.getType() == Constants.TYPE_USER && filterData.get(position).getCan_share() == 1) {
                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                            .setContentTitle("Idea Launched using i-Launched Application!!")
                            .setContentDescription("Idea Launched using i-Launched Application!!")
                            .setRef("Idea Launched using i-Launched Application!!")
                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                            .build();
                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                } else if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                            .setContentTitle("Idea Launched using i-Launched Application!!")
                            .setContentDescription("Idea Launched using i-Launched Application!!")
                            .setRef("Idea Launched using i-Launched Application!!")
                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                            .build();
                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                }

            }


        });

    }

    @Override
    public int getItemCount() {
        return filterData.size();
    }

    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {

    }

    @Override
    public void onILaunchedCompanyDialougeButtonClick(ILaunchedCompanyDialouge dialog) {

    }

    public void setFilter(boolean b) {
        if (b) {
            filterData.clear();
            filterData.addAll(mData);
            for (DataModel e : mData) {
                if (!e.getStatus().equalsIgnoreCase("3")) {
                    filterData.remove(e);
                }
            }
            notifyDataSetChanged();

        } else {
            filterData.clear();
            filterData.addAll(mData);
            notifyDataSetChanged();
        }

    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {

    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout main_view;
        LinearLayout social_share;
        ImageView user_image, btn_fb_share, btn_ilaunched;
        AppCompatTextView user_name, date, company_name, company_name_text, description;
        View btween_view;

        public ViewHolder(View itemView) {
            super(itemView);
            user_image = itemView.findViewById(R.id.user_image);
            main_view = itemView.findViewById(R.id.main_view);
            btween_view = itemView.findViewById(R.id.btween_view);

            user_name = itemView.findViewById(R.id.user_name);
            date = itemView.findViewById(R.id.date);
            company_name = itemView.findViewById(R.id.company_name);
            company_name_text = itemView.findViewById(R.id.company_name_text);
            description = itemView.findViewById(R.id.description);
            btn_fb_share = itemView.findViewById(R.id.btn_fb_share);
            btn_ilaunched = itemView.findViewById(R.id.btn_ilaunched);
            social_share = itemView.findViewById(R.id.social_share);
            description.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void setClickListenerPosition(ItemClickListenerPositionore itemClickListener) {
        this.mClickListenerPos = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface ItemClickListenerPositionore {
        void onItemClickPos(View view, int position);
    }
}
