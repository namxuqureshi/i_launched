package com.dev.codingpixel.i_launched.interfaces;

/**
 * Created by jawadali on 10/30/17.
 */

public interface FileUploadProgressListner {
    public void UploadedProgress(int progress);
}
