package com.dev.codingpixel.i_launched.network.model;


import com.dev.codingpixel.i_launched.data_structure.APIActions;

public interface APIResponseListner {
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions);
    public void onRequestError(String response, APIActions.ApiActions apiActions);
}
