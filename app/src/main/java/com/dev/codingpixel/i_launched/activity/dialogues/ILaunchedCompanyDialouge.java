package com.dev.codingpixel.i_launched.activity.dialogues;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.CompanyListDialogueAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import static android.app.Activity.RESULT_OK;
import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.customui.ImageHelper.getRoundedCornerBitmap;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.approve_ilaunch_request;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.get_idea_by_id;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.reject_ilaunch_request;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.utils.ViewUtils.checkRotation;


public class ILaunchedCompanyDialouge extends BaseDialogFragment<ILaunchedCompanyDialouge.OnDialogFragmentClickListener> implements CompanyListDialogueAdapter.ItemClickListener, APIResponseListner {
    ImageView Attached_Image;
    String ImagePAth = "";
    AlertDialog dialog;
    TextView company_text;
    Button btn_marked_reject, btn_marked_accept;
    String userName = "Company", ideaName = "IdeaName";
    Integer idea_id = 0, sender_id = 0;


    DatePickerDialog.OnDateSetListener mDateSetListener;

    static OnDialogFragmentClickListener Listener;

    @Override
    public void onItemClick(View view, int position) {

    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        try {
            if (apiActions == get_idea_by_id) {
                JSONObject object = null;

                object = new JSONObject(response);

                JSONObject jsonObject = object.getJSONArray("successData").getJSONObject(0);
                ideaName = jsonObject.getString("title");
                company_text.setText(Html.fromHtml("<strong><b><font color=#1fbced>" + userName + "</font></b></strong> has contacted you<br>to mark the idea<br><strong><b><font color=#1fbced>“" + jsonObject.getString("title") + "”</font></b></strong> as i-Launched"));
            } else if (apiActions == approve_ilaunch_request) {

            } else if (apiActions == reject_ilaunch_request) {
//                Listener.onILaunchedCompanyDialougeButtonClick(this);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public interface OnDialogFragmentClickListener {
        public void onILaunchedCompanyDialougeButtonClick(ILaunchedCompanyDialouge dialog);
    }

    public static ILaunchedCompanyDialouge newInstance(OnDialogFragmentClickListener listner, String name, int idea_id, int sender_id)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        ILaunchedCompanyDialouge frag = new ILaunchedCompanyDialouge();
        Bundle args = new Bundle();
        args.putString("name", name);
        args.putInt(Constants.IDEA_ID_KEY, idea_id);
        args.putInt("sender_id", sender_id);
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View main_dialog = factory.inflate(R.layout.i_launched_company_layout, null);
        Bundle args = getArguments();
        userName = args.getString("name", "Company");
        idea_id = args.getInt(Constants.IDEA_ID_KEY, idea_id);
        sender_id = args.getInt("sender_id", sender_id);
        dialog = new AlertDialog.Builder(getContext(), R.style.PauseDialog).create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        //        int top_y = getResources().getDisplayMetrics().heightPixels / 5;
//        wmlp.y = top_y;   //y position
        ImageView Cross_btn = main_dialog.findViewById(R.id.cross_btn);
        Cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        InitContent(main_dialog, dialog);
        dialog.setView(main_dialog);
        new VollyAPICall(getContext()
                , false
                , URL.get_idea_by_id + "/" + idea_id
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , ILaunchedCompanyDialouge.this, get_idea_by_id);
        return dialog;
    }

    public void InitContent(final View view, final AlertDialog dialog) {
//Abubakar has contacted you\nto mark the idea\ \n“Idea Name” as i-Launched
        company_text = view.findViewById(R.id.company_text);
        btn_marked_accept = view.findViewById(R.id.btn_marked_accept);
        btn_marked_reject = view.findViewById(R.id.btn_marked_reject);
        company_text.setText(Html.fromHtml("<strong><b><font color=#1fbced>" + userName + "</font></b></strong> has contacted you<br>to mark the idea<br><strong><b><font color=#1fbced>“" + ideaName + "”</font></b></strong> as i-Launched"));

        btn_marked_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new VollyAPICall(getContext()
                            , false
                            , URL.approve_ilaunch_request
                            , new JSONObject().put(Constants.IDEA_ID_KEY, idea_id).put("company_id", sender_id)
                            , userLoggedIn.getSessionToken()
                            , POST
                            , ILaunchedCompanyDialouge.this, approve_ilaunch_request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Listener.onILaunchedCompanyDialougeButtonClick(ILaunchedCompanyDialouge.this);
            }
        });
        btn_marked_reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    new VollyAPICall(getContext()
                            , false
                            , URL.reject_ilaunch_request
                            , new JSONObject().put(Constants.IDEA_ID_KEY, idea_id).put("company_id", sender_id)
                            , userLoggedIn.getSessionToken()
                            , POST
                            , ILaunchedCompanyDialouge.this, reject_ilaunch_request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Listener.onILaunchedCompanyDialougeButtonClick(ILaunchedCompanyDialouge.this);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1200) {
            ImagePAth = data.getExtras().getString("file_path_arg");
            Log.d("paths", data.getExtras().getString("file_path_arg"));
            Bitmap bitmapOrg = BitmapFactory.decodeFile(data.getExtras().getString("file_path_arg"));
            bitmapOrg = checkRotation(bitmapOrg, data.getExtras().getString("file_path_arg"));
            bitmapOrg = Bitmap.createScaledBitmap(bitmapOrg, 300, 300, false);
            int corner_radious = (bitmapOrg.getWidth() * 10) / 100;
            Bitmap bitmap = getRoundedCornerBitmap(bitmapOrg, corner_radious);
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            Attached_Image.setBackground(drawable);
            Attached_Image.setImageDrawable(null);
        }
    }


}