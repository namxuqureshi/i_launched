package com.dev.codingpixel.i_launched.models;

/**
 * Created by incubasyss on 14/02/2018.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notification implements Parcelable {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("post_id")
    @Expose
    private Integer postId;
    @SerializedName("sender_id")
    @Expose
    private Integer sender_id;
    @SerializedName(Constants.TYPE_KEY)
    @Expose
    private String type;
    @SerializedName("notification_text")
    @Expose
    private String notificationText;
    @SerializedName("is_read")
    @Expose
    private Integer isRead;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("get_user")
    @Expose
    private GetUser get_user;

    public GetUser getGet_user() {
        return get_user;
    }

    public void setGet_user(GetUser get_user) {
        this.get_user = get_user;
    }

    private int DisplayTime;

    public Integer getSender_id() {
        return sender_id;
    }

    public void setSender_id(Integer sender_id) {
        this.sender_id = sender_id;
    }

    public Integer getId() {
        return id;
    }

    public int getDisplayTime() {
        return DisplayTime;
    }

    public void setDisplayTime(int displayTime) {
        DisplayTime = displayTime;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.userId);
        dest.writeValue(this.postId);
        dest.writeValue(this.sender_id);
        dest.writeString(this.type);
        dest.writeString(this.notificationText);
        dest.writeValue(this.isRead);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeParcelable(this.get_user, flags);
        dest.writeInt(this.DisplayTime);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.postId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sender_id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = in.readString();
        this.notificationText = in.readString();
        this.isRead = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.get_user = in.readParcelable(GetUser.class.getClassLoader());
        this.DisplayTime = in.readInt();
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}