package com.dev.codingpixel.i_launched.activity.home.side_menu;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.EditUserProfileUpdatePersonalInfoAlertDialog;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.adapters.ProfileRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.CompanyDataModel;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.i_aunched;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.mark_as_ilaunched;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_IDEA_DETAIL_FRAGMENT;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements ProfileRecyclerAdapter.ItemClickListener, APIResponseListner, EditUserProfileUpdatePersonalInfoAlertDialog.OnDialogFragmentClickListener, ProfileRecyclerAdapter.ItemClickListenerPositionore, ILaunchedDialouge.OnDialogFragmentClickListener {

    private RecyclerView profile_fragment_recycler;
    private BackInterface callBack;
    private TopBarData topBarData;
    private TextView header_text_filter, text_year_founded_company;
    EditText text_description_company, text_mission_company;
    RelativeLayout about, edit_about;
    ProfileRecyclerAdapter recyler_adapter;
    Button btnUpdate;
    LinearLayout not_found, company_side, date_layout;
    ArrayList<DataModel> data = new ArrayList<>();
    DatePickerDialog.OnDateSetListener mDateSetListener;

    public ProfileFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        InitRecyclerView(view);
        setTopView();
        Listener();
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
//            callForILaunched();
        } else {
            callForMyIdeas();
        }

    }

    /**
     * For Company Side
     */
    void callForILaunched() {
        new VollyAPICall(getContext()
                , true
                , URL.i_aunched
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , ProfileFragment.this, i_aunched);
    }

    /**
     * For User Side
     */
    void callForMyIdeas() {
        new VollyAPICall(getContext()
                , true
                , URL.my_ideas
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , ProfileFragment.this, APIActions.ApiActions.my_ideas);
    }

    /**
     * Listener Set HERE
     */
    private void Listener() {
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            header_text_filter.setOnClickListener(null);
            header_text_filter.setGravity(Gravity.START);
            header_text_filter.setText("i-Launched");
        } else {
            header_text_filter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (header_text_filter.getTag().equals("true")) {
                        header_text_filter.setTag("false");
                        header_text_filter.setTextColor(v.getContext().getResources().getColor(R.color.white));
                        recyler_adapter.setFilter(false);
                    } else {
                        header_text_filter.setTextColor(v.getContext().getResources().getColor(R.color.blue_color_difr));
                        header_text_filter.setTag("true");
                        recyler_adapter.setFilter(true);
                    }

                }
            });
        }
//        date_layout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar calendar = Calendar.getInstance();
//                DatePickerDialog abc = new DatePickerDialog(getContext(), R.style.DatePickerTheme, mDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                abc.getDatePicker().setMinDate(calendar.getTimeInMillis());
//                abc.show();
//            }
//        });
//        text_year_founded_company.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Calendar calendar = Calendar.getInstance();
//                DatePickerDialog abc = new DatePickerDialog(getContext(), R.style.DatePickerTheme, mDateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                abc.getDatePicker().setMinDate(calendar.getTimeInMillis());
//                abc.show();
//            }
//        });
        mDateSetListener =
                new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        SimpleDateFormat parseFormat = new SimpleDateFormat("MM/dd/yyyy");
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, monthOfYear, dayOfMonth);
                        Date date = calendar.getTime();
                        String s = parseFormat.format(date);
                        text_year_founded_company.setText(s);
                    }
                };
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject object = new JSONObject();
                try {
                    object.put("about_us", text_description_company.getText().toString());
                    object.put("email", userLoggedIn.getEmail());
                    object.put("phone", userLoggedIn.getPhone());
                    object.put("username", userLoggedIn.getUsername());
//                    object.put("company_desc", text_description_company.getText().toString().trim());
                    object.put("mission", text_mission_company.getText().toString().trim());
                    object.put("year_founded", text_year_founded_company.getText().toString().trim());
                    @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                            Settings.Secure.ANDROID_ID);
                    object.put("device_id", android_id);
                    userLoggedIn.setMission(text_mission_company.getText().toString().trim());
                    userLoggedIn.setYear_founded(text_year_founded_company.getText().toString().trim());
                    userLoggedIn.setAboutUs(text_description_company.getText().toString().trim());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new VollyAPICall(getContext(), true
                        , URL.update_profile, object
                        , userLoggedIn.getSessionToken()
                        , Request.Method.POST
                        , ProfileFragment.this, APIActions.ApiActions.update_profile_info);
            }
        });
//        about.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                updateBio();
//            }
//        });

    }

    /**
     * Setting top Views
     */
    public void setTopView() {
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_PROFILE_KEY, true);
            topBarData.setData(userLoggedIn.getUsername(), userLoggedIn.getEmail() + "\n" + userLoggedIn.getPhone(), true, bundle);
        }
        if (userLoggedIn.getAboutUs() != null && userLoggedIn.getAboutUs().length() > 0) {
            text_description_company.setText(userLoggedIn.getAboutUs());
            text_mission_company.setText(userLoggedIn.getMission());
            text_year_founded_company.setText(userLoggedIn.getYear_founded());
        } else {
            text_description_company.setText("");
        }
    }

    /**
     * @param view for getting and setting the ID"s
     */
    private void FindId(View view) {
        text_year_founded_company = view.findViewById(R.id.text_year_founded_company);
        btnUpdate = view.findViewById(R.id.save_idea);
        text_mission_company = view.findViewById(R.id.text_mission_company);
        profile_fragment_recycler = view.findViewById(R.id.profile_fragment_recycler);
        about = view.findViewById(R.id.about);
        company_side = view.findViewById(R.id.company_side);
        not_found = view.findViewById(R.id.not_found);
        not_found.setVisibility(View.GONE);
        edit_about = view.findViewById(R.id.edit_about);
        date_layout = view.findViewById(R.id.date_layout);
        text_description_company = view.findViewById(R.id.text_description_company);
        header_text_filter = view.findViewById(R.id.header_text_filter);
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            about.setVisibility(View.VISIBLE);
            text_description_company.setVisibility(View.VISIBLE);
            company_side.setVisibility(View.VISIBLE);
            header_text_filter.setVisibility(View.GONE);
            profile_fragment_recycler.setVisibility(View.GONE);
        } else {
            company_side.setVisibility(View.GONE);
            about.setVisibility(View.GONE);
            header_text_filter.setVisibility(View.VISIBLE);
            text_description_company.setVisibility(View.GONE);
            profile_fragment_recycler.setVisibility(View.VISIBLE);

        }

    }

    /**
     * @param view Setting the recycler
     */
    public void InitRecyclerView(final View view) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
        profile_fragment_recycler.setLayoutManager(layoutManager);
        recyler_adapter = new ProfileRecyclerAdapter(view.getContext(), data);
        recyler_adapter.setClickListener(ProfileFragment.this);
        recyler_adapter.setClickListenerPosition(ProfileFragment.this);
        profile_fragment_recycler.setAdapter(recyler_adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                bundle.putBoolean(Constants.IS_FAV_KEY, true);
            } else {
                bundle.putBoolean(Constants.IS_FAV_KEY, false);
            }
            DataModel e = data.get(position);
            bundle.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, e);
            topBarData.callTransaction(bundle, LOAD_IDEA_DETAIL_FRAGMENT);
        }
//        if (topBarData != null) {
//            topBarData.callTransaction(new Bundle(), LOAD_COMPANY_SCREEN_FRAGMENT);
//        }
    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        switch (apiActions) {
            case my_ideas:
                try {
                    JSONObject object = new JSONObject(response);
                    data.clear();
                    if (!object.isNull("status")) {
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            JSONArray jsonArray = object.getJSONArray("successData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                DataModel e = new DataModel();
                                e.setUserName(userLoggedIn.getUsername());
                                e.setCategoryTitle(jsonArray.getJSONObject(i).getJSONObject("get_category").getString("category_title"));
                                if (jsonArray.getJSONObject(i).isNull("get_used_sub_category")) {
                                    e.setCategorySubTitle("null");
                                } else {
                                    e.setCategorySubTitle(jsonArray.getJSONObject(i).getJSONObject("get_used_sub_category").getString("sub_title"));
                                }
                                e.setCan_share(jsonArray.getJSONObject(i).getInt("can_share"));
                                e.setId(jsonArray.getJSONObject(i).getInt("id"));
                                e.setUserId(jsonArray.getJSONObject(i).getInt("user_id"));
                                e.setIdeaTitle(jsonArray.getJSONObject(i).getString("title"));
                                e.setShortDescription(jsonArray.getJSONObject(i).getString("short_description"));
                                e.setDetailDescription(jsonArray.getJSONObject(i).getString("detailed_description"));
                                e.setIsNdaSign(jsonArray.getJSONObject(i).getInt("is_nda_sign"));
                                if (!jsonArray.getJSONObject(i).isNull("created_at")) {
                                    e.setCreated_at(jsonArray.getJSONObject(i).getString("created_at"));
                                } else {
                                    e.setCreated_at("2018-02-07 13:05:51");

                                }
                                if (jsonArray.getJSONObject(i).has("get_posted_user")) {
                                    e.setUserName(jsonArray.getJSONObject(i).getJSONObject("get_posted_user").getString("username"));
                                } else {
                                    e.setUserName(userLoggedIn.getUsername());
                                }

                                if (jsonArray.getJSONObject(i).has("is_launch_idea")) {
                                    if (!jsonArray.getJSONObject(i).isNull("is_launch_idea") && jsonArray.getJSONObject(i).getInt("is_launch_idea") == 1) {
                                        e.setIdeaStatus("3");
                                        e.setIsLaunched(jsonArray.getJSONObject(i).getInt("is_launch_idea"));
                                        e.setCompany_id(jsonArray.getJSONObject(i).getJSONArray("idea_status").getJSONObject(0).getInt("company_id"));

                                    } else {
                                        e.setIsLaunched(0);
                                        e.setIdeaStatus("false");
                                        e.setCompany_id(0);
                                    }
                                } else {
                                    e.setIsLaunched(0);
                                    e.setCompany_id(0);
                                    e.setIdeaStatus("false");
                                }

                                data.add(e);
                            }
                            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                header_text_filter.setOnClickListener(null);
                                header_text_filter.setGravity(Gravity.START);
                                header_text_filter.setText("i-Launched");
                                recyler_adapter.setFilter(false);
                            } else {
                                recyler_adapter.setFilter(true);
                            }

                            recyler_adapter.notifyDataSetChanged();
                            if (data.size() > 0) {
                                not_found.setVisibility(View.GONE);
                            } else {
                                not_found.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case i_aunched:
                try {
                    JSONObject object = new JSONObject(response);
                    data.clear();
                    if (!object.isNull("status")) {
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            JSONArray jsonArray = object.getJSONArray("successData");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                DataModel e = new DataModel();
                                e.setUserName(userLoggedIn.getUsername());
                                e.setCategoryTitle(jsonArray.getJSONObject(i).getJSONObject("get_category").getString("category_title"));
                                if (jsonArray.getJSONObject(i).isNull("get_used_sub_category")) {
                                    e.setCategorySubTitle("null");
                                } else {
                                    e.setCategorySubTitle(jsonArray.getJSONObject(i).getJSONObject("get_used_sub_category").getString("sub_title"));
                                }

                                e.setId(jsonArray.getJSONObject(i).getInt("id"));
                                e.setUserId(jsonArray.getJSONObject(i).getInt("user_id"));
                                e.setIdeaTitle(jsonArray.getJSONObject(i).getString("title"));
                                e.setShortDescription(jsonArray.getJSONObject(i).getString("short_description"));
                                e.setDetailDescription(jsonArray.getJSONObject(i).getString("detailed_description"));
                                e.setIsNdaSign(jsonArray.getJSONObject(i).getInt("is_nda_sign"));
                                if (!jsonArray.getJSONObject(i).isNull("created_at")) {
                                    e.setCreated_at(jsonArray.getJSONObject(i).getString("created_at"));
                                } else {
                                    e.setCreated_at("2018-02-07 13:05:51");

                                }
                                if (jsonArray.getJSONObject(i).has("get_posted_user")) {
                                    e.setUserName(jsonArray.getJSONObject(i).getJSONObject("get_posted_user").getString("username"));
                                } else {
                                    e.setUserName(userLoggedIn.getUsername());
                                }

                                if (jsonArray.getJSONObject(i).has("is_launch_idea")) {
                                    if (!jsonArray.getJSONObject(i).isNull("is_launch_idea") && jsonArray.getJSONObject(i).getInt("is_launch_idea") == 1) {
                                        e.setIdeaStatus("3");

                                    } else {
                                        e.setIdeaStatus("false");
                                    }
                                } else {
                                    e.setIdeaStatus("false");
                                }
                                e.setIdeaStatus("3");
                                data.add(e);
                            }
                            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                header_text_filter.setOnClickListener(null);
                                header_text_filter.setGravity(Gravity.START);
                                header_text_filter.setText("i-Launched");
                                recyler_adapter.setFilter(false);
                            } else {
                                recyler_adapter.setFilter(true);
                            }

                            recyler_adapter.notifyDataSetChanged();
                            if (data.size() > 0) {
                                not_found.setVisibility(View.GONE);
                            } else {
                                not_found.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case mark_as_ilaunched:
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(response);
                        if (!object.isNull("status")) {

                            if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                                CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                            } else {
                                CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(response);
                        if (!object.isNull("status")) {

                            if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                                JSONArray jsonObject = object.getJSONArray("successData");
                                ArrayList<CompanyDataModel> data = new ArrayList<>();
                                for (int i = 0; i < jsonObject.length(); i++) {
                                    data.add(new CompanyDataModel(jsonObject.getJSONObject(i).getString("username"), jsonObject.getJSONObject(i).getInt("id"), false));
                                }
                                ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(ProfileFragment.this, data, this.data.get(position).getId());
                                dialouge.show(getChildFragmentManager(), "dialog");
                            } else {
                                ArrayList<CompanyDataModel> data = new ArrayList<>();
                                ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(ProfileFragment.this, data, this.data.get(position).getId());
                                dialouge.show(getChildFragmentManager(), "dialog");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case update_profile_info:
                break;
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updating EMAIL Dialogue
     */
    public void updateEmail() {
        JSONObject object = new JSONObject();
        try {
            if (!userLoggedIn.getEmail().equalsIgnoreCase("null") && userLoggedIn.getEmail() != null) {
                object.put("email", userLoggedIn.getEmail());
            } else {
                object.put("email", "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EditUserProfileUpdatePersonalInfoAlertDialog shootOutAlertDialog = EditUserProfileUpdatePersonalInfoAlertDialog.newInstance(ProfileFragment.this, "Email", object);
        shootOutAlertDialog.show(getChildFragmentManager(), "dialog");

    }

    /**
     * Updating PHONE Dialogue
     */
    public void updatePhone() {
        JSONObject object = new JSONObject();
        try {
            if (!userLoggedIn.getPhone().equalsIgnoreCase("null") && userLoggedIn.getPhone() != null) {
                object.put("zipcode", userLoggedIn.getPhone());
            } else {
                object.put("zipcode", "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EditUserProfileUpdatePersonalInfoAlertDialog shootOutAlertDialog = EditUserProfileUpdatePersonalInfoAlertDialog.newInstance(ProfileFragment.this, "Zipcode", object);
        shootOutAlertDialog.show(getChildFragmentManager(), "dialog");
    }

    /**
     * Updating BIO Dialogue for company
     */
    public void updateBio() {
        JSONObject object = new JSONObject();
        try {
            object.put("bio", text_description_company.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        EditUserProfileUpdatePersonalInfoAlertDialog shootOutAlertDialog = EditUserProfileUpdatePersonalInfoAlertDialog.newInstance(ProfileFragment.this, "BIO", object);
        shootOutAlertDialog.show(getChildFragmentManager(), "dialog");
    }

    @Override
    public void onSaveButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog dialog, JSONObject data) {
        dialog.dismiss();
        try {
            if (data.getString(Constants.TYPE_KEY).equalsIgnoreCase("email")) {
                userLoggedIn.setEmail(data.getString("email"));
            } else if (data.getString(Constants.TYPE_KEY).equalsIgnoreCase("phone")) {
                userLoggedIn.setPhone(data.getString("phone"));
            } else if (data.getString(Constants.TYPE_KEY).equalsIgnoreCase("bio")) {
                userLoggedIn.setAboutUs(data.getString("bio"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ProfileFragment.this.setTopView();
    }

    @Override
    public void onCrossButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog dialog) {
        dialog.dismiss();
    }

    /**
     * For setting dataList position to send for api
     */
    int position = 0;

    /**
     * @param view     for any view relatied task
     * @param position for getting position of item from list
     *                 and giv a call for i-launched
     */
    @Override
    public void onItemClickPos(View view, int position) {
        if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
            this.position = position;
            try {
                new VollyAPICall(view.getContext()
                        , true
                        , URL.mark_as_ilaunched
                        , new JSONObject().put(Constants.IDEA_ID_KEY, data.get(position).getId())
                        , userLoggedIn.getSessionToken()
                        , POST
                        , ProfileFragment.this
                        , mark_as_ilaunched);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            topBarData.callSubscriptionDialogue();
        }
    }

    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {
        data.get(position).setIdeaStatus("3");
        recyler_adapter.notifyItemChanged(position);
        recyler_adapter.notifyDataSetChanged();
    }
}
