package com.dev.codingpixel.i_launched.customui;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Created by incubasyss on 06/02/2018.
 */

public class CustomAutoComplete extends android.support.v7.widget.AppCompatAutoCompleteTextView {
    public CustomAutoComplete(Context context) {
        super(context);

    }

    public CustomAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomAutoComplete(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
