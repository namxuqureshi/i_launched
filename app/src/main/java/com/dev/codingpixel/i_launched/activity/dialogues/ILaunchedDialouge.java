package com.dev.codingpixel.i_launched.activity.dialogues;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.CompanyListDialogueAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.CompanyDataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.customui.ImageHelper.getRoundedCornerBitmap;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.utils.ViewUtils.checkRotation;


public class ILaunchedDialouge extends BaseDialogFragment<ILaunchedDialouge.OnDialogFragmentClickListener> implements CompanyListDialogueAdapter.ItemClickListener, APIResponseListner {
    ImageView Attached_Image;
    public static final String TAG = "abc";
    String ImagePAth = "";
    AlertDialog dialog;
    RecyclerView company_list;
    CompanyListDialogueAdapter recyler_adapter;
    ArrayList<CompanyDataModel> data = new ArrayList<>();
    DatePickerDialog.OnDateSetListener mDateSetListener;
    CompanyDataModel sendModel;
    Button btn_marked;
    int Idea_id;
    static OnDialogFragmentClickListener Listener;

    @Override
    public void onItemClick(View view, int position) {
        sendModel = data.get(position);
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        Log.d(TAG, "onRequestSuccess: " + response);
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        Log.e(TAG, "onRequestError: " + response);
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public interface OnDialogFragmentClickListener {
        public void onILaunchedButtonClick(ILaunchedDialouge dialog);
    }

    public static ILaunchedDialouge newInstance(OnDialogFragmentClickListener listner)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        ILaunchedDialouge frag = new ILaunchedDialouge();
        Bundle args = new Bundle();
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    public static ILaunchedDialouge newInstance(OnDialogFragmentClickListener listner, ArrayList<CompanyDataModel> data, int idea_id)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        ILaunchedDialouge frag = new ILaunchedDialouge();
        Bundle args = new Bundle();
        args.putParcelableArrayList("data", data);
        args.putInt(Constants.IDEA_ID_KEY, idea_id);
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View main_dialog = factory.inflate(R.layout.i_launched_layout, null);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            this.data = bundle.getParcelableArrayList("data");
            this.Idea_id = bundle.getInt(Constants.IDEA_ID_KEY);
        }
        dialog = new AlertDialog.Builder(getContext(), R.style.PauseDialog).create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        //        int top_y = getResources().getDisplayMetrics().heightPixels / 5;
//        wmlp.y = top_y;   //y position
        ImageView Cross_btn = main_dialog.findViewById(R.id.cross_btn);
        Cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        InitContent(main_dialog, dialog);
        dialog.setView(main_dialog);
        return dialog;
    }

    public void InitContent(final View view, final AlertDialog dialog) {

        company_list = view.findViewById(R.id.company_list);
        btn_marked = view.findViewById(R.id.btn_marked);
        btn_marked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.size() > 0) {
                    if (sendModel != null && sendModel.isTick()) {
                        try {
                            new VollyAPICall(getContext()
                                    , false
                                    , URL.approve_ilaunch_request
                                    , new JSONObject().put(Constants.IDEA_ID_KEY, Idea_id).put("company_id", sendModel.getId())
                                    , userLoggedIn.getSessionToken()
                                    , POST
                                    , ILaunchedDialouge.this
                                    , APIActions.ApiActions.delete_idea);
                            CustomToast.ShowCustomToast(getContext(), "Idea has been i-Launched", Gravity.CENTER);
                            Listener.onILaunchedButtonClick(ILaunchedDialouge.this);
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        CustomToast.ShowCustomToast(getContext(), "Please Select Company to Mark idea as i-Launched!!", Gravity.CENTER);
                    }
                } else {
                    CustomToast.ShowCustomToast(getContext(), "No Company Approach your idea", Gravity.CENTER);
                }
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(view.getContext());
                company_list.setLayoutManager(layoutManager);
                recyler_adapter = new CompanyListDialogueAdapter(view.getContext(), data);
                recyler_adapter.setClickListener(ILaunchedDialouge.this);
                company_list.setAdapter(recyler_adapter);
            }
        }, 200);

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1200) {
            ImagePAth = data.getExtras().getString("file_path_arg");
            Log.d("paths", data.getExtras().getString("file_path_arg"));
            Bitmap bitmapOrg = BitmapFactory.decodeFile(data.getExtras().getString("file_path_arg"));
            bitmapOrg = checkRotation(bitmapOrg, data.getExtras().getString("file_path_arg"));
            bitmapOrg = Bitmap.createScaledBitmap(bitmapOrg, 300, 300, false);
            int corner_radious = (bitmapOrg.getWidth() * 10) / 100;
            Bitmap bitmap = getRoundedCornerBitmap(bitmapOrg, corner_radious);
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            Attached_Image.setBackground(drawable);
            Attached_Image.setImageDrawable(null);
        }
    }

}