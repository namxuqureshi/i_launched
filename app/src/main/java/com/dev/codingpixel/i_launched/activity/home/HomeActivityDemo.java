package com.dev.codingpixel.i_launched.activity.home;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.home.bottom_fragment.HomeIdeaFragment;
import com.dev.codingpixel.i_launched.adapters.HomeDrawerRecylerAdapter;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;

import java.util.ArrayList;

import nl.psdcompany.duonavigationdrawer.views.DuoDrawerLayout;
import nl.psdcompany.duonavigationdrawer.widgets.DuoDrawerToggle;

import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;
import static com.dev.codingpixel.i_launched.static_function.UIModification.ShowStatusBar;

public class HomeActivityDemo extends AppCompatActivity implements HomeDrawerRecylerAdapter.ItemClickListener, BackInterface, View.OnClickListener {
    //    public static DrawerLayout drawerLayout;
    RecyclerView drawer_recyler;
    ImageView menu_btn;
    public FragmentTransaction transaction;
    public FragmentManager manager;
    private HomeIdeaFragment homeIdeaFragment;
    LinearLayout drawer_bottom;
    DuoDrawerLayout drawerLayout;
    DuoDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_demo);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        ShowStatusBar(HomeActivityDemo.this);
        InitDrawerContent();
        InitBottomTab();
        DefineFragmentTransection();

//        drawerLayout = (DrawerLayout) findViewById(R.id.my_drawer_layout);

        drawerLayout = (DuoDrawerLayout) findViewById(R.id.my_drawer_layout);

        drawerToggle = new DuoDrawerToggle(this, drawerLayout, null,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
                invalidateOptionsMenu();

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
                drawer_bottom.setVisibility(View.VISIBLE);

            }

        };
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        LoadMainFragment(true);
    }

    public void LoadMainFragment(boolean check) {
//        if (transaction == null) {
        DefineFragmentTransection();
//        }
//        if (!getIntent().getExtras().isEmpty()) {
////            if (getIntent().getExtras().getString("activityToBeOpened") != null) {
////                String notification_activity = getIntent().getExtras().getString("activityToBeOpened");
////                switch (notification_activity) {
////                    case "Groups":
////                        transaction.add(R.id.fragment_id_full_screen, new GroupsMainTabFragment(), "1");
////                        ChangeStatusBarColor(HomeActivity.this, "#171717");
////                        Top_Line.setBackgroundColor(Color.parseColor("#171717"));
////                        Top_Line.setVisibility(View.GONE);
////                        break;
////                    case "group_invitation":
////                        transaction.add(R.id.fragment_id_full_screen, new GroupsMainTabFragment(true, getIntent().getExtras().getString("data")), "1");
////                        ChangeStatusBarColor(HomeActivity.this, "#171717");
////                        Top_Line.setBackgroundColor(Color.parseColor("#171717"));
////                        Top_Line.setVisibility(View.GONE);
////                        break;
////                }
////            } else {
////                homeMainFragment = new HomeMainFragment();
////                homeMainFragment.SetBackListener(HomeActivity.this);
////                transaction.add(R.id.fragment_id, homeMainFragment, "1");
////            }
//        } else {
//
//        }
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        setHomeFragmentBool(check);

        transaction.add(R.id.fragment_id, homeIdeaFragment, "1");
        transaction.commitAllowingStateLoss();
    }

    void setHomeFragmentBool(boolean check) {
        homeIdeaFragment = new HomeIdeaFragment();
        homeIdeaFragment.SetBackListener(HomeActivityDemo.this);
        Bundle bundle = new Bundle();
        bundle.putBoolean("state", check);
        homeIdeaFragment.setArguments(bundle);

    }

    public void InitDrawerContent() {
        drawer_bottom = findViewById(R.id.drawer_bottom);
        drawer_recyler = (RecyclerView) findViewById(R.id.i_launcher_side_me_recycle);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeActivityDemo.this);
                drawer_recyler.setLayoutManager(layoutManager);
                HomeDrawerRecylerAdapter recyler_adapter = new HomeDrawerRecylerAdapter(HomeActivityDemo.this, home_drawer_data());
                recyler_adapter.setClickListener(HomeActivityDemo.this);
                drawer_recyler.setAdapter(recyler_adapter);
            }
        }, 200);
    }

    @Override
    public void onItemSideMenuClick(View view, int position) {
        switch (position) {
            case 0:
                LoadMainFragment(false);
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 1:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 2:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 3:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 4:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 5:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            case 7:
                drawerLayout.closeDrawer(Gravity.START);
                break;
            default:
                break;
        }

    }

    public static ArrayList<String> home_drawer_data() {
        ArrayList<String> datas = new ArrayList<>();
        datas.add("Home");
        datas.add("My Categories");
        datas.add("Profile");
        datas.add("Notifications");
        datas.add("Messages");
        datas.add("i-Launched");
        datas.add("");
        datas.add("Manager Subscription");
        datas.add("");

        return datas;
    }

    public void DefineFragmentTransection() {
        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.setAllowOptimization(true);
//        transaction.bac
    }

    @Override
    public void callBack() {

    }

    public void InitBottomTab() {
        Button Tab_one = (Button) findViewById(R.id.tab_one);
        Button Tab_two = (Button) findViewById(R.id.tab_two);
        Button Tab_three = (Button) findViewById(R.id.tab_three);
        Button Tab_four = (Button) findViewById(R.id.tab_four);
        Button Tab_five = (Button) findViewById(R.id.tab_five);
        Tab_one.setOnClickListener(this);
        Tab_two.setOnClickListener(this);
        Tab_three.setOnClickListener(this);
        Tab_four.setOnClickListener(this);
        Tab_five.setOnClickListener(this);
        menu_btn = (ImageView) findViewById(R.id.menu_btn);
        menu_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.menu_btn:
                HideKeyboard(this);
                drawerLayout.openDrawer(Gravity.START);

                break;
//            case R.id.q_a_menu_btn:
//                HideKeyboard(this);
//                drawerLayout.openDrawer(Gravity.START);

//                break;
            case R.id.tab_one:
                HideKeyboard(this);
                HomeActivityDemo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        transaction = manager.beginTransaction();
//                        if (!qa_homeTabFragment.isVisible()) {
//                            if (transaction.isEmpty()) {
//                                transaction.remove(budzMapHomeFragment);
//                                //transaction.setCustomAnimations(R.anim.attatch_fragment_left_to_right , R.anim.fade_out);
//                                transaction.add(R.id.fragment_id_full_screen, qa_homeTabFragment, "1");
//                            } else {
//                                transaction.remove(budzMapHomeFragment);
//                                // transaction.setCustomAnimations(R.anim.attatch_fragment_left_to_right , R.anim.fade_out);
//                                transaction.replace(R.id.fragment_id_full_screen, new QA_HomeTabFragment(HomeActivity.this), "1");
//                            }
//                            transaction.commitAllowingStateLoss();
//                            Home_Menu.setVisibility(View.GONE);
//                        } else {
//                            qa_homeTabFragment.DetachSubFragments();
//                            if (discussQuestionFragment != null) {
//                                if (discussQuestionFragment.isVisible()) {
//                                    transaction.remove(discussQuestionFragment);
//                                    transaction.commitAllowingStateLoss();
//                                }
//                            }
//
//                        }
                        LoadMainFragment(true);
                    }
                });
                break;
            case R.id.tab_two:

                HideKeyboard(this);
                HomeActivityDemo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        transaction = manager.beginTransaction();
//                        if (!groupsMainTabFragment.isVisible()) {
//                            if (transaction.isEmpty()) {
//                                transaction.add(R.id.fragment_id_full_screen, groupsMainTabFragment, "1");
//                            } else {
//                                transaction.replace(R.id.fragment_id_full_screen, groupsMainTabFragment, "1");
//                            }
//                            transaction.commitAllowingStateLoss();
//                        } else {
//                            if (discussQuestionFragment != null) {
//                                if (discussQuestionFragment.isVisible()) {
//                                    transaction.remove(discussQuestionFragment);
//                                    transaction.commitAllowingStateLoss();
//                                }
//                            }
//                        }
                    }
                });
                break;
            case R.id.tab_three:
                HideKeyboard(this);
                HomeActivityDemo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        transaction = manager.beginTransaction();
//                        if (!journalTabFragment.isVisible()) {
//                            if (transaction.isEmpty()) {
//                                transaction.add(R.id.fragment_id_full_screen, journalTabFragment, "1");
//                            } else {
//                                transaction.replace(R.id.fragment_id_full_screen, journalTabFragment, "1");
//                            }
//                            transaction.commitAllowingStateLoss();
//                        } else {
//                            if (discussQuestionFragment != null) {
//                                if (discussQuestionFragment.isVisible()) {
//                                    transaction.remove(discussQuestionFragment);
//                                    transaction.commitAllowingStateLoss();
//                                }
//                            }
//                        }
                    }
                });
                break;
            case R.id.tab_four:
                HideKeyboard(this);
                HomeActivityDemo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        transaction = manager.beginTransaction();
//                        if (!strainTabFragment.isVisible()) {
//                            if (transaction.isEmpty()) {
//                                transaction.add(R.id.fragment_id_full_screen, strainTabFragment, "1");
//                            } else {
//                                transaction.replace(R.id.fragment_id_full_screen, strainTabFragment, "1");
//                            }
//                            transaction.commitAllowingStateLoss();
//                        } else {
//                            if (discussQuestionFragment != null) {
//                                if (discussQuestionFragment.isVisible()) {
//                                    transaction.remove(discussQuestionFragment);
//                                    transaction.commitAllowingStateLoss();
//                                }
//                            }
//                        }
                    }
                });
                break;
            case R.id.tab_five:
                HideKeyboard(this);
                HomeActivityDemo.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
//                        transaction = manager.beginTransaction();
//                        if (!budzMapHomeFragment.isVisible()) {
//                            if (transaction.isEmpty()) {
//                                transaction.add(R.id.fragment_id_full_screen, budzMapHomeFragment, "1");
//                            } else {
//                                transaction.replace(R.id.fragment_id_full_screen, budzMapHomeFragment, "1");
//                            }
//                            transaction.commitAllowingStateLoss();
//                        } else {
//                            if (discussQuestionFragment != null) {
//                                if (discussQuestionFragment.isVisible()) {
//                                    transaction.remove(discussQuestionFragment);
//                                    transaction.commitAllowingStateLoss();
//                                }
//                            }
//                        }
                    }
                });
                break;


//            case R.id.home_search:
//                ChangeStatusBarColor(HomeActivity.this, "#5c9233");
//                Search_layout_view.setVisibility(View.VISIBLE);
//
//                Home_search_Header.setVisibility(View.GONE);
//                Home_search_main_content_list.setVisibility(View.GONE);
//                Animation startAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
//                Search_layout_view.startAnimation(startAnimation);
//                break;

//            case R.id.home_search_edit_text:
//                Home_search_Header.setVisibility(View.VISIBLE);
//                Home_search_main_content_list.setVisibility(View.VISIBLE);
//                break;
//            case R.id.back_btn:
//                HideKeyboard(HomeActivity.this);
//                hideSearchLayout();
//                ChangeStatusBarColor(HomeActivity.this, "#1c1919");
//                break;
//            case R.id.home_btn:
//                HideKeyboard(HomeActivity.this);
//                hideSearchLayout();
//                Home_search_editText.setText("");
//                ChangeStatusBarColor(HomeActivity.this, "#1c1919");
//                break;
//            case R.id.profile_btn_slide_menu:
//                drawerLayout.closeDrawer(Gravity.START);
//                GoToProfile(HomeActivity.this, user.getUser_id());
//                break;

//            case R.id.setting_btn:
//                GoTo(HomeActivity.this, SettingActivity.class);
//                break;
        }
    }
}
