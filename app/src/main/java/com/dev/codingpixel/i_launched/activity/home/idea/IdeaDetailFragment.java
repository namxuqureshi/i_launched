package com.dev.codingpixel.i_launched.activity.home.idea;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.ProgressDialog;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.CompanyDataModel;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.IntentFunction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.connect_innovator;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.mark_as_ilaunched;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

//import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class IdeaDetailFragment extends Fragment implements ILaunchedDialouge.OnDialogFragmentClickListener, APIResponseListner {
    public static final String TAG = "IdeaDetailFragment";
    private RecyclerView categories_fragment_recycler;
    private BackInterface callBack;
    private ImageView not_found;
    private TopBarData topBarData;
    DataModel passDataModel;
    TextView response_text, detailed_text;
    Button btn_mark_launched, btn_delete_idea, btn_contact;
    Bundle bundle = new Bundle();
    boolean isApiCall = false;
    int threadId = 0;

    final ProgressDialog pd = ProgressDialog.newInstance();


    public IdeaDetailFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_idea_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        Listener();
        setTopView();
    }

    /**
     * Listener
     */
    private void Listener() {
        btn_mark_launched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                    callForMarkILaunched();
                } else {
                    topBarData.callSubscriptionDialogue();
                }
            }
        });
        btn_delete_idea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callForDeleteIdeaApi(passDataModel.getId());
            }
        });
        btn_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                    callForContactApi();
                } else {
                    topBarData.callSubscriptionDialogue();
                }
            }
        });
        response_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                    Bundle bundle = new Bundle();
                    bundle.putInt(Constants.IDEA_ID_KEY, passDataModel.getId());
                    bundle.putBoolean(Constants.IS_COMPANY_KEY, false);
                    bundle.putParcelable(Constants.DATA_MODEL_KEY, passDataModel);
                    topBarData.callTransaction(bundle, Constants.MESSAGE_THREAD_USER);
                } else {
                    topBarData.callSubscriptionDialogue();
                }
            }
        });
    }

    /**
     * Mark i Launched Api
     */
    void callForMarkILaunched() {
        try {
            new VollyAPICall(getContext()
                    , true
                    , URL.mark_as_ilaunched
                    , new JSONObject().put(Constants.IDEA_ID_KEY, passDataModel.getId())
                    , userLoggedIn.getSessionToken()
                    , POST
                    , IdeaDetailFragment.this
                    , mark_as_ilaunched);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Contact For Company Api Call
     */
    void callForContactApi() {
        try {
            pd.show(getChildFragmentManager(), "pd");
            new VollyAPICall(getContext()
                    , false
                    , URL.connect_innovator
                    , new JSONObject().put(Constants.IDEA_ID_KEY, passDataModel.getId()).put("receiver_id", passDataModel.getUser_id())
                    , userLoggedIn.getSessionToken()
                    , POST
                    , IdeaDetailFragment.this
                    , connect_innovator);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * @param idea_id Delete Idea Api Call by passing id
     */
    void callForDeleteIdeaApi(final int idea_id) {
        //ASK IF WANT TO DELETE
//                try {
//                    new VollyAPICall(getContext()
//                            , true
//                            , URL.delete_idea
//                            , new JSONObject().put("id", passDataModel.getId())
//                            , userLoggedIn.getSessionToken()
//                            , POST
//                            , IdeaDetailFragment.this
//                            , APIActions.ApiActions.delete_idea);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
        new SweetAlertDialog(getContext(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("You want to delete this idea?")
                .setConfirmText("Yes, delete it!")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                        try {
                            new VollyAPICall(getContext()
                                    , true
                                    , URL.delete_idea
                                    , new JSONObject().put(Constants.ID_KEY, idea_id)
                                    , userLoggedIn.getSessionToken()
                                    , POST
                                    , IdeaDetailFragment.this
                                    , APIActions.ApiActions.delete_idea);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setCancelText("Close!")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();

    }

    /**
     * @param idea_id for getting the detail of idea.
     */
    void callForIdeaDetailApi(int idea_id) {
        new VollyAPICall(getContext()
                , true
                , URL.get_idea_by_id + "/" + idea_id
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , IdeaDetailFragment.this, APIActions.ApiActions.get_idea_by_id);
    }

    /**
     * Set Top View From fragment
     */
    public void setTopView() {
        if (getArguments() != null) {

            bundle = getArguments();
            passDataModel = bundle.getParcelable(Constants.OBJECT_DATA_MODEL_KEY);
            bundle.putBoolean(Constants.USER_VIEW_KEY, isApiCall);
            detailed_text.setText(passDataModel.getLongDescription());
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                topBarData.setFavIconDisplay(true, passDataModel);
            } else {
                topBarData.setFavIconDisplay(false, passDataModel);
            }

            if (!isApiCall) {
                callForIdeaDetailApi(passDataModel.getId());
            }
            if (topBarData != null) {
                bundle.putBoolean(Constants.IS_BACK_KEY, true);
                bundle.putBoolean(Constants.IS_BULB_KEY, true);
                bundle.putBoolean(Constants.IS_TAG_KEY, true);
                bundle.putBoolean(Constants.IS_OTHER_KEY, true);
                bundle.putString(Constants.TAG_KEY, TAG);
                topBarData.setData(passDataModel.getShortDescription()
                        , "Detailed description of my idea"
                        , true
                        , bundle);
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    topBarData.setFavIconDisplay(true, passDataModel);
                } else {
                    topBarData.setFavIconDisplay(false, passDataModel);
                }
            }
        } else {
            if (!isApiCall) {
                callForIdeaDetailApi(passDataModel.getId());
            }
            if (topBarData != null) {
                topBarData.setFavIconDisplay(false, passDataModel);
            }
            if (topBarData != null) {
                bundle.putBoolean(Constants.IS_BACK_KEY, true);
                bundle.putBoolean(Constants.IS_BULB_KEY, true);
                bundle.putBoolean(Constants.IS_TAG_KEY, true);
                bundle.putBoolean(Constants.IS_OTHER_KEY, true);
                bundle.putString(Constants.TAG_KEY, TAG);
                topBarData.setData("Idea Heading Here"
                        , "Detailed description of my idea"
                        , true
                        , bundle);
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    topBarData.setFavIconDisplay(true, passDataModel);
                } else {
                    topBarData.setFavIconDisplay(false, passDataModel);
                }
            }
        }


    }

    @Override
    public void onPause() {
        super.onPause();

    }

    /**
     * @param view Set ID's
     */
    private void FindId(View view) {
        btn_mark_launched = view.findViewById(R.id.btn_mark_launched);
        btn_delete_idea = view.findViewById(R.id.btn_delete_idea);
        response_text = view.findViewById(R.id.response_text);

        detailed_text = view.findViewById(R.id.detailed_text);
        btn_contact = view.findViewById(R.id.btn_contact);
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            btn_contact.setVisibility(View.VISIBLE);
            btn_delete_idea.setVisibility(View.GONE);
            response_text.setVisibility(View.GONE);
        } else {
            response_text.setVisibility(View.VISIBLE);
            btn_contact.setVisibility(View.GONE);
            btn_delete_idea.setVisibility(View.VISIBLE);
        }
    }


    /**
     * @param topBarData
     */
    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }


    /**
     * @param homeActivity
     */
    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {

    }


    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        switch (apiActions) {
            case get_idea_by_id:
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.isNull("status")) {
                        isApiCall = true;
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            JSONObject jsonObject = object.getJSONArray("successData").getJSONObject(0);
                            DataModel e = new DataModel();
                            e.setCategoryTitle(jsonObject.getJSONObject("get_category").getString("category_title"));
                            if (jsonObject.isNull("get_used_sub_category")) {
                                e.setCategorySubTitle("null");
                            } else {
                                e.setCategorySubTitle(jsonObject.getJSONObject("get_used_sub_category").getString("sub_title"));
                            }
                            if (jsonObject.has("is_my_favourite_idea")) {
                                if (!jsonObject.isNull("is_my_favourite_idea") && jsonObject.getInt("is_my_favourite_idea") == 1) {
                                    e.setFavorite(true);
                                } else {
                                    e.setFavorite(false);
                                }
                            } else {
                                e.setFavorite(false);
                            }
                            e.setId(jsonObject.getInt("id"));
                            e.setUserId(jsonObject.getInt("user_id"));
                            e.setIdeaTitle(jsonObject.getString("title"));
                            e.setShortDescription(jsonObject.getString("short_description"));
                            e.setIsNdaSign(jsonObject.getInt("is_nda_sign"));
                            e.setDetailDescription(jsonObject.getString("detailed_description"));
                            if (!jsonObject.isNull("created_at")) {
                                e.setCreated_at(jsonObject.getString("created_at"));
                            } else {
                                e.setCreated_at("2018-02-07 13:05:51");
                            }
                            e.setUserName(jsonObject.getJSONObject("get_user").getString("username"));
                            bundle.putBoolean(Constants.USER_VIEW_KEY, true);
                            bundle.putString("user_name", jsonObject.getJSONObject("get_user").getString("username"));
                            if (jsonObject.has("is_launch_idea")) {
                                if (!jsonObject.isNull("is_launch_idea") && jsonObject.getInt("is_launch_idea") == 1) {
                                    e.setIdeaStatus("3");
                                    btn_mark_launched.setVisibility(View.GONE);
                                } else {
                                    btn_mark_launched.setVisibility(View.VISIBLE);
                                    e.setIdeaStatus("false");
                                }
                            } else {
                                btn_mark_launched.setVisibility(View.VISIBLE);
                                e.setIdeaStatus("false");
                            }
                            passDataModel = e;
                            Bundle bundleabc = new Bundle();
                            bundleabc.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, passDataModel);
                            bundleabc.putBoolean(Constants.USER_VIEW_KEY, true);
                            bundleabc.putString("user_name", jsonObject.getJSONObject("get_user").getString("username"));
                            setArguments(bundleabc);
                            detailed_text.setText(jsonObject.getString("detailed_description"));
                            setTopView();

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case delete_idea:
                Log.d(TAG, "onRequestSuccess: " + response);
                topBarData.pressedBackButtonFromFragment();
                break;
            case mark_as_ilaunched:
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    JSONObject object = null;

                    try {
                        object = new JSONObject(response);
                        if (!object.isNull("status")) {
                            isApiCall = true;
                            if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                                CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                            } else {
                                CustomToast.ShowCustomToast(getContext(), object.getString("successMessage"), Gravity.CENTER);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(response);
                        if (!object.isNull("status")) {
                            isApiCall = true;
                            if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                                JSONArray jsonObject = object.getJSONArray("successData");
                                ArrayList<CompanyDataModel> data = new ArrayList<>();
                                for (int i = 0; i < jsonObject.length(); i++) {
                                    data.add(new CompanyDataModel(jsonObject.getJSONObject(i).getString("username"), jsonObject.getJSONObject(i).getInt("id"), false));
                                }
                                ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(IdeaDetailFragment.this, data, passDataModel.getId());
                                dialouge.show(getChildFragmentManager(), "dialog");
                            } else {
                                ArrayList<CompanyDataModel> data = new ArrayList<>();
                                ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(IdeaDetailFragment.this, data, passDataModel.getId());
                                dialouge.show(getChildFragmentManager(), "dialog");
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                break;
            case connect_innovator:
                JSONObject object = null;

                try {
                    object = new JSONObject(response);
                    if (!object.isNull("status")) {
                        isApiCall = true;
                        pd.dismiss();
                        if (object.getString("status").equalsIgnoreCase("success") && !object.opt("successData").equals("")) {
                            threadId = object.getInt("successData");
                            IntentFunction.GoToChatView(getActivity(), threadId);
                        } else {
                            CustomToast.ShowCustomToast(getContext(), "Error!!", Gravity.CENTER);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case send_message:
                pd.dismiss();
                IntentFunction.GoToChatView(getActivity(), threadId);

                break;
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
