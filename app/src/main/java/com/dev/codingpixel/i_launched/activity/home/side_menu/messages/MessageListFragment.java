package com.dev.codingpixel.i_launched.activity.home.side_menu.messages;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.MessageListRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.models.MessageListModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.IntentFunction;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.android.volley.Request.Method.GET;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.message_list_by_idea;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * A simple {@link Fragment} subclass.
 */
public class MessageListFragment extends Fragment implements MessageListRecyclerAdapter.ItemClickListener, APIResponseListner {
    public static final String TAG = "MessageList";
    private RecyclerView message_fragment_recycler;
    private BackInterface callBack;

    private TopBarData topBarData;
    private String description_top = "";
    private int idea_id;
    TextView not_found;
    DataModel ideaModel;
    MessageListRecyclerAdapter recyler_adapter;
    List<MessageListModel> data = new ArrayList<>();

    public MessageListFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_message_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        InitRecyclerView(view);
        setTopView();
    }

    /**
     * For Company Side Api For List
     */
    void callForMessageListCompany() {
        new VollyAPICall(getContext()
                , true
                , URL.message_list
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , MessageListFragment.this
                , APIActions.ApiActions.message_list);
    }

    /**
     * @param idea_id for user side by giving id of IDEA
     */
    void callForMessageListUser(int idea_id) {
        new VollyAPICall(getContext()
                , true
                , URL.message_list_by_idea + "/" + idea_id
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , MessageListFragment.this
                , message_list_by_idea);

    }

    /**
     * setting top views
     */
    public void setTopView() {
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            if (bundle.getBoolean(Constants.IS_COMPANY_KEY, false)) {
                //WHEN COME FROM COMPANY
                callForMessageListCompany();
            } else {
                //WHEN COME FROM USER
                idea_id = bundle.getInt(Constants.IDEA_ID_KEY);
                ideaModel = bundle.getParcelable(Constants.DATA_MODEL_KEY);
                callForMessageListUser(idea_id);
            }
        }
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_BACK_KEY, true);
            bundle.putBoolean(Constants.IS_BULB_KEY, true);
            bundle.putBoolean(Constants.IS_TAG_KEY, true);
            bundle.putString(Constants.TAG_KEY, TAG);
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                topBarData.setData("Messages List", "List of Innovators", false, bundle);
            } else {
                topBarData.setData("Messages List", ideaModel.getShortDescription(), false, bundle);
            }

        }
    }

    /**
     * @param view for setting and getting ID's
     */
    private void FindId(View view) {
        message_fragment_recycler = view.findViewById(R.id.message_fragment_recycler);
        not_found = view.findViewById(R.id.not_found);

    }

    /**
     * @param view setting the recycler list
     */
    public void InitRecyclerView(final View view) {

        RecyclerView.LayoutManager layoutManager;
        layoutManager = new LinearLayoutManager(view.getContext());
        message_fragment_recycler.setLayoutManager(layoutManager);

        recyler_adapter = new MessageListRecyclerAdapter(view.getContext(), data);
        recyler_adapter.setClickListener(MessageListFragment.this);
        message_fragment_recycler.setAdapter(recyler_adapter);
    }


    @Override
    public void onItemClick(View view, int position) {
        IntentFunction.GoToChatView((Activity) view.getContext(), data.get(position).getId());
    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == message_list_by_idea) {
            Log.d(TAG, "onRequestSuccess: " + response);
            try {
                JSONObject object = new JSONObject(response);
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        data = Arrays.asList(new Gson().fromJson(object.getJSONArray("successData").toString(), MessageListModel[].class));
                        recyler_adapter = new MessageListRecyclerAdapter(getContext(), data);
                        recyler_adapter.setClickListener(MessageListFragment.this);
                        message_fragment_recycler.setAdapter(recyler_adapter);
                        if (data.size() > 0) {
                            message_fragment_recycler.setVisibility(View.VISIBLE);
                            not_found.setVisibility(View.GONE);
                        } else {
                            message_fragment_recycler.setVisibility(View.GONE);
                            not_found.setVisibility(View.VISIBLE);
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiActions == APIActions.ApiActions.message_list) {
            Log.d(TAG, "onRequestSuccess: " + response);
            try {
                JSONObject object = new JSONObject(response);
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        data = Arrays.asList(new Gson().fromJson(object.getJSONArray("successData").toString(), MessageListModel[].class));
                        recyler_adapter = new MessageListRecyclerAdapter(getContext(), data);
                        recyler_adapter.setClickListener(MessageListFragment.this);
                        message_fragment_recycler.setAdapter(recyler_adapter);
                    }
                    if (data.size() > 0) {
                        message_fragment_recycler.setVisibility(View.VISIBLE);
                        not_found.setVisibility(View.GONE);
                    } else {
                        message_fragment_recycler.setVisibility(View.GONE);
                        not_found.setVisibility(View.VISIBLE);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
