package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by incubasyss on 29/01/2018.
 */

public class MySubCategoriesDataModel implements Parcelable {
    private boolean isClicked;
    int id;
    private int user_id;
    private int categorie_id;
    private String sub_title;
    String created_at;
    String updated_at;


    public MySubCategoriesDataModel() {
        isClicked = false;
    }

    public MySubCategoriesDataModel(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCategory_title() {
        return sub_title;
    }

    public void setCategory_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(int categorie_id) {
        this.categorie_id = categorie_id;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isClicked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.categorie_id);
        dest.writeString(this.sub_title);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
    }

    protected MySubCategoriesDataModel(Parcel in) {
        this.isClicked = in.readByte() != 0;
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.categorie_id = in.readInt();
        this.sub_title = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
    }

    public static final Parcelable.Creator<MySubCategoriesDataModel> CREATOR = new Parcelable.Creator<MySubCategoriesDataModel>() {
        @Override
        public MySubCategoriesDataModel createFromParcel(Parcel source) {
            return new MySubCategoriesDataModel(source);
        }

        @Override
        public MySubCategoriesDataModel[] newArray(int size) {
            return new MySubCategoriesDataModel[size];
        }
    };
}
