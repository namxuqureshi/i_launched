package com.dev.codingpixel.i_launched.customui;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dev.codingpixel.i_launched.R;


public class CustomToast {
    public static void ShowCustomToast(Context context, String message, int Toast_Gravity) {
        if (context != null) {
            Toast toast = new Toast(context);
            LinearLayout linearLayout = new LinearLayout(context);
            TextView textView = new TextView(context);
            textView.setBackgroundResource(R.drawable.toast_bg);
            textView.setText(message);
            textView.setTextColor(Color.parseColor("#FFFFFF"));
            textView.setTextSize(14);
            textView.setGravity(Gravity.CENTER);
            textView.setPadding(26, 26, 26, 26);
            linearLayout.setBackgroundColor(Color.TRANSPARENT);
            toast.setView(textView);
            toast.setGravity(Toast_Gravity, 0, 100);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
