package com.dev.codingpixel.i_launched.models;

/**
 * Created by incubasyss on 13/02/2018.
 */

import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastMessage {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName(Constants.THREAD_ID_KEY)
    @Expose
    private Integer threadId;
    @SerializedName("sender_id")
    @Expose
    private Integer senderId;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("is_seem")
    @Expose
    private Integer isSeem;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LastMessage withId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getThreadId() {
        return threadId;
    }

    public void setThreadId(Integer threadId) {
        this.threadId = threadId;
    }

    public LastMessage withThreadId(Integer threadId) {
        this.threadId = threadId;
        return this;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public LastMessage withSenderId(Integer senderId) {
        this.senderId = senderId;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LastMessage withMessage(String message) {
        this.message = message;
        return this;
    }

    public Integer getIsSeem() {
        return isSeem;
    }

    public void setIsSeem(Integer isSeem) {
        this.isSeem = isSeem;
    }

    public LastMessage withIsSeem(Integer isSeem) {
        this.isSeem = isSeem;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public LastMessage withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public LastMessage withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

}