package com.dev.codingpixel.i_launched.activity.splash;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.home.HomeActivity;
import com.dev.codingpixel.i_launched.activity.registration.LoginActivity;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.android.volley.Request.Method.GET;
import static com.dev.codingpixel.i_launched.activity.registration.LoginActivity.isLogin;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.IntentFunction.GoTo;
import static com.dev.codingpixel.i_launched.static_function.UIModification.FullScreen;

public class SplashActivity extends AppCompatActivity implements APIResponseListner {

    private ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FullScreen(SplashActivity.this);
        icon = (ImageView) findViewById(R.id.icon);
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo(
//                    "com.dev.codingpixel.i_launched",
//                    PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
//                Log.d("KeyHashA:", MessageDigest.getInstance("SHA").toString());
//            }
//        } catch (PackageManager.NameNotFoundException e) {
//
//        } catch (NoSuchAlgorithmException e) {
//
//        }

        NextActivity();
        if (SharedPrefrences.savePreference.get("is_remember", Boolean.class, false)) {
            if (SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null) != null) {
                userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);
//                callDashBoardApi();
            }
        }
    }

    /**
     * User Default Api
     */
    void callDashBoardApi() {

        new VollyAPICall(SplashActivity.this
                , false
                , URL.dashboard
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , SplashActivity.this, APIActions.ApiActions.dashboard);
    }

    private void NextActivity() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SharedPrefrences.savePreference.get("is_remember", Boolean.class, false)) {
                    if (SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null) != null) {
                        userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);
                        OneSignal.sendTag("user_id", userLoggedIn.getId() + "");
                        if (getIntent().getExtras() != null && getIntent().getExtras().getBundle("NotificationBundle") != null) {
                            Bundle bundle = getIntent().getExtras().getBundle("NotificationBundle");
                            GoTo(SplashActivity.this, HomeActivity.class, bundle);
                            isLogin = false;

                        } else {
                            isLogin = false;
                            GoTo(SplashActivity.this, HomeActivity.class);
                        }

                    } else {
                        OneSignal.deleteTag("user_id");
                        SharedPrefrences.savePreference.clear();
                        GoTo(SplashActivity.this, LoginActivity.class);
                    }
                } else {
                    OneSignal.deleteTag("user_id");
                    SharedPrefrences.savePreference.clear();
                    GoTo(SplashActivity.this, LoginActivity.class);
                }

                finish();
            }
        }, 2000);
    }


    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == APIActions.ApiActions.dashboard) {

        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {

    }
}
