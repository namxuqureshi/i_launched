package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class HomeDrawerRecylerAdapter extends RecyclerView.Adapter<HomeDrawerRecylerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    List<String> mData = new ArrayList<>();
    private List<Integer> mDataInteger = new ArrayList<>();
    private HomeDrawerRecylerAdapter.ItemClickListener mClickListener;

    public HomeDrawerRecylerAdapter(Context context, List<String> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        for (int i = 0; i < data.size(); i++) {
            mDataInteger.add(i);
        }

    }

    // inflates the cell layout from xml when needed
    @Override
    public HomeDrawerRecylerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.home_drawer_recyler_item, parent, false);
        int row = 11;
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.M) {
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();

            lp.height = parent.getMeasuredHeight() / row;
            int height = parent.getMeasuredHeight() / row;
            if (height > 50) {
                view.setLayoutParams(lp);
            }
            return new ViewHolder(view);
        } else {
            RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
            Log.d("heaight", parent.getMeasuredHeight() / row + "");
            int height = parent.getMeasuredHeight() / row;
            if (height == 0) {
                lp.height = 90;
            } else {
                lp.height = height;
            }
            view.setLayoutParams(lp);
            return new ViewHolder(view);
        }
    }

    // binds the data to the textview in each cell
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(HomeDrawerRecylerAdapter.ViewHolder holder, final int position) {
        holder.Title.setText(mData.get(position));
        if (mData.get(position).equalsIgnoreCase("notifications")) {
            holder.notify_area.setVisibility(View.VISIBLE);
            holder.notify_number.setText(String.valueOf(mDataInteger.get(position)));
            if (mDataInteger.get(position) > 0) {
                holder.notify_area.setVisibility(View.VISIBLE);
            } else {
                holder.notify_area.setVisibility(View.GONE);
            }
        } else {
            holder.notify_area.setVisibility(View.GONE);
        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnTouchListener {
        TextView Title, text_image, notify_number;
        ImageView image_resourse;
        RelativeLayout notify_area;

        public ViewHolder(View itemView) {
            super(itemView);
            Title = itemView.findViewById(R.id.drawer_title);
            text_image = itemView.findViewById(R.id.text_img);
            image_resourse = itemView.findViewById(R.id.img_resourse);
            notify_area = itemView.findViewById(R.id.notify_area);
            notify_number = itemView.findViewById(R.id.notify_number);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemSideMenuClick(view, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    public void setNotifyNumber(int position, int unread) {
        mDataInteger.set(position, unread);
        notifyDataSetChanged();
    }

    // allows clicks events to be caught
    public void setClickListener(HomeDrawerRecylerAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemSideMenuClick(View view, int position);
    }


}