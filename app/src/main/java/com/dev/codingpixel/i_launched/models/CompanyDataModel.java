package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by incubasyss on 29/01/2018.
 */

public class CompanyDataModel implements Parcelable {
    private String name;
    int id;
    private boolean isTick;

    public CompanyDataModel(String name, boolean isTick) {
        this.name = name;
        this.isTick = isTick;
    }

    public CompanyDataModel(String name, int id, boolean isTick) {
        this.name = name;
        this.id = id;
        this.isTick = isTick;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isTick() {
        return isTick;
    }

    public void setTick(boolean tick) {
        isTick = tick;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeInt(this.id);
        dest.writeByte(this.isTick ? (byte) 1 : (byte) 0);
    }

    protected CompanyDataModel(Parcel in) {
        this.name = in.readString();
        this.id = in.readInt();
        this.isTick = in.readByte() != 0;
    }

    public static final Parcelable.Creator<CompanyDataModel> CREATOR = new Parcelable.Creator<CompanyDataModel>() {
        @Override
        public CompanyDataModel createFromParcel(Parcel source) {
            return new CompanyDataModel(source);
        }

        @Override
        public CompanyDataModel[] newArray(int size) {
            return new CompanyDataModel[size];
        }
    };
}
