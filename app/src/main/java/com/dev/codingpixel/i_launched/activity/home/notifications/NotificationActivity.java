package com.dev.codingpixel.i_launched.activity.home.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedCompanyDialouge;
import com.dev.codingpixel.i_launched.adapters.NotificationRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.GetUser;
import com.dev.codingpixel.i_launched.models.Notification;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.UIModification;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.dev.codingpixel.i_launched.utils.TimeUtils;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static com.android.volley.Request.Method.GET;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

// FOR THUMB SWIPE UP AND DOWN LIKE ANDROID NOTIFICATION BAR EXTEND WITH SwipeBackActivity
public class NotificationActivity extends AppCompatActivity implements View.OnClickListener, NotificationRecyclerAdapter.ItemClickListener, APIResponseListner, ILaunchedCompanyDialouge.OnDialogFragmentClickListener {
    ImageView back_btn;
    RecyclerView notification_recycler_view;
    private TextView clear_all;
    NotificationRecyclerAdapter recyler_adapter;
    List<Notification> data = new ArrayList<>();
    GestureDetector gestureScanner;
    public static final String TAG = "NotificationActivity";
    private int pages = 0;
    String url = URL.get_all_notifications + "/" + 0;
    Bundle bundle;
    int position = 0;
    private TextView not_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIModification.FullScreenWithTransparentStatusBar(NotificationActivity.this);
        setContentView(R.layout.activity_notification);
//        FOR THUMB SWIPE UP AND DOWN LIKE ANDROID NOTIFICATION BAR
//        setDragEdge(SwipeBackLayout.DragEdge.BOTTOM);
        bundle = new Bundle();
        initViews();
        initRecyclerView();
        initListener();
        callPaginated(url);
    }

    /**
     * @param url Paginated Api For Notification List
     */
    public void callPaginated(String url) {
        new VollyAPICall(NotificationActivity.this
                , false
                , url
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , NotificationActivity.this, APIActions.ApiActions.get_all_notifications);

    }

    /**
     * Listener
     */
    private void initListener() {
        clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callForReadAllNotification();

            }
        });

    }

    /**
     * clear all notification call
     */
    void callForReadAllNotification() {
        new VollyAPICall(NotificationActivity.this
                , true
                , URL.read_all_notifications
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , NotificationActivity.this, APIActions.ApiActions.read_all_notifications);

    }

    /**
     * Init Recycler View
     */
    private void initRecyclerView() {

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(NotificationActivity.this);
        notification_recycler_view.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator animator = notification_recycler_view.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(true);
        }
        recyler_adapter = new NotificationRecyclerAdapter(NotificationActivity.this, data);
        recyler_adapter.setClickListener(NotificationActivity.this);
        notification_recycler_view.setAdapter(recyler_adapter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notification_recycler_view.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(notification_recycler_view.getLayoutManager());
                    int lastVisible = layoutManager.findLastVisibleItemPosition();
                    if (lastVisible >= (pages + 1) * 9) {
                        if ((lastVisible % 9) == 0) {
                            pages = pages + 1;
                            JSONObject object = new JSONObject();
                            url = URL.get_all_notifications + "/" + pages;
                            callPaginated(url);
                        }
                    }
                }
            });
        } else {
            notification_recycler_view.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    LinearLayoutManager layoutManager = LinearLayoutManager.class.cast(notification_recycler_view.getLayoutManager());
                    int lastVisible = layoutManager.findLastVisibleItemPosition();
                    Log.d("vissible", lastVisible + "");
                    Log.d("vissible_t", (lastVisible % 10) + "");
                    if (lastVisible >= (pages + 1) * 9) {
                        if ((lastVisible % 9) == 0) {
                            pages = pages + 1;
                            JSONObject object = new JSONObject();
                            url = URL.get_all_notifications + "/" + pages;

                            //                        Refresh_layout_progress.setVisibility(View.VISIBLE);
                            //                        new VollyAPICall(getContext(), false, url, object, user.getSession_key(), Request.Method.GET, GroupsMainTabFragment.this, get_groups_with_pagination);
                            callPaginated(url);
                        }
                    }
                }
            });
        }
    }


    /**
     * View ID's
     */
    private void initViews() {
        back_btn = findViewById(R.id.back_btn);
        not_found = findViewById(R.id.not_found);
        clear_all = findViewById(R.id.clear_all);
        notification_recycler_view = findViewById(R.id.notification_recycler_view);
        back_btn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back_btn:
                super.onBackPressed();
//                overridePendingTransition(R.anim.enter_from_top_reverse, R.anim.leave_from_top_reverse);
                overridePendingTransition(R.anim.dialog_appear, R.anim.leave_from_top_reverse);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        overridePendingTransition(R.anim.enter_from_top_reverse, R.anim.leave_from_top_reverse);
        overridePendingTransition(R.anim.dialog_appear, R.anim.leave_from_top_reverse);
    }

    /**
     * @param id for single notification read
     */
    void callForReadNotification(int id) {
        new VollyAPICall(getApplicationContext()
                , false
                , URL.read_notification + "/" + id
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , NotificationActivity.this, APIActions.ApiActions.read_notification);
    }

    @Override
    public void onItemClick(View view, int position) {
        if (data.get(position).getType().equalsIgnoreCase("idea")) {
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                callForReadNotification(data.get(position).getId());
                bundle.putString(Constants.TYPE_KEY, data.get(position).getType());
                bundle.putInt(Constants.POST_ID_KEY, data.get(position).getPostId());
                setResult(RESULT_OK, new Intent().putExtra("result", bundle));
                onBackPressed();
            } else {
                this.position = position;
                ILaunchedCompanyDialouge dialouge = ILaunchedCompanyDialouge.newInstance(NotificationActivity.this, data.get(position).getGet_user().getUsername(), data.get(position).getPostId(), data.get(position).getSender_id());
                dialouge.show(getSupportFragmentManager(), "Reject");

            }
        } else {
            callForReadNotification(data.get(position).getId());
            bundle.putString(Constants.TYPE_KEY, data.get(position).getType());
            bundle.putInt(Constants.POST_ID_KEY, data.get(position).getPostId());
            setResult(RESULT_OK, new Intent().putExtra("result", bundle));
            onBackPressed();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();
    }

    /**
     * For usage Purpose dummy data
     */
    void setDefaultData() {
        List<Notification> listData = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            Notification e = new Notification();
            e.setNotificationText("as");
            if (i == 0)
                e.setDisplayTime(1);
            else
                e.setDisplayTime(0);
            e.setCreatedAt("2010-01-15 12:44:44");
            e.setUpdatedAt("2010-01-15 12:44:44");
            e.setId(0);
            e.setPostId(0);
            e.setSender_id(0);
            e.setType("");
            e.setUserId(0);
            e.setGet_user(new GetUser().withUsername("").withId(0));
            data.add(e);
        }
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (!NotificationActivity.this.isDestroyed()) {
            if (apiActions == APIActions.ApiActions.read_all_notifications) {
                int totalCount = data.size();
                for (int i = 0; i < data.size(); i++) {
                    data.remove(i);

                }
                recyler_adapter.notifyItemRangeRemoved(0, totalCount);

                onBackPressed();
            } else if (apiActions == APIActions.ApiActions.get_all_notifications) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.isNull("status")) {
                        if (object.getString("status").equalsIgnoreCase("success")) {
                            if (pages == 0) {
                                data.clear();
                                recyler_adapter.notifyDataSetChanged();
                            }

                            JSONObject jsonObject = object.getJSONObject("successData");
                            int count = data.size();
                            data.addAll(Arrays.asList(new Gson().fromJson(jsonObject.getJSONArray("notifications").toString(), Notification[].class)));
                            recyler_adapter.notifyItemRangeInserted(count, data.size());
                            int countToday = 0;
                            int countYesterday = 0;
                            int countDaysAgo = 0;
                            int countWeeksAgo = 0;
                            int countMonthAgo = 0;
                            Collections.sort(data, new Comparator<Notification>() {
                                public int compare(Notification obj1, Notification obj2) {
                                    if (obj1.getUpdatedAt() != null && obj2.getUpdatedAt() != null) {
                                        Date ob1Date = TimeUtils.getTimeSort(obj1.getUpdatedAt(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                        Date ob2Date = TimeUtils.getTimeSort(obj2.getUpdatedAt(), new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                                        return ob2Date.compareTo(ob1Date);
                                    } else {
                                        return obj2.getId().compareTo(obj1.getId()); // To compare integer values
                                    }

                                }
                            });
                            for (int i = 0; i < data.size(); i++) {
                                if (!data.get(i).getGet_user().getUsername().equalsIgnoreCase(userLoggedIn.getUsername())) {
                                    data.get(i).setNotificationText(data.get(i).getNotificationText().replace(data.get(i).getGet_user().getUsername(), ""));
                                }
                                if (countToday == 0 &&
                                        (DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("minutes")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("hour")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("minute")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("hours")
                                        )) {
                                    data.get(i).setDisplayTime(1);
                                    countToday = 1;

                                } else if ((countYesterday == 0 || countDaysAgo == 0) &&
                                        (DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("days")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("day")
                                        )) {
                                    if (DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("1 days")
                                            || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("1 day")) {
                                        if (countYesterday == 0) {
                                            data.get(i).setDisplayTime(1);
                                            countYesterday = 1;
                                        } else {
                                            data.get(i).setDisplayTime(0);
                                        }
                                    } else if (countDaysAgo == 0) {
                                        data.get(i).setDisplayTime(1);
                                        countDaysAgo = 1;
                                    } else {
                                        data.get(i).setDisplayTime(0);
                                    }
                                } else if (countWeeksAgo == 0 &&
                                        (DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("weeks")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("week")
                                        )) {
                                    data.get(i).setDisplayTime(1);
                                    countWeeksAgo = 1;
                                } else if (countMonthAgo == 0 &&
                                        (DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("months")
                                                || DateConverter.getPrettyTime(data.get(i).getUpdatedAt()).contains("month")
                                        )) {
                                    data.get(i).setDisplayTime(1);
                                    countMonthAgo = 1;
                                } else {
                                    data.get(i).setDisplayTime(0);
                                }

                            }
//                            setDefaultData();
                            recyler_adapter.notifyItemRangeChanged(count, data.size());

                            if (data.size() > 0) {
                                not_found.setVisibility(View.GONE);
                                notification_recycler_view.setVisibility(View.VISIBLE);
                            } else {
                                not_found.setVisibility(View.VISIBLE);
                                notification_recycler_view.setVisibility(View.GONE);
                            }

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        Log.d(TAG, "onRequestError: " + response);
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onILaunchedCompanyDialougeButtonClick(ILaunchedCompanyDialouge dialog) {
        dialog.dismiss();
        callForReadNotification(data.get(position).getId());
        bundle.putString(Constants.TYPE_KEY, data.get(position).getType());
        bundle.putInt(Constants.POST_ID_KEY, data.get(position).getPostId());
        setResult(RESULT_OK, new Intent().putExtra("result", bundle));
        onBackPressed();
    }

    /**
     * For Swipe Up Gesture class Handling USELESS NOW
     */
    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        //handle 'swipe left' action only

        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2,
                               float velocityX, float velocityY) {

         /*
         Toast.makeText(getBaseContext(),
          event1.toString() + "\n\n" +event2.toString(),
          Toast.LENGTH_SHORT).show();
         */

            if (event2.getY() < event1.getY()) {

                NotificationActivity.this.onBackPressed();
            }

            return true;
        }
    }
}
