package com.dev.codingpixel.i_launched.interfaces;

/**
 * Created by codingpixel on 03/08/2017.
 */

public interface DetectMenuBarHideListner {
    public void HideMenuBar();
    public Void ShowMenuBar();
}
