package com.dev.codingpixel.i_launched.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.MyCategoriesDataModel;

import java.util.ArrayList;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class MyCategoriesRecyclerAdapter extends RecyclerView.Adapter<MyCategoriesRecyclerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    Context mContext;
    ArrayList<MyCategoriesDataModel> mData = new ArrayList<>();
    ArrayList<MyCategoriesDataModel> filterData = new ArrayList<>();
    private MyCategoriesRecyclerAdapter.ItemClickListener mClickListener;

    public MyCategoriesRecyclerAdapter(Context context, ArrayList<MyCategoriesDataModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        filterData.addAll(data);
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.categories_fragment_item, parent, false);
        return new ViewHolder(view);
//        }
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.options.setText(filterData.get(position).getCategory_title());
        if (filterData.get(position).isClicked()) {
            holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg_blue));
        } else {
            holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg));
        }

        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterData.get(position).isClicked()) {
                    filterData.get(position).setClicked(false);
                    holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg));
                } else {
                    filterData.get(position).setClicked(true);
                    holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg_blue));
                }

                if (mClickListener != null) {
                    mClickListener.onItemClick(v, position, filterData.get(position));
                }
                notifyDataSetChanged();

            }
        });

    }

    @Override
    public int getItemCount() {
        return filterData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Button options;

        public ViewHolder(View itemView) {
            super(itemView);

            options = itemView.findViewById(R.id.options);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition(), filterData.get(getAdapterPosition()));
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, MyCategoriesDataModel myCategoriesDataModel);
    }

    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterData.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterData.addAll(mData);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (MyCategoriesDataModel item : mData) {
                        if (item.getCategory_title().toLowerCase().contains(text.toLowerCase())
//                                || item.place.toLowerCase().contains(text.toLowerCase())
                                ) {
                            // Adding Matched items
                            filterData.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

}
