package com.dev.codingpixel.i_launched.static_function;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.home.HomeActivity;
import com.dev.codingpixel.i_launched.activity.home.notifications.NotificationActivity;
import com.dev.codingpixel.i_launched.activity.home.side_menu.messages.MessageChatActivity;

import static com.dev.codingpixel.i_launched.static_function.Constants.NOTIFICATION_RESULT_CODE;

public class IntentFunction {
    public static void GoTo(Context context, Class next_class, Bundle bundle) {
        Intent intent = new Intent(context, next_class);
        intent.putExtra("NotificationBundle", bundle);
//        UIModification.ShowStatusBar((Activity) context);
        context.startActivity(intent);
    }

    public static void GoTo(Activity context, Class next_class) {
        Intent intent = new Intent(context, next_class);
//        context.startActivityForResult(intent, NOTIFICATION_RESULT_CODE);
        context.startActivity(intent);
    }
    public static void GoToCon(Activity context, Class next_class) {
        Intent intent = new Intent(context, next_class);
        context.startActivityForResult(intent, NOTIFICATION_RESULT_CODE);
//        context.startActivity(intent);
    }

    public static void GoToOther(Context context, Class next_class) {
        Intent intent = new Intent(context, next_class);

        context.startActivity(intent);
    }

    public static void GoTopolicy(Context context, Class next_class, boolean isPrivecy) {
        Intent intent = new Intent(context, next_class);
        intent.putExtra("isPrivecy", isPrivecy);
        context.startActivity(intent);
    }

    public static void GoToHome(Context context, boolean isActive) {
        Intent i = new Intent(context, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra("isActive", isActive);
        context.startActivity(i);
    }

    public static void GoToNotification(Activity activity) {
        Intent intent = new Intent(activity, NotificationActivity.class);
        activity.startActivityForResult(intent, NOTIFICATION_RESULT_CODE);
        activity.overridePendingTransition(R.anim.enter_from_top, R.anim.dialog_disappear);

    }

    public static void GoToChatView(Activity activity, int chat_id) {
        Intent intent = new Intent(activity, MessageChatActivity.class);
        intent.putExtra(Constants.THREAD_ID_KEY, chat_id);
        activity.startActivityForResult(intent, NOTIFICATION_RESULT_CODE);
    }

}
