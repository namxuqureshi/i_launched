package com.dev.codingpixel.i_launched.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;

/**
 * Created by jawadali on 12/20/17.
 */

public class CallUser {

    public static void callUserPhone(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static void sendSMS(String phoneNo, String msg) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + phoneNo));
        intent.putExtra("sms_body", "Check out HealingBud for your smartphone. Download it today from http://139.162.37.73/healingbudz/");
        I_LaunchedApplication.getContext().startActivity(intent);
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phoneNo, null, msg, null, null);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }
}
