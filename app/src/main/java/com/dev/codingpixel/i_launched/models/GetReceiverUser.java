package com.dev.codingpixel.i_launched.models;

import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by incubasyss on 13/02/2018.
 */

public class GetReceiverUser {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("company_name")
    @Expose
    private Object companyName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GetReceiverUser withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GetReceiverUser withUsername(String username) {
        this.username = username;
        return this;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public GetReceiverUser withCompanyName(Object companyName) {
        this.companyName = companyName;
        return this;
    }
}
