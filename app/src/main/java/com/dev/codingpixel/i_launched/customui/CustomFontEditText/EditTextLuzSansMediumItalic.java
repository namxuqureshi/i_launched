package com.dev.codingpixel.i_launched.customui.CustomFontEditText;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.dev.codingpixel.i_launched.customui.CustomEditText;
import com.dev.codingpixel.i_launched.customui.FontCache;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class EditTextLuzSansMediumItalic extends CustomEditText {
    public EditTextLuzSansMediumItalic(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public EditTextLuzSansMediumItalic(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public EditTextLuzSansMediumItalic(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    public EditTextLuzSansMediumItalic(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("LuzSans-MediumItalic.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }
}
