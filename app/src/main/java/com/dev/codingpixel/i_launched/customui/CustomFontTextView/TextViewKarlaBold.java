package com.dev.codingpixel.i_launched.customui.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.dev.codingpixel.i_launched.customui.CustomTextView;
import com.dev.codingpixel.i_launched.customui.FontCache;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class TextViewKarlaBold extends CustomTextView {
    public TextViewKarlaBold(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public TextViewKarlaBold(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public TextViewKarlaBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

//    public TextViewKarlaBold(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//        applyCustomFont(context);
//    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("Karla-Bold.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }
}
