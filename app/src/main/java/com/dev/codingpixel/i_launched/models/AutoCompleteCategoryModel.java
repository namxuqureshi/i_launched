package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by incubasyss on 07/02/2018.
 */

public class AutoCompleteCategoryModel implements Parcelable {

    private boolean isClicked;

    int id;//PARANT CAT ID
    private int user_id;

    private String category_title;
    String created_at;
    String updated_at;
    //SUB DETAIL
    private int categorie_id;//SUB CAT ID
    private String sub_title;

    private String name;
    private boolean isSub;

    public AutoCompleteCategoryModel() {
        isClicked = false;
    }

    public AutoCompleteCategoryModel(MyCategoriesDataModel e) {
        isSub = false;
        isClicked = false;

        category_title = e.getCategory_title();
        this.name = e.getCategory_title();
        this.id = e.getId();
        if (e.getCategory_id() > 0) {
            this.categorie_id = e.getCategory_id();
        }

    }

    public AutoCompleteCategoryModel(MySubCategoriesDataModel e) {
        isSub = true;
        isClicked = false;
        sub_title = e.getCategory_title();
        this.name = e.getCategory_title();
        this.id = e.getId();
        this.categorie_id = e.getCategorie_id();
    }

    public AutoCompleteCategoryModel(MySubCategoriesDataModel e, String parent_title) {
        isSub = true;
        isClicked = false;
        category_title = parent_title;
        sub_title = e.getCategory_title();
        this.name = e.getCategory_title();
        this.id = e.getId();
        this.categorie_id = e.getCategorie_id();
    }

    public AutoCompleteCategoryModel(boolean isSub, String name, String created_at, String updated_at, int id, int categorie_id) {
        isClicked = false;
        if (isSub) {
            sub_title = name;
        } else {
            category_title = name;
        }
        this.name = name;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.id = id;
        this.categorie_id = categorie_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isSub() {

        return isSub;
    }

    public void setSub(boolean sub) {
        isSub = sub;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getCategorie_id() {
        return categorie_id;
    }

    public void setCategorie_id(int categorie_id) {
        this.categorie_id = categorie_id;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    @Override
    public String toString() {
        return "AutoCompleteCategoryModel{" +
                "isClicked=" + isClicked +
                ", id=" + id +
                ", user_id=" + user_id +
                ", category_title='" + category_title + '\'' +
                ", created_at='" + created_at + '\'' +
                ", updated_at='" + updated_at + '\'' +
                ", categorie_id=" + categorie_id +
                ", sub_title='" + sub_title + '\'' +
                ", name='" + name + '\'' +
                ", isSub=" + isSub +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isClicked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeString(this.category_title);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
        dest.writeInt(this.categorie_id);
        dest.writeString(this.sub_title);
        dest.writeString(this.name);
        dest.writeByte(this.isSub ? (byte) 1 : (byte) 0);
    }

    protected AutoCompleteCategoryModel(Parcel in) {
        this.isClicked = in.readByte() != 0;
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.category_title = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.categorie_id = in.readInt();
        this.sub_title = in.readString();
        this.name = in.readString();
        this.isSub = in.readByte() != 0;
    }

    public static final Parcelable.Creator<AutoCompleteCategoryModel> CREATOR = new Parcelable.Creator<AutoCompleteCategoryModel>() {
        @Override
        public AutoCompleteCategoryModel createFromParcel(Parcel source) {
            return new AutoCompleteCategoryModel(source);
        }

        @Override
        public AutoCompleteCategoryModel[] newArray(int size) {
            return new AutoCompleteCategoryModel[size];
        }
    };
}
