package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.dev.codingpixel.i_launched.models.Notification;
import com.dev.codingpixel.i_launched.static_function.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class NotificationRecyclerAdapter extends RecyclerView.Adapter<NotificationRecyclerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    List<Notification> mData = new ArrayList<>();
    private NotificationRecyclerAdapter.ItemClickListener mClickListener;

    public NotificationRecyclerAdapter(Context context, List<Notification> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public NotificationRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.notification_recyler_item, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 9;
        view.setLayoutParams(lp);

        NotificationRecyclerAdapter.ViewHolder viewHolder = new NotificationRecyclerAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(NotificationRecyclerAdapter.ViewHolder holder, final int position) {
        Notification temp = mData.get(position);
        if (temp.getDisplayTime() == 1) {
            if (DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("minutes") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("hours") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("minute") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("hour")) {
                holder.days_type.setText("Today");
                holder.days_type.setVisibility(View.VISIBLE);
            } else if (DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("day") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("days")) {
                if (DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("1 day")) {
                    holder.days_type.setText("Yesterday");
                    holder.days_type.setVisibility(View.VISIBLE);
                } else {
                    holder.days_type.setText("Days Ago");
                    holder.days_type.setVisibility(View.VISIBLE);
                }

            } else if (DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("week") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("weeks")) {
                holder.days_type.setText("Weeks Ago");
                holder.days_type.setVisibility(View.VISIBLE);
            } else if (DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("month") ||
                    DateConverter.getPrettyTime(temp.getUpdatedAt()).contains("months")) {
                holder.days_type.setText("Months Ago");
                holder.days_type.setVisibility(View.VISIBLE);
            }


        } else {
            holder.days_type.setText(DateConverter.getPrettyTime(temp.getUpdatedAt()));
            holder.days_type.setVisibility(View.INVISIBLE);
        }

        if (temp.getNotificationText().length() > 0) {
            if (userLoggedIn.getType() == Constants.TYPE_USER) {
                holder.notification_title.setText(Html.fromHtml("<strong><b><font color=#1fbced>" + temp.getGet_user().getUsername() + "</font></b></strong>" + temp.getNotificationText()));
            } else {
                holder.notification_title.setText(Html.fromHtml("<strong><b><font color=#1fbced>" + temp.getGet_user().getUsername() + "</font></b></strong>" + temp.getNotificationText()));
//                holder.notification_title.setText(Html.fromHtml(temp.getNotificationText()));
            }

        }
//        else {
//            holder.days_type.setVisibility(View.INVISIBLE);
//            holder.notification_title.setVisibility(View.INVISIBLE);
//            holder.days_type.setVisibility(View.INVISIBLE);
//            holder.img_resourse.setVisibility(View.INVISIBLE);
//        }

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnTouchListener {
        TextView days_type, notification_title;
        ImageView img_resourse;

        public ViewHolder(View itemView) {
            super(itemView);
            notification_title = itemView.findViewById(R.id.notification_title);
            days_type = itemView.findViewById(R.id.days_type);
            img_resourse = itemView.findViewById(R.id.img_resourse);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    // allows clicks events to be caught
    public void setClickListener(NotificationRecyclerAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}