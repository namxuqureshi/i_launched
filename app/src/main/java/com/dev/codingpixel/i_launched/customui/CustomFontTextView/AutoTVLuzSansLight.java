package com.dev.codingpixel.i_launched.customui.CustomFontTextView;

import android.content.Context;
import android.graphics.Typeface;
import android.text.util.Linkify;
import android.util.AttributeSet;

import com.dev.codingpixel.i_launched.customui.CustomAutoComplete;
import com.dev.codingpixel.i_launched.customui.FontCache;

/**
 * Created by incubasyss on 06/02/2018.
 */

public class AutoTVLuzSansLight extends CustomAutoComplete {
    public AutoTVLuzSansLight(Context context) {
        super(context);
        applyCustomFont(context);
    }

    public AutoTVLuzSansLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }

    public AutoTVLuzSansLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }

    private void applyCustomFont(Context context) {
        Typeface customFont = FontCache.getTypeface("LuzSans-Light.ttf", context);
        setTypeface(customFont, Typeface.NORMAL);
        setLinksClickable(true);
        setAutoLinkMask(Linkify.WEB_URLS);
    }
}
