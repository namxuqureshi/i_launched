package com.dev.codingpixel.i_launched.models;

/**
 * Created by incubasyss on 13/02/2018.
 */


import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessageListModel {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName(Constants.IDEA_ID_KEY)
    @Expose
    private Integer ideaId;
    @SerializedName("sender_id")
    @Expose
    private Integer senderId;
    @SerializedName("receiver_id")
    @Expose
    private Integer receiverId;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("last_message")
    @Expose
    private LastMessage lastMessage;
    @SerializedName("get_user")
    @Expose
    private GetUser getUser;
    @SerializedName("get_receiver_user")
    @Expose
    private GetReceiverUser getReceiverUser;
    @SerializedName("get_ideas")
    @Expose
    private List<GetIdea> getIdeas = null;

    public List<GetIdea> getGetIdeas() {
        return getIdeas;
    }

    public void setGetIdeas(List<GetIdea> getIdeas) {
        this.getIdeas = getIdeas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public MessageListModel withId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getIdeaId() {
        return ideaId;
    }

    public void setIdeaId(Integer ideaId) {
        this.ideaId = ideaId;
    }

    public MessageListModel withIdeaId(Integer ideaId) {
        this.ideaId = ideaId;
        return this;
    }

    public Integer getSenderId() {
        return senderId;
    }

    public void setSenderId(Integer senderId) {
        this.senderId = senderId;
    }

    public MessageListModel withSenderId(Integer senderId) {
        this.senderId = senderId;
        return this;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public MessageListModel withReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public MessageListModel withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public MessageListModel withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public LastMessage getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    public MessageListModel withLastMessage(LastMessage lastMessage) {
        this.lastMessage = lastMessage;
        return this;
    }

    public GetUser getGetUser() {
        return getUser;
    }

    public void setGetUser(GetUser getUser) {
        this.getUser = getUser;
    }

    public MessageListModel withGetUser(GetUser getUser) {
        this.getUser = getUser;
        return this;
    }

    public GetReceiverUser getGetReceiverUser() {
        return getReceiverUser;
    }

    public void setGetReceiverUser(GetReceiverUser getReceiverUser) {
        this.getReceiverUser = getReceiverUser;
    }

    public MessageListModel withGetReceiverUser(GetReceiverUser getReceiverUser) {
        this.getReceiverUser = getReceiverUser;
        return this;
    }
}
