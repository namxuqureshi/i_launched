package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.models.DataModel;

import java.util.ArrayList;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> implements ILaunchedDialouge.OnDialogFragmentClickListener {
    private LayoutInflater mInflater;
    ArrayList<DataModel> mData = new ArrayList<>();
    private CompanyAdapter.ItemClickListener mClickListener;

    public CompanyAdapter(Context context, ArrayList<DataModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.profile_fragment_item, parent, false);

        CompanyAdapter.ViewHolder viewHolder = new CompanyAdapter.ViewHolder(view);
        return viewHolder;
//        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.company_name.setText(mData.get(position).getCompany_name());
        holder.company_name_text.setText(mData.get(position).getShortDescription());
        holder.user_name.setText(mData.get(position).getUser_name());
        holder.description.setText(mData.get(position).getDescription());
        holder.date.setText(mData.get(position).getCreated_at());
        holder.btn_ilaunched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ILaunchedDialouge dialouge = ILaunchedDialouge.newInstance(CompanyAdapter.this);
                dialouge.show(((AppCompatActivity) v.getContext()).getSupportFragmentManager(), "dialog");
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout main_view;
        LinearLayout social_share;
        ImageView user_image, btn_fb_share, btn_ilaunched;
        AppCompatTextView user_name, date, company_name, company_name_text, description;

        public ViewHolder(View itemView) {
            super(itemView);
            user_image = itemView.findViewById(R.id.user_image);
            main_view = itemView.findViewById(R.id.main_view);
            social_share = itemView.findViewById(R.id.social_share);
            user_name = itemView.findViewById(R.id.user_name);
            date = itemView.findViewById(R.id.date);
            company_name = itemView.findViewById(R.id.company_name);
            company_name_text = itemView.findViewById(R.id.company_name_text);
            description = itemView.findViewById(R.id.description);
            btn_fb_share = itemView.findViewById(R.id.btn_fb_share);
            btn_ilaunched = itemView.findViewById(R.id.btn_ilaunched);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
