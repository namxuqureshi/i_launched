package com.dev.codingpixel.i_launched.activity.dialogues;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.static_function.Constants;

import static android.app.Activity.RESULT_OK;
import static com.dev.codingpixel.i_launched.customui.ImageHelper.getRoundedCornerBitmap;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.utils.ViewUtils.checkRotation;

public class WelcomeDialogue extends BaseDialogFragment<WelcomeDialogue.OnDialogFragmentClickListener> {
    ImageView Attached_Image;
    String ImagePAth = "", companyName = "";
    AlertDialog dialog;
    TextView first_text, second_text;
    Button d_sign_nda;
    DatePickerDialog.OnDateSetListener mDateSetListener;

    static OnDialogFragmentClickListener Listener;

    public interface OnDialogFragmentClickListener {
        public void onDoneClick(WelcomeDialogue dialog);
    }

    public static WelcomeDialogue newInstance(OnDialogFragmentClickListener listner)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        WelcomeDialogue frag = new WelcomeDialogue();
        Bundle args = new Bundle();
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View main_dialog = factory.inflate(R.layout.welcome_layout, null);
        dialog = new AlertDialog.Builder(getContext(), R.style.PauseDialog).create();
        dialog.requestWindowFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
        //        int top_y = getResources().getDisplayMetrics().heightPixels / 5;
//        wmlp.y = top_y;   //y position
        ImageView Cross_btn = main_dialog.findViewById(R.id.cross_btn);
        TextView first_text = main_dialog.findViewById(R.id.first_text);
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            first_text.setText("Congratulations! Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $29.99 per month.");
        } else {
            first_text.setText("Congratulations! Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $2.99 per month.");
        }
        Cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        InitContent(main_dialog, dialog);
        dialog.setView(main_dialog);
        return dialog;
    }
//Change to (For Company): 5-day trial is active! Subscribe monthly for only 29.99 per month.
//Change to (For Innovator): 5-day trial is active! Subscribe monthly for only 2.99 per month.

    public void InitContent(View view, final AlertDialog dialog) {
        d_sign_nda = view.findViewById(R.id.d_sign_nda);
        d_sign_nda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Listener.onBuySubscriptionDialogueClick(WelcomeDialogue.this);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1200) {
            ImagePAth = data.getExtras().getString("file_path_arg");
            Log.d("paths", data.getExtras().getString("file_path_arg"));
            Bitmap bitmapOrg = BitmapFactory.decodeFile(data.getExtras().getString("file_path_arg"));
            bitmapOrg = checkRotation(bitmapOrg, data.getExtras().getString("file_path_arg"));
            bitmapOrg = Bitmap.createScaledBitmap(bitmapOrg, 300, 300, false);
            int corner_radious = (bitmapOrg.getWidth() * 10) / 100;
            Bitmap bitmap = getRoundedCornerBitmap(bitmapOrg, corner_radious);
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            Attached_Image.setBackground(drawable);
            Attached_Image.setImageDrawable(null);
        }
    }


}