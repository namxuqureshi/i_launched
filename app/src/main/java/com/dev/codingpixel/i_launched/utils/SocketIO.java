package com.dev.codingpixel.i_launched.utils;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONObject;

import java.net.URISyntaxException;

/**
 * Created by incubasyss on 06/03/2018.
 */

public class SocketIO {
    private Socket mSocket;
    private String SocketReceivedMsgFunctionName = "message_send";
    private String SocketSendMsgFunctionName = "message_get";
    private static final String SOCKET_URL_SITE = "http://139.162.37.73:5001/";

    {
        try {
            mSocket = IO.socket(SOCKET_URL_SITE);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private SocketIO() {
        mSocket.connect();
    }

    public static SocketIO sharedInstance = new SocketIO();

    /**
     * @param object   for data which need to send
     * @param callBack for call back result
     */
    public void Send(JSONObject object, SocketCallBack callBack) {
        sharedInstance.mSocket.emit(SocketSendMsgFunctionName, object);
        callBack.SendCallBack();
    }

    /**
     * @param callBack for giving data back to Activity Or Fragment
     */
    public void Get(final SocketCallBack callBack) {
        sharedInstance.mSocket.on(SocketReceivedMsgFunctionName, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                callBack.GetCallBack(args);
            }
        });
    }

    public interface SocketCallBack {

        /**
         * @param args for parsing the info from socket listener
         */
        public void GetCallBack(Object... args);

        /**
         * for sending call back method
         */
        public void SendCallBack();
    }

}
