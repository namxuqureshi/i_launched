package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.MsgChatModel;
import com.dev.codingpixel.i_launched.utils.DateConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class MsgChatRecyclerAdapter extends RecyclerView.Adapter<MsgChatRecyclerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    List<MsgChatModel> mData = new ArrayList<>();
    private MsgChatRecyclerAdapter.ItemClickListener mClickListener;

    public MsgChatRecyclerAdapter(Context context, List<MsgChatModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public MsgChatRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.msg_recyler_item, parent, false);

        MsgChatRecyclerAdapter.ViewHolder viewHolder = new MsgChatRecyclerAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(MsgChatRecyclerAdapter.ViewHolder holder, final int position) {
        MsgChatModel msgDetail = mData.get(position);
        if (mData.get(position).isSender()) {
            holder.sender.setVisibility(View.VISIBLE);
            holder.receiver.setVisibility(View.GONE);
            holder.dots_layout_sender.setVisibility(View.GONE);
            holder.chat_head_sender.setVisibility(View.VISIBLE);
            holder.sender_msg.setText(msgDetail.getSender_msg());
            holder.sender_date.setText(DateConverter.getPrettyTime(msgDetail.getSender_date()));
        } else {
            holder.receiver.setVisibility(View.VISIBLE);
            holder.sender.setVisibility(View.GONE);
            holder.dots_layout_receiver.setVisibility(View.GONE);
            holder.chat_head_receiver.setVisibility(View.VISIBLE);
            holder.receiver_msg.setText(msgDetail.getReceiver_msg());
            holder.receiver_date.setText(DateConverter.getPrettyTime(msgDetail.getReceiver_date()));
        }

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnTouchListener {

        ImageView receiver_image, sender_image;
        LinearLayout dots_layout_sender, dots_layout_receiver, chat_head_receiver, chat_head_sender;
        //        TextView Lodaing_Dots_text;
//        LoadingDots Lodaing_Dots;
        RelativeLayout receiver, sender;
        TextView receiver_msg, receiver_date, sender_msg, sender_date;

        public ViewHolder(View itemView) {
            super(itemView);
            receiver_image = itemView.findViewById(R.id.receiver_image);
            sender_image = itemView.findViewById(R.id.sender_image);
            dots_layout_sender = itemView.findViewById(R.id.dots_layout_sender);
            dots_layout_receiver = itemView.findViewById(R.id.dots_layout_receiver);
            chat_head_receiver = itemView.findViewById(R.id.chat_head_receiver);
            chat_head_sender = itemView.findViewById(R.id.chat_head_sender);
            receiver = itemView.findViewById(R.id.receiver);
            sender = itemView.findViewById(R.id.sender);
            receiver_msg = itemView.findViewById(R.id.receiver_msg);
            receiver_date = itemView.findViewById(R.id.receiver_date);
            sender_msg = itemView.findViewById(R.id.sender_msg);
            sender_date = itemView.findViewById(R.id.sender_date);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    // allows clicks events to be caught
    public void setClickListener(MsgChatRecyclerAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}