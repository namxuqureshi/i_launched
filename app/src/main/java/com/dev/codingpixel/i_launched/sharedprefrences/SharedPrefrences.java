package com.dev.codingpixel.i_launched.sharedprefrences;

import android.app.Activity;

import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.sharedprefrences.prefrencecustom.ILaunchedPrefrence;

public class SharedPrefrences extends Activity {
    public static final String MY_PREFS_NAME = "i-Launched";
    public static final ILaunchedPrefrence savePreference = new ILaunchedPrefrence(I_LaunchedApplication.getContext().getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE));
    public static UserModel userLoggedIn = new UserModel();
    public static UserModel userNotify = new UserModel();
}
