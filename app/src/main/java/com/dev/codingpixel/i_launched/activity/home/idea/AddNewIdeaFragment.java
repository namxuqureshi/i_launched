package com.dev.codingpixel.i_launched.activity.home.idea;


import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.NDADialouge;
import com.dev.codingpixel.i_launched.adapters.AddNewIdeaRecyclerAdapter;
import com.dev.codingpixel.i_launched.adapters.CategoryAutocompleteListAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.AutoCompleteCategoryModel;
import com.dev.codingpixel.i_launched.models.MyCategoriesDataModel;
import com.dev.codingpixel.i_launched.models.MySubCategoriesDataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.create_idea;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewIdeaFragment extends Fragment implements AddNewIdeaRecyclerAdapter.ItemClickListener, APIResponseListner, CategoryAutocompleteListAdapter.ItemClickListener, NDADialouge.OnDialogFragmentClickListener {

    //    private RecyclerView home_fragment_recycler;
    private BackInterface callBack;
    private ImageView not_found;
    private TopBarData topBarData;
    private RecyclerView categories_fragment_recycler;
    Button save_idea;
    ScrollView scroll_view_add_idea;
    EditText title_idea, detail_description_idea, short_description_idea;
    AppCompatTextView title_idea_count, detail_description_idea_count, short_description_idea_count;
    AddNewIdeaRecyclerAdapter recyler_adapter;
    ArrayList<AutoCompleteCategoryModel> data = new ArrayList<>();
    ArrayList<AutoCompleteCategoryModel> dataSearch = new ArrayList<>();
    AutoCompleteCategoryModel sendModel;
    private AutoCompleteTextView NameOREmail;
    CategoryAutocompleteListAdapter categoryAutocompleteListAdapter;


    public AddNewIdeaFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_new_idea, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        InitRecyclerView(view);
        InitWatcher();
        InitListener();
        setTopView();
        callCategoryApi();
    }

    /**
     * Get All Categopry For AutoComplete Search
     */
    void callCategoryApi() {
        new VollyAPICall(getContext()
                , true
                , URL.all_category
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , AddNewIdeaFragment.this, APIActions.ApiActions.all_category);
    }

    /**
     * Listener Set HERE
     */
    private void InitListener() {

        scroll_view_add_idea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_SCROLL || event.getAction() == MotionEvent.ACTION_MOVE) {
                    HideKeyboard((Activity) v.getContext());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            HideKeyboard((Activity) v.getContext());
                        }
                    }, 100);
                }
                return false;
            }
        });

        save_idea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidFields()) {
                    NDADialouge dialouge = NDADialouge.newInstance(AddNewIdeaFragment.this);
                    dialouge.show(getChildFragmentManager(), "");

                }
            }
        });
    }

    /**
     * @return true or false for if field are valid or not
     */
    public boolean isValidFields() {
        if (title_idea.getText().length() == 0) {
            CustomToast.ShowCustomToast(getContext(), "Title field required!!", Gravity.TOP);
            return false;
        } else if (short_description_idea.getText().length() == 0) {
            CustomToast.ShowCustomToast(getContext(), "Short Description field required!!", Gravity.TOP);
            return false;
        } else if (detail_description_idea.getText().length() == 0) {
            CustomToast.ShowCustomToast(getContext(), "Detail Description field required!!", Gravity.TOP);
            return false;
        } else if (sendModel == null || !sendModel.isClicked()) {
            CustomToast.ShowCustomToast(getContext(), "Select one category is mandatory!!", Gravity.TOP);
            return false;
        }
        return true;
    }

    /**
     * Setting EditText Watcher
     */
    private void InitWatcher() {
        title_idea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                title_idea_count.setText((100 - s.length()) + " char remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        detail_description_idea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                detail_description_idea_count.setText((1500 - s.length()) + " char remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        short_description_idea.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                short_description_idea_count.setText((300 - s.length()) + " char remaining");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        detail_description_idea.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
    }

    /**
     * Setting Top Views Values
     */
    public void setTopView() {
        if (topBarData != null) {
            topBarData.setData("Start Innovating", "My ideas matter, time to start innovating", true, new Bundle());
        }
    }

    /**
     * @param view for setting and gatting ID's
     */
    private void FindId(View view) {
        categories_fragment_recycler = view.findViewById(R.id.categories_fragment_recycler);
        title_idea = view.findViewById(R.id.title_idea);
        detail_description_idea = view.findViewById(R.id.detail_description_idea);
        short_description_idea = view.findViewById(R.id.short_description_idea);
        title_idea_count = view.findViewById(R.id.title_idea_count);
        detail_description_idea_count = view.findViewById(R.id.detail_description_idea_count);
        short_description_idea_count = view.findViewById(R.id.short_description_idea_count);
        save_idea = view.findViewById(R.id.save_idea);
        NameOREmail = view.findViewById(R.id.search_text);
        scroll_view_add_idea = view.findViewById(R.id.scroll_view_add_idea);


    }

    /**
     * @param view setting list recycler for autocomplete
     */
    public void InitRecyclerView(final View view) {

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(view.getContext(), 3);
        categories_fragment_recycler.setLayoutManager(layoutManager);

        recyler_adapter = new AddNewIdeaRecyclerAdapter(view.getContext(), data);
        recyler_adapter.setClickListener(AddNewIdeaFragment.this);
        categories_fragment_recycler.setAdapter(recyler_adapter);
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }


    @Override
    public void onItemClick(View view, int position) {
        if (data.get(position).isClicked()) {
            sendModel = data.get(position);
        } else {
            dataSearch.add(data.get(position));
            data.remove(position);
            recyler_adapter.notifyDataSetChanged();
            categoryAutocompleteListAdapter.notifyDataSetChanged();
            categoryAutocompleteListAdapter = new CategoryAutocompleteListAdapter(getContext(), R.layout.invite_groups_bud_autocomplete_list_item, dataSearch, AddNewIdeaFragment.this, 0);
            NameOREmail.setAdapter(categoryAutocompleteListAdapter);
            NameOREmail.setThreshold(1);
        }
    }


    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == APIActions.ApiActions.all_category) {
            try {
                JSONObject object = new JSONObject(response);
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        JSONArray jsonArray = object.getJSONArray("successData");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MyCategoriesDataModel e = new MyCategoriesDataModel();
                            AutoCompleteCategoryModel search = new AutoCompleteCategoryModel();
                            e.setId(jsonArray.getJSONObject(i).getInt("id"));
                            e.setCreated_at(jsonArray.getJSONObject(i).getString("created_at"));
                            e.setUpdated_at(jsonArray.getJSONObject(i).getString("updated_at"));
                            e.setCategoryTitle(jsonArray.getJSONObject(i).getString("category_title"));
                            e.setSubHas(false);
                            List<MySubCategoriesDataModel> sub_category = new ArrayList<>();
                            JSONObject subArray = jsonArray.getJSONObject(i);
                            JSONArray inArray = subArray.getJSONArray("sub_category");
                            for (int inner = 0; inner < inArray.length(); inner++) {
                                MySubCategoriesDataModel eSub = new MySubCategoriesDataModel();
                                e.setSubHas(true);
                                eSub.setId(inArray.getJSONObject(inner).getInt("id"));
                                eSub.setCategorie_id(inArray.getJSONObject(inner).getInt("categorie_id"));
                                eSub.setCreated_at(inArray.getJSONObject(inner).getString("created_at"));
                                eSub.setUpdated_at(inArray.getJSONObject(inner).getString("updated_at"));
                                eSub.setCategory_title(inArray.getJSONObject(inner).getString("sub_title"));
                                sub_category.add(eSub);
                                search = new AutoCompleteCategoryModel(eSub, e.getCategory_title());
                                dataSearch.add(search);
                            }
                            e.setSub_category(sub_category);
                            search = new AutoCompleteCategoryModel(e);
                            dataSearch.add(search);
                        }
                        recyler_adapter.notifyDataSetChanged();

                        categoryAutocompleteListAdapter = new CategoryAutocompleteListAdapter(getContext(), R.layout.invite_groups_bud_autocomplete_list_item, dataSearch, AddNewIdeaFragment.this, 0);
                        NameOREmail.setAdapter(categoryAutocompleteListAdapter);
                        NameOREmail.setThreshold(1);
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiActions == create_idea) {
//            CustomToast.ShowCustomToast(getContext(), "Idea Created Successfully!!", Gravity.TOP);
            title_idea.setText("");
            short_description_idea.setText("");
            detail_description_idea.setText("");
            data.clear();
            recyler_adapter.notifyDataSetChanged();

            new SweetAlertDialog(getContext(), SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText("SUCCESS!!")
                    .setContentText("Idea Created Successfully!!")
                    .setConfirmText("Ok")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            if (topBarData != null) {
                                topBarData.callTransaction(new Bundle(), Constants.LOAD_HOME_NEW);
                            }
                        }
                    })
                    .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);


        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCategroyTextClick(View view, int position, AutoCompleteCategoryModel categoryDataModel) {
        if (!data.contains(categoryDataModel)) {
            dataSearch.remove(categoryDataModel);
            categoryDataModel.setClicked(true);
            sendModel = categoryDataModel;
            data.add(categoryDataModel);
            if (data.size() > 1) {
                dataSearch.add(data.get(data.size() - 2));
                data.remove(data.size() - 2);
            }
            recyler_adapter.notifyDataSetChanged();
            categoryAutocompleteListAdapter = new CategoryAutocompleteListAdapter(getContext(), R.layout.invite_groups_bud_autocomplete_list_item, dataSearch, AddNewIdeaFragment.this, 0);
            NameOREmail.setAdapter(categoryAutocompleteListAdapter);
            NameOREmail.setThreshold(1);
            HideKeyboard((Activity) view.getContext());
            NameOREmail.setText("");
        }
    }

    /**
     * @param dialog for giving call to create idea
     */
    @Override
    public void onNDAButtonClick(NDADialouge dialog) {
        dialog.dismiss();
        try {
            JSONObject params = new JSONObject();
            params.put("title", title_idea.getText().toString());
            params.put("short_description", short_description_idea.getText().toString());
            params.put("detailed_description", detail_description_idea.getText().toString());
            if (sendModel.isSub()) {
                params.put("sub_cate_id", sendModel.getId());
                params.put("category_id", sendModel.getCategorie_id());
            } else {
                params.put("sub_cate_id", "");
                params.put("category_id", sendModel.getId());
            }
            new VollyAPICall(getContext()
                    , true
                    , URL.create_idea
                    , params
                    , userLoggedIn.getSessionToken()
                    , POST
                    , AddNewIdeaFragment.this, create_idea);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


