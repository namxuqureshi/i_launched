package com.dev.codingpixel.i_launched.eventbus;

/**
 * Created by incubasyss on 16/02/2018.
 */

public class MessageEvent {
    public final Boolean isNotify;

    public MessageEvent(boolean isNotify) {
        this.isNotify = isNotify;
    }

    /* Additional fields if needed */
}
