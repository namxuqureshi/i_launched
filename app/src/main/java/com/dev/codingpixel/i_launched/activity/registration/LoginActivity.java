package com.dev.codingpixel.i_launched.activity.registration;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.CancelSubscriptionDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.HonorPledgeCompanyDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.HonorPledgeDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.ILaunchedDialouge;
import com.dev.codingpixel.i_launched.activity.dialogues.NDADialouge;
import com.dev.codingpixel.i_launched.activity.home.HomeActivity;
import com.dev.codingpixel.i_launched.activity.policy.PolicyActivity;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.Gson;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.IntentFunction.GoToOther;
import static com.dev.codingpixel.i_launched.static_function.IntentFunction.GoTopolicy;
import static com.dev.codingpixel.i_launched.static_function.StaticObjects.emailPattern;
import static com.dev.codingpixel.i_launched.static_function.UIModification.ChangeStatusBarColorTransparent;
import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;

public class LoginActivity extends AppCompatActivity implements HonorPledgeDialouge.OnDialogFragmentClickListener, NDADialouge.OnDialogFragmentClickListener, ILaunchedDialouge.OnDialogFragmentClickListener, CancelSubscriptionDialouge.OnDialogFragmentClickListener, HonorPledgeCompanyDialouge.OnDialogFragmentClickListener, APIResponseListner {
    public static final String TAG = "LOGINACTIVITY";
    public static Boolean isLogin = true;
    private LinearLayout login_view, sign_up_view, remember_me_check, company_check, innovator_check, registration_name_forget, forget_view, registration_name;
    private TextView logIn, signUp, term_service, privacy_service, forget_display;
    private Button btn_login, btn_registor, btn_forget;
    private ImageView remember_me, innovator, company, back_btn;
    boolean isRemember = false, isInnovator = true, isCompany = false;
    public static boolean isFromSignUp = false;
    //Login
    EditText userName, password;
    //SignUp
    EditText userNameSignUp, email, passwordSignUp, confirmPasswordSignUp, phone;
    //FORGET
    EditText user_email_forget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ChangeStatusBarColorTransparent(LoginActivity.this);
        setContentView(R.layout.activity_login);
//        getWindow().setBackgroundDrawableResource(R.drawable.bg_for_all_newq);
        HideKeyboard(LoginActivity.this);
        initViews();
        initClickListener();

    }

    private void initClickListener() {
        forget_display.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration_name_forget.setVisibility(View.VISIBLE);
                forget_view.setVisibility(View.VISIBLE);
                back_btn.setVisibility(View.VISIBLE);
                //
                login_view.setVisibility(View.GONE);
                sign_up_view.setVisibility(View.GONE);
                registration_name.setVisibility(View.GONE);

            }
        });
        btn_forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard((Activity) v.getContext());
                if (isValidForgetFields()) {
                    forgetCallApi();
                }
            }
        });
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp.setTextColor(Color.parseColor("#FFFFFF"));
                logIn.setTextColor(v.getContext().getResources().getColor(R.color.sign_up_off));
                login_view.setVisibility(View.GONE);
                sign_up_view.setVisibility(View.VISIBLE);
            }
        });
        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp.setTextColor(v.getContext().getResources().getColor(R.color.sign_up_off));
                logIn.setTextColor(Color.parseColor("#FFFFFF"));
                login_view.setVisibility(View.VISIBLE);
                sign_up_view.setVisibility(View.GONE);
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HideKeyboard((Activity) v.getContext());
                if (isValidLoginFields()) {
                    logInCallApi();
                }

            }
        });
        btn_registor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidSignUpFields()) {
                    if (isInnovator) {
                        HonorPledgeDialouge sendShootOutAlertDialog = HonorPledgeDialouge.newInstance(LoginActivity.this, userNameSignUp.getText().toString());
                        sendShootOutAlertDialog.show(LoginActivity.this.getSupportFragmentManager(), "dialog");
                    } else if (isCompany) {
                        HonorPledgeCompanyDialouge sendShootOutAlertDialog = HonorPledgeCompanyDialouge.newInstance(LoginActivity.this, userNameSignUp.getText().toString());
                        sendShootOutAlertDialog.show(LoginActivity.this.getSupportFragmentManager(), "dialog");
                    }
                }

            }
        });
        remember_me_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRemember) {
                    isRemember = false;
                    remember_me.setImageDrawable(null);
                } else {
                    isRemember = true;
                    remember_me.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_tick_login_signup));
                }
            }
        });
        innovator_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isInnovator) {

                    isCompany = false;
                    isInnovator = true;
                    innovator.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_tick_login_signup));
                    company.setImageDrawable(null);
                }
            }
        });
        company_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCompany) {
                    isCompany = true;
                    isInnovator = false;
                    company.setImageDrawable(v.getContext().getResources().getDrawable(R.drawable.ic_tick_login_signup));
                    innovator.setImageDrawable(null);
                }
            }
        });

        term_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoTopolicy(LoginActivity.this, PolicyActivity.class, false);
            }
        });
        privacy_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoTopolicy(LoginActivity.this, PolicyActivity.class, true);
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registration_name_forget.setVisibility(View.GONE);
                forget_view.setVisibility(View.GONE);
                back_btn.setVisibility(View.GONE);
                //
                login_view.setVisibility(View.VISIBLE);
//                sign_up_view.setVisibility(View.VISIBLE);
                registration_name.setVisibility(View.VISIBLE);
            }
        });
    }

    //FORGET CALL
    private void forgetCallApi() {
        JSONObject object = new JSONObject();
        try {
            object.put("email", user_email_forget.getText().toString().trim());
            user_email_forget.setText("");
            new VollyAPICall(LoginActivity.this
                    , true
                    , URL.forget_password
                    , object
                    , null
                    , POST
                    , LoginActivity.this, APIActions.ApiActions.forget_password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //LOGIN CALL
    private void logInCallApi() {
        JSONObject object = new JSONObject();

        try {
            object.put("username", userName.getText().toString().trim());
            object.put("password", password.getText().toString().trim());
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            object.put("device_id", android_id);
            object.put("device_type", "Android");
            new VollyAPICall(LoginActivity.this
                    , true
                    , URL.login
                    , object
                    , null
                    , POST
                    , LoginActivity.this, APIActions.ApiActions.login);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //SIGNUP CALL
    private void registrationCallApi() {
        JSONObject object = new JSONObject();
        try {
            object.put("username", userNameSignUp.getText().toString().trim());
            object.put("email", email.getText().toString().trim());
            object.put("password", passwordSignUp.getText().toString().trim());
            object.put("phone", phone.getText().toString().trim());
            @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            object.put("device_id", android_id);
            object.put("device_type", "Android");
            if (isInnovator) {
                object.put(Constants.TYPE_KEY, 0);
            } else if (isCompany) {
                object.put(Constants.TYPE_KEY, 1);
                object.put("company_name", userNameSignUp.getText().toString().trim());
            } else {
                object.put(Constants.TYPE_KEY, 0);
            }

            new VollyAPICall(LoginActivity.this
                    , true
                    , URL.register
                    , object
                    , null
                    , POST
                    , LoginActivity.this, APIActions.ApiActions.register);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (back_btn.getVisibility() == View.VISIBLE) {
            back_btn.performClick();
        } else {
            super.onBackPressed();
        }

    }

    private void initViews() {
        forget_view = findViewById(R.id.forget_view);
        registration_name = findViewById(R.id.registration_name);
        user_email_forget = findViewById(R.id.user_email_forget);
        registration_name_forget = findViewById(R.id.registration_name_forget);
        forget_display = findViewById(R.id.forget_display);
        btn_forget = findViewById(R.id.btn_forget);
        signUp = findViewById(R.id.signup);
        back_btn = findViewById(R.id.back_btn);
        logIn = findViewById(R.id.login);
        login_view = findViewById(R.id.login_view);
        sign_up_view = findViewById(R.id.sign_up_view);
        btn_login = findViewById(R.id.btn_login);
        remember_me_check = findViewById(R.id.remember_me_check);
        remember_me = findViewById(R.id.remember_me);
        company_check = findViewById(R.id.company_check);
        innovator_check = findViewById(R.id.innovator_check);
        innovator = findViewById(R.id.innovator);
        company = findViewById(R.id.company);
        term_service = findViewById(R.id.term_service);
        privacy_service = findViewById(R.id.privacy_service);
        btn_registor = findViewById(R.id.btn_registor);
        password = findViewById(R.id.password_login);
        userName = findViewById(R.id.user_name_login);
        //SignUp
        confirmPasswordSignUp = findViewById(R.id.confirm_password_signup);
        phone = findViewById(R.id.phone_signup);
        passwordSignUp = findViewById(R.id.password_signup);
        email = findViewById(R.id.email_signup);
        userNameSignUp = findViewById(R.id.user_name_signup);

    }

    //LOGIN VALIDATION
    private boolean isValidLoginFields() {
        if (userName.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Username field required!!", Gravity.CENTER);
            return false;
        } else if (password.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Password field required!!", Gravity.CENTER);
            return false;
        } else if (password.getText().toString().trim().length() > 0 && password.getText().toString().trim().length() < 8) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Password must be of 8 characters!!", Gravity.CENTER);
            return false;
        }
        return true;

    }


    //SIGN UP VALIDATION
    private boolean isValidSignUpFields() {
        if (userNameSignUp.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Username field required!!", Gravity.CENTER);
            return false;
        } else if (email.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Email field required!!", Gravity.CENTER);
            return false;
        } else if (phone.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Phone field required!!", Gravity.CENTER);
            return false;
        } else if (passwordSignUp.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Password field required!!", Gravity.CENTER);
            return false;
        } else if (passwordSignUp.getText().toString().trim().length() > 0 && passwordSignUp.getText().toString().trim().length() < 8) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Password must be of 8 characters!!", Gravity.CENTER);
            return false;
        } else if (confirmPasswordSignUp.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Confirm Password field required!!", Gravity.CENTER);
            return false;
        } else if (confirmPasswordSignUp.getText().toString().trim().length() > 0 && confirmPasswordSignUp.getText().toString().trim().length() < 8) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Confirm Password be of 8 characters!!", Gravity.CENTER);
            return false;
        } else if (!confirmPasswordSignUp.getText().toString().trim().equalsIgnoreCase(passwordSignUp.getText().toString().trim())) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Password and Confirm Password does not match!!", Gravity.CENTER);
            return false;
        } else if (email.getText().toString().trim().length() >= 0) {
            if (!email.getText().toString().trim().matches(emailPattern)) {
                CustomToast.ShowCustomToast(LoginActivity.this, "Incorrect email format. Please provide a valid email address!!", Gravity.CENTER);
                return false;
            } else {
                return true;
            }
        }
        return true;

    }

    //FORGET PASSWORD VALIDATION
    private boolean isValidForgetFields() {
        if (user_email_forget.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(LoginActivity.this, "Email field required!!", Gravity.CENTER);
            return false;
        } else if (user_email_forget.getText().toString().trim().length() > 0) {
            if (!user_email_forget.getText().toString().trim().matches(emailPattern)) {
                CustomToast.ShowCustomToast(LoginActivity.this, "Incorrect email format. Please provide a valid email address!!", Gravity.CENTER);
                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    @Override
    public void onHonorPledgeDialougeButtonClick(HonorPledgeDialouge dialog) {
        HideKeyboard((Activity) dialog.getContext());
        dialog.dismiss();
        registrationCallApi();

    }

    @Override
    public void onNDAButtonClick(NDADialouge dialog) {
        dialog.dismiss();
    }


    @Override
    public void onILaunchedButtonClick(ILaunchedDialouge dialog) {

    }

    @Override
    public void onConfirmButtonClick(CancelSubscriptionDialouge dialog) {

    }

    @Override
    public void onHonorPledgeCompanyDialougeButtonClick(HonorPledgeCompanyDialouge dialog) {
        dialog.dismiss();
        registrationCallApi();

    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        Log.d(TAG, "onRequestSuccess: " + response);
        if (apiActions == APIActions.ApiActions.login) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                    UserModel user = new Gson().fromJson(jsonObject.getJSONObject("successData").toString(), UserModel.class);
                    SharedPrefrences.savePreference.put(Constants.USER_SESSION_KEY, user);
                    SharedPrefrences.savePreference.put("is_remember", isRemember);
                    OneSignal.sendTag("user_id", user.getId() + "");
                    userLoggedIn = user;
                    isLogin = true;
                    GoToOther(LoginActivity.this, HomeActivity.class);
                    finish();
                } else if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                    CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage"), Gravity.CENTER);
                }
            } catch (JSONException e) {
                Log.e(TAG, "onRequestSuccess: ", e);
            }
        } else if (apiActions == APIActions.ApiActions.register) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("status")) {
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        UserModel user = new Gson().fromJson(jsonObject.getJSONObject("successData").toString(), UserModel.class);
                        SharedPrefrences.savePreference.put(Constants.USER_SESSION_KEY, user);
                        SharedPrefrences.savePreference.put("is_remember", isRemember);
                        userLoggedIn = user;
                        OneSignal.sendTag("user_id", user.getId() + "");
                        GoToOther(LoginActivity.this, HomeActivity.class);
                        isFromSignUp = true;
                        finish();
                    } else if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                        CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
                    }
                } else {
                    JSONObject errorObject = jsonObject.getJSONObject("errors");
                    if (!errorObject.isNull("username")) {
                        CustomToast.ShowCustomToast(LoginActivity.this, errorObject.getString("username"), Gravity.CENTER);
                    }
                    if (!errorObject.isNull("email")) {
                        CustomToast.ShowCustomToast(LoginActivity.this, errorObject.getString("email"), Gravity.CENTER);
                    }
                }
            } catch (JSONException e) {
                Log.e(TAG, "onRequestSuccess: ", e);
                CustomToast.ShowCustomToast(LoginActivity.this, response, Gravity.CENTER);
            }
        } else if (apiActions == APIActions.ApiActions.forget_password) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);

                if (!jsonObject.isNull("status")) {
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.NORMAL_TYPE)
                                .setTitleText("Success!!")
                                .setContentText(jsonObject.getString("successMessage"))
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        back_btn.performClick();

                                    }
                                })
                                .show();
//                        CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("successMessage"), Gravity.CENTER);
                    } else if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                        CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage"), Gravity.CENTER);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        Log.d(TAG, "onRequestError: " + response);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
