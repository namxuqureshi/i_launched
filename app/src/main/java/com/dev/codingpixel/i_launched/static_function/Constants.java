package com.dev.codingpixel.i_launched.static_function;

/**
 * Created by incubasyss on 09/02/2018.
 */

public class Constants {
    public static final int LOAD_IDEA_DETAIL_FRAGMENT = 1;
    public static final int LOAD_COMPANY_SCREEN_FRAGMENT = 2;
    public static final int LOAD_SEARCH_CATE = 3;
    public static final int BY_CATEGORY_IDEA = 0;
    public static final int MESSAGE_THREAD_USER = 5;
    public static final int NOTIFICATION_RESULT_CODE = 777;
    public static final int LOAD_HOME_NEW = 129;
    public static final int TYPE_COMPANY = 1;
    public static final int TYPE_USER = 0;


    /// KEYS FOR BUNDLE AND PARSING VALUE
    public static final String TYPE_KEY = "type";
    public static final String ID_KEY = "id";
    public static final String IDEA_ID_KEY = "idea_id";
    public static final String IS_COMPANY_KEY = "isCompany";
    public static final String OBJECT_DATA_MODEL_KEY = "objectDataModel";
    public static final String DATA_MODEL_KEY = "DataModel";
    public static final String TAG_KEY = "tag";
    public static final String IS_TAG_KEY = "isTag";
    public static final String IS_BULB_KEY = "isBulb";
    public static final String IS_OTHER_KEY = "isOtherView";
    public static final String IS_BACK_KEY = "isBack";
    public static final String IS_IDEA_KEY = "isIdea";
    public static final String IS_MSG_KEY = "isMsg";
    public static final String USER_SESSION_KEY = "user_session";
    public static final String IS_ALL_IDEAS_KEY = "isAllIdeas";
    public static final String IS_ALL_KEY = "isAll";
    public static final String USER_VIEW_KEY = "user_view";
    public static final String IS_PROFILE_KEY = "isProfile";
    public static final String IS_FAV_ONLY_KEY = "isFavOnly";
    public static final String IS_FAV_KEY = "isFav";
    public static final String IS_SEARCH_CAT_KEY = "isSearchCategory";
    public static final String POST_ID_KEY = "postId";
    public static final String THREAD_ID_KEY = "thread_id";
    public static final String AUTO_COM_CAT_KEY = "AutoCompleteCategoryModel";


    public static final int PAID_USER = 2;
    public static final int UN_PAID_USER_FREE = 0;
    public static final int PAID_USER_CANCEL = 1;
    public static final java.lang.String IS_SEARCH_BY_KEYWORD_KEY = "keyword";
    public static final java.lang.String SEARCH_KEY = "query";


    /**
     * Change Here
     */
    //// IN_APP VAR
    public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq67fkg1M1f/OoUQS5O0OMepYtJICBdQ5vKmfkazSMy+D3reLi0jwWsG5k28oh4BeN2MztHnj4A38i0+9N9hj7gtBhqZp5zh8/7u1WWM0ErpSLUAnUAG/z9S1pLSmN1mfZa+HyYmuccqv5aZw0jv1UdcjUfn5N/I8sDrKXpf7O/6CsJ9JGAZPS4BaVOIZ9yMuQgAzR6E4fVJDnlgry3DdIdAEYeXar5XOC9fhPS7jwUrngj3428C11zlDrMz1/6hY8yGnkW5cXemhow0SVzkftmRTuDXf0Wt8aoPsFd3rxyDfxX5oDA60ci2IpSUp5TiOM0JHP9SUzgZ+DElfcqx7KQIDAQAB";
    /**
     * Change id here if you make change on play console
     */
    public static final String subscriptionId = "codingpixel_test_sub";
//    public static final String subscriptionIdCompany = "idea_company_sub";//i_launched_company
    public static final String subscriptionIdCompany = "idea_company_per_month";//i_launched_company
//    public static final String subscriptionIdUser = "idea_innovator_sub";
    public static final String subscriptionIdUser = "idea_innovator_per_month";
    public static final String subscriptionIdUser_2 = "idea_innovator_sub_2";

}
