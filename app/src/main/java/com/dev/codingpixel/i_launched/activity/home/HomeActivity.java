package com.dev.codingpixel.i_launched.activity.home;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.android.volley.Request;
import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.BuySubscriptionDialogue;
import com.dev.codingpixel.i_launched.activity.dialogues.WelcomeDialogue;
import com.dev.codingpixel.i_launched.activity.home.bottom_fragment.HomeIdeaFragment;
import com.dev.codingpixel.i_launched.activity.home.bottom_fragment.MyCategoriesFourTabFragment;
import com.dev.codingpixel.i_launched.activity.home.bottom_fragment.MyCategoriesFragment;
import com.dev.codingpixel.i_launched.activity.home.idea.AddNewIdeaFragment;
import com.dev.codingpixel.i_launched.activity.home.idea.IdeaDetailFragment;
import com.dev.codingpixel.i_launched.activity.home.side_menu.CompanyScreenFragment;
import com.dev.codingpixel.i_launched.activity.home.side_menu.ContactUsActivity;
import com.dev.codingpixel.i_launched.activity.home.side_menu.MangeSubscriptionFragment;
import com.dev.codingpixel.i_launched.activity.home.side_menu.ProfileFragment;
import com.dev.codingpixel.i_launched.activity.home.side_menu.messages.MessageListFragment;
import com.dev.codingpixel.i_launched.activity.registration.LoginActivity;
import com.dev.codingpixel.i_launched.adapters.HomeDrawerRecylerAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.FontCache;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.eventbus.MessageEvent;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.IntentFunction;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.dev.codingpixel.i_launched.utils.TimeUtils;
import com.dev.codingpixel.i_launched.utils.subscription.util.IabHelper;
import com.facebook.FacebookSdk;
import com.google.gson.Gson;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bolts.AppLinks;

import static android.view.View.GONE;
import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.activity.registration.LoginActivity.isFromSignUp;
import static com.dev.codingpixel.i_launched.activity.registration.LoginActivity.isLogin;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userNotify;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_COMPANY_SCREEN_FRAGMENT;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_IDEA_DETAIL_FRAGMENT;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_SEARCH_CATE;
import static com.dev.codingpixel.i_launched.static_function.Constants.NOTIFICATION_RESULT_CODE;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdCompany;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdUser;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdUser_2;
import static com.dev.codingpixel.i_launched.static_function.IntentFunction.GoTo;
import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;
import static com.dev.codingpixel.i_launched.static_function.UIModification.ShowStatusBar;
import static com.dev.codingpixel.i_launched.utils.subscription.util.IabHelper.BILLING_RESPONSE_RESULT_OK;
import static com.dev.codingpixel.i_launched.utils.subscription.util.IabHelper.RESPONSE_CODE;
import static com.dev.codingpixel.i_launched.utils.subscription.util.IabHelper.RESPONSE_INAPP_PURCHASE_DATA_LIST;

public class HomeActivity extends AppCompatActivity implements HomeDrawerRecylerAdapter.ItemClickListener
        , BackInterface, View.OnClickListener, TopBarData, APIResponseListner
        , BuySubscriptionDialogue.OnDialogFragmentClickListener, WelcomeDialogue.OnDialogFragmentClickListener {
    public DrawerLayout drawerLayout;
    RecyclerView drawer_recyler;
    ImageView menu_btn, home_notification, first, second, four, five, exit_icon, back_btn, idea_image, start_tick, home_search_ic;
    public FragmentTransaction transaction;
    public FragmentManager manager;
    private HomeIdeaFragment homeIdeaFragment;
    private ProfileFragment profileFragment;
    private MyCategoriesFragment myCategoriesFragment;
    private MyCategoriesFourTabFragment myCategoriesFourTabFragment;
    private AddNewIdeaFragment addNewIdeaFragment;
    private MangeSubscriptionFragment mangeSubscriptionFragment;
    private MessageListFragment messageListFragment;
    private IdeaDetailFragment ideaDetailFragment;
    private CompanyScreenFragment companyScreenFragment;
    AppCompatTextView notify_counter;
    AppCompatTextView description_text;
    AppCompatTextView title_text;
    AppCompatTextView phone_text;
    AppCompatTextView detail_name;
    AppCompatTextView text_response;
    AppCompatTextView user_name_text;
    AppCompatTextView date_text;
    AppCompatTextView profile_name;
    AppCompatTextView idea_number;
    AppCompatTextView description_company_text;
    Button Tab_one, Tab_two, Tab_three, Tab_four, Tab_five;
    LinearLayout profile_btn_slide_menu, company_view, search_side_menu;//,user_view;
    RelativeLayout user_view;
    RelativeLayout bottom_bar;
    private static String TAG = "";
    //FLAG FOR FRAGMENT
    boolean isMyCategory = true;
    boolean isFavourite = false;
    HomeDrawerRecylerAdapter recyler_adapter;
    boolean isMyCategoryIdeas = false;
    boolean isPause = false, isBackPressed = true;
    public static final int REQUEST_CODE_DETAIL_SUBSCRIBE = 121215;
    private ImageView iv_ic_search;
    private EditText et_search_side_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        FacebookSdk.sdkInitialize(this);
        if (checkIfSession()) {
            Intent serviceIntent =
                    new Intent("com.android.vending.billing.InAppBillingService.BIND");
            serviceIntent.setPackage("com.android.vending");
            bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);
            userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                getWindow().setStatusBarColor(Color.TRANSPARENT);
            }
            ShowStatusBar(HomeActivity.this);
            InitViews();
            InitDrawerContent();
            InitBottomTab();
            DefineFragmentTransaction();
            firstLoadHome();
            callDashBoardApi();
            if (getIntent().getExtras() != null) {
                Bundle bundleab = getIntent().getExtras().getBundle("NotificationBundle");
                if (bundleab != null) {
                    pushNotificationHandle(bundleab);
                }
            }

            Uri targetUrl =
                    AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
            if (targetUrl != null) {
                Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
            }
            if (isFromSignUp) {
                isFromSignUp = false;
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("Congratulations!")
                            .setContentText("Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $29.99 per month.")
                            .show();
                } else {
                    new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("Congratulations!")
                            .setContentText("Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $2.99 per month.")
                            .show();
                }
//                WelcomeDialogue welcomeDialogue = WelcomeDialogue.newInstance(HomeActivity.this);
//                welcomeDialogue.show(getSupportFragmentManager(), "pd");
//                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
//                    first_text.setText("Congratulations! Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $29.99 per month.");
//                } else {
//                    first_text.setText("Congratulations! Welcome to i-Launched. 5-day active trial is active. Subscribe monthly for only $2.99 per month.");
//                }
            }
        }

    }

    /**
     * For Coming From a Link shared in facebook Check
     */
    private boolean checkIfSession() {
        if (SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null) != null) {
            userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);
            OneSignal.sendTag("user_id", userLoggedIn.getId() + "");
            return true;
        } else {
            OneSignal.deleteTag("user_id");
            SharedPrefrences.savePreference.clear();
            GoTo(HomeActivity.this, LoginActivity.class);
            finishAffinity();
            return false;
        }
    }

    /**
     * First Fragment Load Func
     */
    private void firstLoadHome() {
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
            LoadHomeMainFragment(bundle);
        } else {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
            LoadHomeMainFragment(bundle);
        }
    }


    /**
     * User Default Api
     */
    @SuppressLint("HardwareIds")
    void callDashBoardApi() {
        try {
            new VollyAPICall(HomeActivity.this
                    , false
                    , URL.dashboard
                    , new JSONObject().put("device_id", Settings.Secure.getString(HomeActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID))
                    , userLoggedIn.getSessionToken()
                    , POST
                    , HomeActivity.this, APIActions.ApiActions.dashboard);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPause) {
            isPause = false;
            callDashBoardApi();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        isPause = true;
    }

    /**
     * View By FindById
     */
    private void InitViews() {
        description_company_text = findViewById(R.id.description_company_text);
        iv_ic_search = findViewById(R.id.iv_ic_search);
        et_search_side_bar = findViewById(R.id.et_search_side_bar);
        iv_ic_search.setOnClickListener(this);

        phone_text = findViewById(R.id.phone_text);
        home_notification = findViewById(R.id.home_notification);
        home_notification.setOnClickListener(this);
        title_text = findViewById(R.id.title_text);
        description_text = findViewById(R.id.description_text);
        drawer_recyler = findViewById(R.id.i_launcher_side_me_recycle);
        menu_btn = findViewById(R.id.menu_btn);
        back_btn = findViewById(R.id.back_btn);
        idea_image = findViewById(R.id.idea_image);
        user_view = findViewById(R.id.user_view);
        detail_name = findViewById(R.id.detail_name);
        text_response = findViewById(R.id.text_response);
        company_view = findViewById(R.id.company_view);
        start_tick = findViewById(R.id.start_tick);
        home_search_ic = findViewById(R.id.home_search_ic);
        notify_counter = findViewById(R.id.notify_counter);

        user_name_text = findViewById(R.id.user_name_text);
        date_text = findViewById(R.id.date_text);
        profile_name = findViewById(R.id.profile_name);
        idea_number = findViewById(R.id.idea_number);
        idea_number.setOnClickListener(this);
        /// bottom bar
        Tab_one = findViewById(R.id.tab_one);
        Tab_two = findViewById(R.id.tab_two);
        Tab_three = findViewById(R.id.tab_three);
        Tab_four = findViewById(R.id.tab_four);
        Tab_five = findViewById(R.id.tab_five);
        ///Indicator Icons
        first = findViewById(R.id.first);
        second = findViewById(R.id.second);
        four = findViewById(R.id.four);
        five = findViewById(R.id.five);
        //side menu bottom icon
        bottom_bar = findViewById(R.id.bottom_bar);
        search_side_menu = findViewById(R.id.search_side_menu);
        profile_btn_slide_menu = findViewById(R.id.profile_btn_slide_menu);
        profile_btn_slide_menu.setOnClickListener(this);
        exit_icon = findViewById(R.id.exit_icon);
        exit_icon.setOnClickListener(this);


        //SET VIEW VISIBLE AND GONE FOR USER AND COMPANY
//        if(userLoggedIn.getType()==Constants.TYPE_COMPANY){
//            search_side_menu.setVisibility(View.VISIBLE);
//        }else {
//            search_side_menu.setVisibility(View.GONE);
//        }
        et_search_side_bar.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (et_search_side_bar.getText().toString().trim().length() > 0) {
                        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                            callForSearchSideMenu();
                        }
                    }
                }
                return false;
            }
        });
    }

    /**
     * func for searching idea by name from side menu
     */
    private void callForSearchSideMenu() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IS_SEARCH_BY_KEYWORD_KEY, true);
        bundle.putString(Constants.SEARCH_KEY, et_search_side_bar.getText().toString().trim());
        LoadHomeMainFragment(bundle);
        tabOneClicked();
        drawerLayout.closeDrawer(Gravity.START);
        et_search_side_bar.setText("");
    }

    /**
     * @param bundle for setting argument and call the home fragment
     */
    public void LoadHomeMainFragment(Bundle bundle) {
        DefineFragmentTransaction();

        if (manager.getBackStackEntryCount() != 0) {
            manager.popBackStackImmediate();
        }
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isAdded()) {
                transaction.remove(profileFragment);
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isAdded()) {
                transaction.remove(myCategoriesFragment);
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isAdded()) {
                transaction.remove(myCategoriesFourTabFragment);
            }
        }
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isAdded()) {
                transaction.remove(addNewIdeaFragment);
            }
        }
        backButtonFragment();
        setHomeFragmentBundle(bundle);

        transaction.add(R.id.fragment_id, homeIdeaFragment, "home");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the profile fragment
     */
    private void LoadProfileFragment(Bundle bundle) {
        DefineFragmentTransaction();
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isAdded()) {
                transaction.remove(addNewIdeaFragment);
            }
        }
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isAdded()) {
                transaction.remove(profileFragment);
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isAdded()) {
                transaction.remove(myCategoriesFragment);
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isAdded()) {
                transaction.remove(myCategoriesFourTabFragment);
            }
        }
        //
        backButtonFragment();
        setProfileFragmentBundle(bundle);

        transaction.add(R.id.fragment_id, profileFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the myCategory fragment
     */
    private void LoadMyCategoriesFragment(Bundle bundle) {
        DefineFragmentTransaction();
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isAdded()) {
                transaction.remove(addNewIdeaFragment);
            }
        }
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isAdded()) {
                transaction.remove(profileFragment);
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isAdded()) {
                transaction.remove(myCategoriesFragment);
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isAdded()) {
                transaction.remove(myCategoriesFourTabFragment);
            }
        }
        //back buttn fragment
        backButtonFragment();
        setMyCategoriesFragmentBundle(bundle);

        transaction.add(R.id.fragment_id, myCategoriesFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    private void LoadMyCategoriesTabFourFragment(Bundle bundle) {
        DefineFragmentTransaction();
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isAdded()) {
                transaction.remove(addNewIdeaFragment);
            }
        }
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isAdded()) {
                transaction.remove(profileFragment);
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isAdded()) {
                transaction.remove(myCategoriesFragment);
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isAdded()) {
                transaction.remove(myCategoriesFourTabFragment);
            }
        }
        //back buttn fragment
        backButtonFragment();
        setMyCategoriesTabFourFragmentBundle(bundle);

        transaction.add(R.id.fragment_id, myCategoriesFourTabFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the AddNewIdea fragment
     */
    private void LoadAddNewIdeaFragment(Bundle bundle) {
        DefineFragmentTransaction();
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isAdded()) {
                transaction.remove(homeIdeaFragment);
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isAdded()) {
                transaction.remove(profileFragment);
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isAdded()) {
                transaction.remove(myCategoriesFragment);
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isAdded()) {
                transaction.remove(myCategoriesFourTabFragment);
            }
        }
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isAdded()) {
                transaction.remove(addNewIdeaFragment);
            }
        }
        //
        backButtonFragment();
        setAddNewIdeaFragmentBundle(bundle);
        transaction.add(R.id.fragment_id, addNewIdeaFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the Subscription fragment
     */
    private void LoadMangeSubscriptionFragment(Bundle bundle) {
        DefineFragmentTransaction();
        if (mangeSubscriptionFragment != null) {
            if (mangeSubscriptionFragment.isAdded()) {
                transaction.remove(mangeSubscriptionFragment);
            }
        }
        setMangeSubscriptionFragmentBundle(bundle);
        transaction.add(R.id.fragment_id, mangeSubscriptionFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the MessageList fragment
     */
    private void LoadMessageListFragment(Bundle bundle) {
        DefineFragmentTransaction();

        if (messageListFragment != null) {
            if (messageListFragment.isAdded()) {
                transaction.remove(messageListFragment);
            }
        }
        setMessageListFragmentBundle(bundle);
        transaction.add(R.id.fragment_id, messageListFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the IdeaDetail fragment
     */
    private void LoadIdeaDetailFragment(Bundle bundle) {
        DefineFragmentTransaction();

        if (ideaDetailFragment != null) {
            if (ideaDetailFragment.isAdded()) {
                transaction.remove(ideaDetailFragment);
            }
        }
        setIdeaDetailFragmentBundle(bundle);
        transaction.setCustomAnimations(R.anim.attatch_fragment_right_to_left, R.anim.attatch_fragment_right_to_left);
        transaction.add(R.id.fragment_id, ideaDetailFragment, "profile");
        transaction.commitAllowingStateLoss();
    }

    /**
     * @param bundle for setting argument and call the Company Profile For User fragment
     */
    private void LoadCompanyScreenFragment(Bundle bundle) {
        DefineFragmentTransaction();

        if (companyScreenFragment != null) {
            if (companyScreenFragment.isAdded()) {
                transaction.remove(companyScreenFragment);
            }
        }
        setCompanyScreenFragmentBundle(bundle);
        transaction.setCustomAnimations(R.anim.attatch_fragment_right_to_left, R.anim.attatch_fragment_right_to_left);
        transaction.add(R.id.fragment_id, companyScreenFragment, "profile");
        transaction.commitAllowingStateLoss();
    }


    /**
     * Add Back Button Fragment Here
     */
    private void backButtonFragment() {
        if (mangeSubscriptionFragment != null) {
            if (mangeSubscriptionFragment.isAdded() || mangeSubscriptionFragment.isVisible()) {
                transaction.remove(mangeSubscriptionFragment);
            }
        }
        if (messageListFragment != null) {
            if (messageListFragment.isAdded() || messageListFragment.isVisible()) {
                transaction.remove(messageListFragment);
            }
        }
        if (ideaDetailFragment != null) {
            if (ideaDetailFragment.isAdded() || ideaDetailFragment.isVisible()) {
                transaction.remove(ideaDetailFragment);
            }
        }
        if (companyScreenFragment != null) {
            if (companyScreenFragment.isAdded() || companyScreenFragment.isVisible()) {
                transaction.remove(companyScreenFragment);
            }
        }
    }

    /**
     * @param bundle setting argument
     */
    private void setHomeFragmentBundle(Bundle bundle) {
        homeIdeaFragment = new HomeIdeaFragment();
        homeIdeaFragment.SetBackListener(HomeActivity.this);
        homeIdeaFragment.SetTopData(HomeActivity.this);
        homeIdeaFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setProfileFragmentBundle(Bundle bundle) {
        profileFragment = new ProfileFragment();
        profileFragment.SetBackListener(HomeActivity.this);
        profileFragment.SetTopData(HomeActivity.this);

        profileFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setMyCategoriesFragmentBundle(Bundle bundle) {
        myCategoriesFragment = new MyCategoriesFragment();
        myCategoriesFragment.SetBackListener(HomeActivity.this);
        myCategoriesFragment.SetTopData(HomeActivity.this);

        myCategoriesFragment.setArguments(bundle);

    }

    private void setMyCategoriesTabFourFragmentBundle(Bundle bundle) {
        myCategoriesFourTabFragment = new MyCategoriesFourTabFragment();
        myCategoriesFourTabFragment.SetBackListener(HomeActivity.this);
        myCategoriesFourTabFragment.SetTopData(HomeActivity.this);

        myCategoriesFourTabFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setAddNewIdeaFragmentBundle(Bundle bundle) {
        addNewIdeaFragment = new AddNewIdeaFragment();
        addNewIdeaFragment.SetBackListener(HomeActivity.this);
        addNewIdeaFragment.SetTopData(HomeActivity.this);

        addNewIdeaFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setMangeSubscriptionFragmentBundle(Bundle bundle) {
        mangeSubscriptionFragment = new MangeSubscriptionFragment();
        mangeSubscriptionFragment.SetBackListener(HomeActivity.this);
        mangeSubscriptionFragment.SetTopData(HomeActivity.this);

        mangeSubscriptionFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setMessageListFragmentBundle(Bundle bundle) {
        messageListFragment = new MessageListFragment();
        messageListFragment.SetBackListener(HomeActivity.this);
        messageListFragment.SetTopData(HomeActivity.this);

        messageListFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setIdeaDetailFragmentBundle(Bundle bundle) {
        ideaDetailFragment = new IdeaDetailFragment();
        ideaDetailFragment.SetBackListener(HomeActivity.this);
        ideaDetailFragment.SetTopData(HomeActivity.this);
        ideaDetailFragment.setArguments(bundle);

    }

    /**
     * @param bundle setting argument
     */
    private void setCompanyScreenFragmentBundle(Bundle bundle) {
        companyScreenFragment = new CompanyScreenFragment();
        companyScreenFragment.SetBackListener(HomeActivity.this);
        companyScreenFragment.SetTopData(HomeActivity.this);

        companyScreenFragment.setArguments(bundle);

    }

    /**
     * Setting Side Menu Drawer
     */
    public void InitDrawerContent() {
        drawerLayout = findViewById(R.id.my_drawer_layout);
        drawerLayout.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                callDashBoardApi();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
        new Handler().postDelayed(new Runnable() {
            public void run() {
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomeActivity.this);
                drawer_recyler.setLayoutManager(layoutManager);
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    recyler_adapter = new HomeDrawerRecylerAdapter(HomeActivity.this, home_drawer_data_company());
                } else {
                    recyler_adapter = new HomeDrawerRecylerAdapter(HomeActivity.this, home_drawer_data_user());
                }
                recyler_adapter.setClickListener(HomeActivity.this);
                drawer_recyler.setAdapter(recyler_adapter);
            }
        }, 200);
    }

    /**
     * @param view     CURRENT VIEW CLICKED
     * @param position ITEM POSITION
     *                 SIDE MENU ITEMS CLICK HANDLER
     */
    @Override
    public void onItemSideMenuClick(View view, int position) {
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            switch (position) {
                case 0:
                    drawerLayout.closeDrawer(Gravity.START);
                    Tab_one.performClick();
                    tabOneClicked();
                    break;
                case 1:
                    drawerLayout.closeDrawer(Gravity.START);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.IS_ALL_KEY, false);
                    if (myCategoriesFragment != null && !myCategoriesFragment.isVisible()) {
                        LoadMyCategoriesFragment(bundle);
                    } else if (myCategoriesFragment == null) {
                        LoadMyCategoriesFragment(bundle);
                    } else if (isMyCategory) {
                        isMyCategory = false;
                        LoadMyCategoriesFragment(bundle);
                    }
                    tabTwoClicked();
                    break;
                case 2:
                    drawerLayout.closeDrawer(Gravity.START);
                    if (profileFragment != null && !profileFragment.isVisible()) {
                        LoadProfileFragment(new Bundle());
                    } else if (profileFragment == null) {
                        LoadProfileFragment(new Bundle());
                    }
                    tabFiveClicked();
                    break;
                case 3:
                    home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                    notify_counter.setVisibility(View.GONE);
                    drawerLayout.closeDrawer(Gravity.START);
                    IntentFunction.GoToNotification(HomeActivity.this);
                    break;
                case 4:
                    drawerLayout.closeDrawer(Gravity.START);
                    if (userLoggedIn.getType() == Constants.TYPE_COMPANY && userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                        Bundle bundleMsg = new Bundle();
                        bundleMsg.putBoolean(Constants.IS_COMPANY_KEY, true);
                        LoadMessageListFragment(bundleMsg);
                        tabOneClicked();
                    } else {
                        callSubscriptionDialogue();
                        //NEED TO WORk
                    }
                    break;
                case 5:
                    drawerLayout.closeDrawer(Gravity.START);
                    break;
                case 6:
                    //
                    drawerLayout.closeDrawer(Gravity.START);
                    LoadMangeSubscriptionFragment(new Bundle());
                    tabThreeClicked();
                    break;
                case 7:
                    drawerLayout.closeDrawer(Gravity.START);
                    IntentFunction.GoToCon(HomeActivity.this, ContactUsActivity.class);
                    break;
                default:
                    break;
            }
        } else if (userLoggedIn.getType() == Constants.TYPE_USER) {
            switch (position) {
                case 0:
                    drawerLayout.closeDrawer(Gravity.START);
                    Tab_one.performClick();
                    tabOneClicked();
                    break;
                case 1:
                    drawerLayout.closeDrawer(Gravity.START);
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(Constants.IS_ALL_KEY, false);
                    if (myCategoriesFragment != null && !myCategoriesFragment.isVisible()) {
                        LoadMyCategoriesFragment(bundle);
                    } else if (myCategoriesFragment == null) {
                        LoadMyCategoriesFragment(bundle);
                    } else if (isMyCategory) {
                        isMyCategory = false;
                        LoadMyCategoriesFragment(bundle);
                    }
                    tabTwoClicked();
                    break;
                case 2:
                    drawerLayout.closeDrawer(Gravity.START);
                    if (profileFragment != null && !profileFragment.isVisible()) {
                        LoadProfileFragment(new Bundle());
                    } else if (profileFragment == null) {
                        LoadProfileFragment(new Bundle());
                    }
                    tabFiveClicked();
                    break;
                case 3:
                    home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                    notify_counter.setVisibility(View.GONE);
                    drawerLayout.closeDrawer(Gravity.START);
                    IntentFunction.GoToNotification(HomeActivity.this);
                    break;
                case 4:
                    drawerLayout.closeDrawer(Gravity.START);
                    break;
                case 5:
                    //
                    drawerLayout.closeDrawer(Gravity.START);
                    LoadMangeSubscriptionFragment(new Bundle());
                    tabThreeClicked();
                    //WORK FOR SUBSCRIPTION
//                    Bundle bundleSub = null;
//                    try {
//                        bundleSub = mService.getBuyIntent(3, "com.example.myapp",
//                                "IN_APP_25", "subs", "");
//                        IntentSender pendingIntent = bundleSub.getParcelable(RESPONSE_BUY_INTENT);
//                        if (bundleSub.getInt(RESPONSE_CODE) == BILLING_RESPONSE_RESULT_OK) {
//                            // Start purchase flow (this brings up the Google Play UI).
//                            // Result will be delivered through onActivityResult().
//                            try {
//                                startIntentSenderForResult(pendingIntent, REQUEST_CODE_DETAIL_SUBSCRIBE, new Intent(),
//                                        Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
//                            } catch (IntentSender.SendIntentException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    } catch (RemoteException e) {
//                        e.printStackTrace();
//                    }


                    break;
                case 6:
                    drawerLayout.closeDrawer(Gravity.START);
                    IntentFunction.GoToCon(HomeActivity.this, ContactUsActivity.class);
                    break;
                default:
                    break;
            }
        }

    }

    /**
     * @return SIDE DATA POPULATE FOR COMPANY
     */
    public static List<String> home_drawer_data_company() {

        List<String> mDataDrawer = new ArrayList<>();
        mDataDrawer.add("Home");
        mDataDrawer.add("My Categories");
        mDataDrawer.add("Profile");
        mDataDrawer.add("Notifications");
        mDataDrawer.add("Messages");
        mDataDrawer.add("Ideas Launched (Purchased)");
//        mDataDrawer.add("");
        mDataDrawer.add("Manage Subscriptions");
        mDataDrawer.add("Contact Us");

        return mDataDrawer;
    }

    /**
     * @return SIDE DATA POPULATE FOR USER
     */
    public static List<String> home_drawer_data_user() {

        List<String> mDataDrawer = new ArrayList<>();
        mDataDrawer.add("Home");
        mDataDrawer.add("My Categories");
        mDataDrawer.add("Profile");
        mDataDrawer.add("Notifications");
//        mDataDrawer.add("Messages");
        mDataDrawer.add("Ideas Launched (Sold)");
//        mDataDrawer.add("");
        mDataDrawer.add("Manage Subscriptions");
        mDataDrawer.add("Contact Us");

        return mDataDrawer;
    }

    /**
     * Call For Transaction
     */
    public void DefineFragmentTransaction() {

        manager = getSupportFragmentManager();
        transaction = manager.beginTransaction();
        transaction.setAllowOptimization(true);
//        transaction.disallowAddToBackStack();

    }

    @Override
    public void callBack() {
        // NOT IN USER
    }

    /**
     * Setting Bottom Click Listener
     */
    public void InitBottomTab() {

        Tab_one.setOnClickListener(this);
        Tab_two.setOnClickListener(this);
        Tab_three.setOnClickListener(this);
        Tab_four.setOnClickListener(this);
        Tab_five.setOnClickListener(this);
        menu_btn.setOnClickListener(this);
        back_btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_ic_search:
                if (et_search_side_bar.getText().toString().trim().length() > 0) {
                    if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                        callForSearchSideMenu();
                    }
                }
                break;
            case R.id.menu_btn:
                HideKeyboard(this);
                drawerLayout.openDrawer(Gravity.START);
                break;

            case R.id.tab_one:
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabOneClicked();
                        if (homeIdeaFragment != null && !homeIdeaFragment.isVisible()) {
                            if (isFavourite) {
                                isFavourite = false;
                                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
                                    LoadHomeMainFragment(bundle);
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                                    LoadHomeMainFragment(bundle);
                                }
                            } else if (isMyCategoryIdeas) {
                                isMyCategoryIdeas = false;
                                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
                                    LoadHomeMainFragment(bundle);
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                                    LoadHomeMainFragment(bundle);
                                }
                            } else if (homeIdeaFragment != null && !homeIdeaFragment.isVisible()) {
                                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
                                    LoadHomeMainFragment(bundle);
                                } else {
                                    Bundle bundle = new Bundle();
                                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                                    LoadHomeMainFragment(bundle);
                                }
                            }
                        } else if (homeIdeaFragment != null && homeIdeaFragment.isVisible()) {
                            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
                                LoadHomeMainFragment(bundle);
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                                LoadHomeMainFragment(bundle);
                            }
                        }
                    }
                });
                break;
            case R.id.tab_two:
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabTwoClicked();
                        if (userLoggedIn.getType() == Constants.TYPE_USER) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.IS_ALL_KEY, false);
                            if (myCategoriesFragment != null && !myCategoriesFragment.isVisible()) {
                                LoadMyCategoriesFragment(bundle);
                            } else if (myCategoriesFragment == null) {
                                LoadMyCategoriesFragment(bundle);
                            } else if (isMyCategory) {
                                isMyCategory = false;
                                LoadMyCategoriesFragment(bundle);
                            }
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.IS_ALL_KEY, true);
                            if (myCategoriesFragment != null && !myCategoriesFragment.isVisible()) {
                                LoadMyCategoriesFragment(bundle);
                            } else if (myCategoriesFragment == null) {
                                LoadMyCategoriesFragment(bundle);
                            } else if (!isMyCategory) {
                                isMyCategory = true;
                                LoadMyCategoriesFragment(bundle);
                            }
                        }

                    }
                });
                break;
            case R.id.tab_three:
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callForTrialOver()) {
                            if (userLoggedIn.getType() != Constants.TYPE_COMPANY) {
                                tabThreeClicked();
                                if (addNewIdeaFragment != null && !addNewIdeaFragment.isVisible()) {
                                    LoadAddNewIdeaFragment(new Bundle());
                                } else if (addNewIdeaFragment == null) {
                                    LoadAddNewIdeaFragment(new Bundle());
                                }

                            } else {
//                                CustomToast.ShowCustomToast(HomeActivity.this, "You can't create idea.", Gravity.CENTER);
                                onClick(findViewById(R.id.tab_one));
//                                tabThreeClicked();
                            }
                        } else {
                            callSubscriptionDialogue();
                        }
                    }
                });
                break;
            case R.id.tab_four:
//                HideKeyboard(this);
//                drawerLayout.closeDrawer(Gravity.START);
//                HomeActivity.this.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        tabFourClicked();
//                    }
//                });
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabFourClicked();
                        if (userLoggedIn.getType() == Constants.TYPE_USER) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.IS_ALL_KEY, true);
                            if (myCategoriesFourTabFragment != null && !myCategoriesFourTabFragment.isVisible()) {
                                LoadMyCategoriesTabFourFragment(bundle);
                            } else if (myCategoriesFourTabFragment == null) {
                                LoadMyCategoriesTabFourFragment(bundle);
                            } else if (isMyCategory) {
                                isMyCategory = false;
                                LoadMyCategoriesTabFourFragment(bundle);
                            }
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean(Constants.IS_ALL_KEY, true);
                            if (myCategoriesFourTabFragment != null && !myCategoriesFourTabFragment.isVisible()) {
                                LoadMyCategoriesTabFourFragment(bundle);
                            } else if (myCategoriesFourTabFragment == null) {
                                LoadMyCategoriesTabFourFragment(bundle);
                            } else if (!isMyCategory) {
                                isMyCategory = true;
                                LoadMyCategoriesTabFourFragment(bundle);
                            }
                        }

                    }
                });
                break;
            case R.id.tab_five:
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabFiveClicked();
                        if (profileFragment != null && !profileFragment.isVisible()) {
                            LoadProfileFragment(new Bundle());
                        } else if (profileFragment == null) {
                            LoadProfileFragment(new Bundle());
                        }
                    }
                });
                break;
            case R.id.home_notification:
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                notify_counter.setVisibility(View.GONE);
                drawerLayout.closeDrawer(Gravity.START);
                IntentFunction.GoToNotification(HomeActivity.this);
                break;
            case R.id.exit_icon:
                drawerLayout.closeDrawer(Gravity.START);
                try {
                    new VollyAPICall(HomeActivity.this
                            , true
                            , URL.logout
                            , new JSONObject().put("device_id", Settings.Secure.getString(HomeActivity.this.getContentResolver(),
                            Settings.Secure.ANDROID_ID))
                            , userLoggedIn.getSessionToken()
                            , POST
                            , HomeActivity.this, APIActions.ApiActions.logout);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//                finish();
                break;
            case R.id.profile_btn_slide_menu:
                HideKeyboard(this);
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabFiveClicked();
                        if (profileFragment != null && !profileFragment.isVisible()) {
                            LoadProfileFragment(new Bundle());
                        } else if (profileFragment == null) {
                            LoadProfileFragment(new Bundle());
                        }
                    }
                });
                break;
            case R.id.back_btn:
                HideKeyboard(this);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        HomeActivity.this.onBackPressed();
//                        backHandle(TAG);
                    }
                });
                break;
            case R.id.start_tick:
                if (start_tick.getTag().equals("star_fill")) {
                    start_tick.setTag("star_unfill");
                    start_tick.setImageDrawable(start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_unfill));
                } else {
                    start_tick.setTag("star_fill");
                    start_tick.setImageDrawable(start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_fill));
                }
                break;
            case R.id.idea_number:
                HideKeyboard(this);
                isFavourite = true;
                drawerLayout.closeDrawer(Gravity.START);
                HomeActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tabOneClicked();
                        if (homeIdeaFragment != null) {
                            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.IS_FAV_ONLY_KEY, true);
                                LoadHomeMainFragment(bundle);
                            } else {
                                Bundle bundle = new Bundle();
                                bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                                LoadHomeMainFragment(bundle);
                            }
                        }
                    }
                });
                break;

        }
    }

    public void tabOneClicked() {
        setBottomBarDefault();
        first.setVisibility(View.VISIBLE);
        second.setVisibility(GONE);
        four.setVisibility(GONE);
        five.setVisibility(GONE);

    }

    /**
     * Blue Line Bottom Bar func
     */
    private void setBottomBarBlue() {

        bottom_bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bottom_bar_background_blue_curve));
    }

    /**
     * Default Line Bottom Bar func
     */
    private void setBottomBarDefault() {

        bottom_bar.setBackgroundDrawable(getResources().getDrawable(R.drawable.bottom_bar_background));
    }

    public void tabTwoClicked() {

        setBottomBarDefault();
        first.setVisibility(GONE);
        second.setVisibility(View.VISIBLE);
        four.setVisibility(GONE);
        five.setVisibility(GONE);

    }

    public void tabThreeClicked() {

        setBottomBarBlue();
        first.setVisibility(GONE);
        second.setVisibility(GONE);
        four.setVisibility(GONE);
        five.setVisibility(GONE);
    }

    public void tabFourClicked() {

        setBottomBarDefault();
        first.setVisibility(GONE);
        second.setVisibility(GONE);
        four.setVisibility(View.VISIBLE);
        five.setVisibility(GONE);
    }

    public void tabFiveClicked() {

        setBottomBarDefault();
        first.setVisibility(GONE);
        second.setVisibility(GONE);
        four.setVisibility(GONE);
        five.setVisibility(View.VISIBLE);
    }

    /**
     * @param title
     * @param description
     * @param isNotify
     * @param bundle      Interface Method For setting the top views
     */
    @Override
    public void setData(String title, String description, boolean isNotify, Bundle bundle) {
        title_text.setText(title);
        description_text.setText(description);
        menu_btn.setVisibility(View.VISIBLE);
        back_btn.setVisibility(GONE);
        user_view.setVisibility(GONE);
        detail_name.setVisibility(GONE);
        text_response.setVisibility(GONE);
        idea_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_blue));
        applyCustomFont(description_text, 2);
        setCompanyViewDisable();
        if (bundle.getBoolean(Constants.IS_BACK_KEY, false)) {
            menu_btn.setVisibility(GONE);
            back_btn.setVisibility(View.VISIBLE);
        } else {
            menu_btn.setVisibility(View.VISIBLE);
            back_btn.setVisibility(GONE);
        }
        if (bundle.getBoolean(Constants.IS_BULB_KEY, false)) {
            idea_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_white));
        } else {
            idea_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_blue));
        }
        if (bundle.getBoolean(Constants.IS_TAG_KEY, false)) {
            TAG = bundle.getString(Constants.TAG_KEY, Constants.TAG_KEY);
            if (TAG.equalsIgnoreCase(MessageListFragment.TAG)) {
                applyCustomFont(description_text, 1);
            }
        }
        if (bundle.getBoolean(Constants.IS_PROFILE_KEY, false)) {
            company_view.setVisibility(View.VISIBLE);
            description_text.setVisibility(GONE);
            description_company_text.setText(userLoggedIn.getEmail());
            phone_text.setText(userLoggedIn.getPhone());
            description_company_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profileFragment != null && profileFragment.isVisible()) {
                        profileFragment.updateEmail();
                    }
                }
            });
            phone_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (profileFragment != null && profileFragment.isVisible()) {
                        profileFragment.updatePhone();
                    }
                }
            });
        } else {
            description_text.setVisibility(View.VISIBLE);
            company_view.setVisibility(View.GONE);
        }
        if (bundle.getBoolean(Constants.IS_OTHER_KEY, false)) {
            user_view.setVisibility(View.VISIBLE);
            detail_name.setVisibility(View.VISIBLE);
            if (!bundle.isEmpty()) {
                DataModel e = bundle.getParcelable(Constants.OBJECT_DATA_MODEL_KEY);
                if (e.getSub_title() != null) {
                    if (e.getSub_title().equalsIgnoreCase("null")) {
                        detail_name.setText(e.getCategory_title());
                    } else {
                        detail_name.setText(e.getSub_title());
                    }
                } else {
                    detail_name.setText("Category Name");
                }
                title_text.setText(e.getShortDescription());
                description_text.setText(description);
                if (bundle.getBoolean(Constants.USER_VIEW_KEY)) {
                    user_name_text.setText(bundle.getString("user_name"));
                } else {
                    user_name_text.setText(userLoggedIn.getUsername());
                }
                if (e.getCreated_at() != null) {
                    if (DateConverter.getPrettyTime(e.getCreated_at()).contains("minutes")) {
                        date_text.setText(DateConverter.getPrettyTime(e.getCreated_at()).replace("minutes", "min"));
                    } else {
                        date_text.setText(DateConverter.getPrettyTime(e.getCreated_at()));
                    }
                } else {
                    date_text.setText("");
                }
            } else {
                detail_name.setText("Category Name");
                date_text.setText("");
            }
            idea_image.setImageDrawable(getResources().getDrawable(R.drawable.ic_bulb_blue));
            text_response.setVisibility(GONE);
            applyCustomFont(description_text, 2);
            if (userLoggedIn.getType() == Constants.TYPE_USER) {
                user_name_text.setText(userLoggedIn.getUsername());
            }
        } else {
            user_view.setVisibility(GONE);
            detail_name.setVisibility(GONE);
        }
    }

    private void setCompanyViewEnable() {
        description_text.setVisibility(GONE);
        company_view.setVisibility(View.VISIBLE);
        home_search_ic.setVisibility(View.VISIBLE);
    }

    private void setCompanyViewDisable() {
        description_text.setVisibility(View.VISIBLE);
        company_view.setVisibility(View.GONE);
        home_search_ic.setVisibility(View.GONE);
    }

    /**
     * @param bundle
     * @param type   Handle Fragments Transections
     */
    @Override
    public void callTransaction(Bundle bundle, int type) {

        if (type == LOAD_IDEA_DETAIL_FRAGMENT) {
            LoadIdeaDetailFragment(bundle);
            tabOneClicked();
        } else if (type == LOAD_COMPANY_SCREEN_FRAGMENT) {
            LoadCompanyScreenFragment(bundle);
            tabOneClicked();
        } else if (type == LOAD_SEARCH_CATE) {
            if (callForTrialOver()) {
                isMyCategoryIdeas = true;
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, true);
                    LoadHomeMainFragment(bundle);
                } else {
                    bundle.putBoolean(Constants.IS_ALL_IDEAS_KEY, false);
                    LoadHomeMainFragment(bundle);
                }
                tabOneClicked();
            } else {
                callSubscriptionDialogue();
            }
        } else if (type == Constants.MESSAGE_THREAD_USER) {
            if (callForTrialOver()) {
                tabOneClicked();
                LoadMessageListFragment(bundle);
            } else {
                callSubscriptionDialogue();
            }
        } else if (type == Constants.LOAD_HOME_NEW) {
            tabOneClicked();
            firstLoadHome();
        }


    }

    /**
     * @param isVisible
     * @param object    On Detail Idea Page Handle Top Menu
     */
    @Override
    public void setFavIconDisplay(boolean isVisible, Object object) {
        if (isVisible) {
            start_tick.setVisibility(View.VISIBLE);
        } else {
            start_tick.setVisibility(View.GONE);
        }
        if (object instanceof DataModel && object != null) {
            final DataModel detailDataModel = (DataModel) object;
            if (detailDataModel.isFavorite()) {
                start_tick.setImageDrawable(start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_fill));
            } else {
                start_tick.setImageDrawable(start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_unfill));
            }
            start_tick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (detailDataModel.isFavorite()) {
                        detailDataModel.setFavorite(false);
                        new VollyAPICall(v.getContext()
                                , false
                                , URL.remove_favourite + "/" + detailDataModel.getId()
                                , new JSONObject()
                                , userLoggedIn.getSessionToken()
                                , GET
                                , HomeActivity.this, APIActions.ApiActions.ideas_by_category_id);
                        start_tick.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_unfill));
                    } else {
                        detailDataModel.setFavorite(true);
                        new VollyAPICall(v.getContext()
                                , false
                                , URL.mark_favourite + "/" + detailDataModel.getId()
                                , new JSONObject()
                                , userLoggedIn.getSessionToken()
                                , GET
                                , HomeActivity.this, APIActions.ApiActions.ideas_by_category_id);
                        start_tick.setImageDrawable(getResources().getDrawable(R.drawable.ic_star_fill));
                    }
                }
            });
        }


    }

    @Override
    public void pressedBackButtonFromFragment() {
        backHandle(TAG);
        if (TAG.equalsIgnoreCase(MangeSubscriptionFragment.TAG)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/account/subscriptions"));
                    startActivity(browserIntent);
                }
            }, 250);
        }
    }

    /**
     * @param bundle CALL SUBSCRIPTION FRAGMENT FOR BUY OR CANCEL
     */
    @Override
    public void callTransactionForPurchase(Bundle bundle) {
        LoadMangeSubscriptionFragment(bundle);
    }

    BuySubscriptionDialogue buySubscriptionDialogue;

    /**
     * Call this in Fragment using interfaces
     * And in activity for display Subscription page
     */
    @Override
    public void callSubscriptionDialogue() {

        buySubscriptionDialogue = BuySubscriptionDialogue.newInstance(this);
        buySubscriptionDialogue.show(getSupportFragmentManager(), "pd");

    }

    /**
     * Back Pressed For Activity And Fragment
     */
    @Override
    public void onBackPressed() {
//        if (getFragmentManager().getBackStackEntryCount() > 0) {
//            getFragmentManager().popBackStack();
//        } else {
//            super.onBackPressed();
//        }
        if (back_btn.getVisibility() == View.VISIBLE) {
            backHandle(TAG);
        } else {
            if (isBackPressed) {
                isBackPressed = false;
                CustomToast.ShowCustomToast(this, "Press again to exit", Gravity.BOTTOM);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        isBackPressed = true;
                    }
                }, 1500);
            } else {
                isBackPressed = true;
                super.onBackPressed();
            }

        }
    }

    /**
     * @param tag Fragment Back Button Handle
     */
    private void backHandle(String tag) {
        DefineFragmentTransaction();
        if (tag.equalsIgnoreCase(MessageListFragment.TAG)) {
            if (messageListFragment != null) {
                if (messageListFragment.isVisible()) {
                    if (messageListFragment.isAdded()) {
                        transaction.setCustomAnimations(R.anim.detach_fragment, R.anim.detach_fragment);
                        transaction.remove(messageListFragment);
                        transaction.commitAllowingStateLoss();
                    }
                }
            }
        } else if (tag.equalsIgnoreCase(MangeSubscriptionFragment.TAG)) {
            if (mangeSubscriptionFragment != null) {
                if (mangeSubscriptionFragment.isVisible()) {
                    if (mangeSubscriptionFragment.isAdded()) {
                        transaction.setCustomAnimations(R.anim.detach_fragment, R.anim.detach_fragment);
                        transaction.remove(mangeSubscriptionFragment);
                        transaction.commitAllowingStateLoss();
                    }
                }
            }
        } else if (tag.equalsIgnoreCase(IdeaDetailFragment.TAG)) {
            if (ideaDetailFragment != null) {
                if (ideaDetailFragment.isVisible()) {
                    if (ideaDetailFragment.isAdded()) {
                        transaction.setCustomAnimations(R.anim.detach_fragment, R.anim.detach_fragment);
                        transaction.remove(ideaDetailFragment);
                        transaction.commitAllowingStateLoss();
                    }
                }
            }
        } else if (tag.equalsIgnoreCase(CompanyScreenFragment.TAG)) {
            if (companyScreenFragment != null) {
                if (companyScreenFragment.isVisible()) {
                    if (companyScreenFragment.isAdded()) {
                        transaction.setCustomAnimations(R.anim.detach_fragment, R.anim.detach_fragment);
                        transaction.remove(companyScreenFragment);
                        transaction.commitAllowingStateLoss();
                    }
                }
            }
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setTopAndBottomBar();
            }
        }, 100);
    }

    /**
     * Set Data in Top Bar
     */
    private void setTopAndBottomBar() {
        if (homeIdeaFragment != null) {
            if (homeIdeaFragment.isVisible()) {
                tabOneClicked();
                homeIdeaFragment.setTopView();
            }
        }
        if (profileFragment != null) {
            if (profileFragment.isVisible()) {
                tabFiveClicked();
                profileFragment.setTopView();
            }
        }
        if (myCategoriesFragment != null) {
            if (myCategoriesFragment.isVisible()) {
                tabTwoClicked();
                myCategoriesFragment.setTopView();
            }
        }
        if (myCategoriesFourTabFragment != null) {
            if (myCategoriesFourTabFragment.isVisible()) {
                tabFourClicked();
                myCategoriesFourTabFragment.setTopView();
            }
        }
        if (addNewIdeaFragment != null) {
            if (addNewIdeaFragment.isVisible()) {
                tabThreeClicked();
                addNewIdeaFragment.setTopView();
            }
        }
        if (mangeSubscriptionFragment != null) {
            if (mangeSubscriptionFragment.isVisible()) {
                tabThreeClicked();
                mangeSubscriptionFragment.setTopView();
            }
        }
        if (messageListFragment != null) {
            if (messageListFragment.isVisible()) {
                tabOneClicked();
                messageListFragment.setTopView();
            }
        }
        if (ideaDetailFragment != null) {
            if (ideaDetailFragment.isVisible()) {
                tabOneClicked();
                ideaDetailFragment.setTopView();
            }
        }
        if (companyScreenFragment != null) {
            if (companyScreenFragment.isVisible()) {
                tabOneClicked();
                companyScreenFragment.setTopView();
            }
        }
    }

    private void applyCustomFont(AppCompatTextView tv, int caseId) {

        Typeface customFont = FontCache.getTypeface("LuzSans-Light.ttf", tv.getContext());
        if (caseId == 1) {
            customFont = FontCache.getTypeface("LuzSans-Bold.ttf", tv.getContext());
            tv.setTypeface(customFont, Typeface.NORMAL);
            tv.setLinksClickable(true);
            tv.setAutoLinkMask(Linkify.WEB_URLS);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
//            tv.setTextColor(Color.parseColor("#0ea1d5"));
            tv.setTextColor(tv.getContext().getResources().getColor(R.color.blue_color));
        } else if (caseId == 2) {
            customFont = FontCache.getTypeface("LuzSans-Light.ttf", tv.getContext());
            tv.setTypeface(customFont, Typeface.NORMAL);
            tv.setLinksClickable(true);
            tv.setAutoLinkMask(Linkify.WEB_URLS);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
//            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setTextColor(tv.getContext().getResources().getColor(R.color.white));
        } else {
            tv.setTypeface(customFont, Typeface.NORMAL);
            tv.setLinksClickable(true);
            tv.setAutoLinkMask(Linkify.WEB_URLS);
            tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
//            tv.setTextColor(Color.parseColor("#FFFFFF"));
            tv.setTextColor(tv.getContext().getResources().getColor(R.color.white));

        }


    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        switch (apiActions) {
            case update_status:
                callDashBoardApi();
                break;
            case logout:
                OneSignal.deleteTag("user_id");
                SharedPrefrences.savePreference.clear();
                GoTo(this, LoginActivity.class);
                finish();
                break;
            case dashboard:
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        UserModel user = new Gson().fromJson(jsonObject.getJSONObject("successData").toString(), UserModel.class);
                        profile_name.setText(user.getUsername());
                        OneSignal.sendTag("user_id", user.getId() + "");
                        SharedPrefrences.savePreference.put(Constants.USER_SESSION_KEY, user);
                        userNotify = user;
                        userLoggedIn = user;
                        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                            idea_number.setText(user.getFavoriteIdeas() + " ideas liked");
                        } else {
                            idea_number.setText(user.getPostIdeas() + " ideas");
                        }
                        recyler_adapter.setNotifyNumber(3, user.getUnread());
                        notify_counter.setText("" + user.getUnread());
                        if (isLogin) {
                            isLogin = false;
                            home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell_blue));
                            notify_counter.setVisibility(View.VISIBLE);
                            notify_counter.setText("" + user.getUnread());
                            if (user.getUnread() < 1) {
                                notify_counter.setVisibility(View.GONE);
                                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                            }
                        } else {
                            home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                            notify_counter.setVisibility(View.GONE);
                        }
                    } else if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                        CustomToast.ShowCustomToast(HomeActivity.this, jsonObject.getString("errorMessage"), Gravity.CENTER);
                    }
                    if (!callForTrialOver()) {
                        callSubscriptionDialogue();
                    }

                    // HANDLING THE USER STATE HERE WHEN USER CANCEL SUBS FROM GOOGLE PLAY STORE
                    if (userLoggedIn.getGetSubscription() != null && userLoggedIn.getUserStatus() == Constants.PAID_USER) {

                        try {
                            Bundle activeSubs = mService.getPurchases(3, userLoggedIn.getGetSubscription().getPackageName(),
                                    IabHelper.ITEM_TYPE_SUBS, userLoggedIn.getGetSubscription().getToken());
                            if (activeSubs.getInt(RESPONSE_CODE) == BILLING_RESPONSE_RESULT_OK) {
                                ArrayList<String> purchaseData
                                        = activeSubs.getStringArrayList(RESPONSE_INAPP_PURCHASE_DATA_LIST);
                                JSONObject object = null;
                                for (int i = 0; i < purchaseData.size(); i++) {
                                    JSONObject check = new JSONObject(purchaseData.get(i));
                                    if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                                        if (check.getString("productId").equalsIgnoreCase(subscriptionIdCompany)) {
                                            object = check;
                                        }
                                    } else if (userLoggedIn.getType() == Constants.TYPE_USER) {
                                        if (check.getString("productId").equalsIgnoreCase(subscriptionIdUser)) {
                                            object = check;
                                        } else if (check.getString("productId").equalsIgnoreCase(subscriptionIdUser_2)) {
                                            object = check;
                                        }
                                    }
                                }
                                if (object != null) {
                                    if (!object.getBoolean("autoRenewing")) {
                                        if (TimeUtils.isOlder(userLoggedIn.getGetSubscription().getExpireAt())) {
                                            callStatusChanged(Constants.PAID_USER_CANCEL);
                                        }
                                    }
                                }
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public boolean callForTrialOver() {
        if (DateConverter.checkFor5Day(userLoggedIn.getCreatedAt()) && (userLoggedIn.getUserStatus() < Constants.PAID_USER)) {
//            callSubscriptionDialogue();
            return false;
        } else if (userLoggedIn.getUserStatus() == Constants.PAID_USER_CANCEL) {
            return false;
        }
        return true;
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        Log.e(TAG, "onRequestError: " + response);
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Send Purchase Result to fragment tu handle
        if (mangeSubscriptionFragment != null && mangeSubscriptionFragment.isVisible()) {
            mangeSubscriptionFragment.onActivityResult(requestCode, resultCode, data);
        }
        // ON NOTIFICATION SCREEN WHEN PRESSED ANY NOTIFICATION HANDLE HERE
        if (requestCode == NOTIFICATION_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                callDashBoardApi();
                Bundle bundle = data.getExtras().getBundle("result");
                pushNotificationHandle(bundle);
            }
        }
        if (requestCode == REQUEST_CODE_DETAIL_SUBSCRIBE) {
            Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        }
    }

    /**
     * @param bundle Pushed Notification Clicked Handle
     */
    private void pushNotificationHandle(Bundle bundle) {
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            if (bundle.getString(Constants.TYPE_KEY).equalsIgnoreCase("msg")) {
                if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                    IntentFunction.GoToChatView(HomeActivity.this, bundle.getInt(Constants.POST_ID_KEY, 0));
                } else {
                    callSubscriptionDialogue();
                }

            } else if (bundle.getString(Constants.TYPE_KEY).equalsIgnoreCase("idea")) {
                DataModel e = new DataModel();
                e.setId(bundle.getInt(Constants.POST_ID_KEY, 0));
                bundle.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, e);
                LoadIdeaDetailFragment(bundle);
                tabOneClicked();
            }
        } else {
            if (bundle.getString(Constants.TYPE_KEY).equalsIgnoreCase("idea")) {
                DataModel e = new DataModel();
                e.setId(bundle.getInt(Constants.POST_ID_KEY, 0));
                bundle.putParcelable(Constants.OBJECT_DATA_MODEL_KEY, e);
                LoadIdeaDetailFragment(bundle);
                tabOneClicked();
            } else if (bundle.getString(Constants.TYPE_KEY).equalsIgnoreCase("msg")) {
                if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
                    IntentFunction.GoToChatView(HomeActivity.this, bundle.getInt(Constants.POST_ID_KEY, 0));
                } else {
                    callSubscriptionDialogue();
                }

            }
        }
    }

    @Override
    public void callStatusChanged(int status) {
        callUpdateStatusApi(status);
    }

    /**
     * @param status for setting user state  PAID=2, UNPAID=0, TRIAL_OVER=1, SUB_CANCEL=1
     */
    private void callUpdateStatusApi(int status) {
        userLoggedIn.setUserStatus(status);
        try {
            new VollyAPICall(HomeActivity.this
                    , false
                    , URL.update_status
                    , new JSONObject().put("status", status)
                    , userLoggedIn.getSessionToken()
                    , Request.Method.POST
                    , HomeActivity.this
                    , APIActions.ApiActions.update_status);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param event On Foreground app handling the notification stats.
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (home_notification != null && notify_counter != null) {
            callDashBoardApi();
            isLogin = true;
//            if (event.isNotify) {
//                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell_blue));
//                notify_counter.setVisibility(View.VISIBLE);
//                userNotify.setUnread(userNotify.getUnread() + 1);
//                notify_counter.setText("" + userNotify.getUnread());
//                if (userNotify.getUnread() < 1) {
//                    notify_counter.setVisibility(View.GONE);
//                    home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
//                }
//            } else {
//                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
//                notify_counter.setVisibility(View.GONE);
//            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onBuySubscriptionDialogueClick(BuySubscriptionDialogue dialog) {
        dialog.dismiss();
        LoadMangeSubscriptionFragment(new Bundle());
    }

    ///////////////////////////WORK FOR SUBSCRIPTION///////////////////////////
    private IInAppBillingService mService;

    private ServiceConnection mServiceConn = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
            mService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {
            mService = IInAppBillingService.Stub.asInterface(service);
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        //WORK FOR SUBSCRIPTION
        if (mService != null) {
            unbindService(mServiceConn);
        }
    }

    @Override
    public void onDoneClick(WelcomeDialogue dialog) {
        dialog.dismiss();
    }


    /*
    Code for Checking in-app subscription
    * Bundle bundle = mService.getBuyIntent(3, "com.example.myapp",
   MY_SKU, "subs", developerPayload);

PendingIntent pendingIntent = bundle.getParcelable(RESPONSE_BUY_INTENT);
if (bundle.getInt(RESPONSE_CODE) == BILLING_RESPONSE_RESULT_OK) {
   // Start purchase flow (this brings up the Google Play UI).
   // Result will be delivered through onActivityResult().
   startIntentSenderForResult(pendingIntent, RC_BUY, new Intent(),
       Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0));
}
To query for active subscriptions, use the getPurchases method, again with the product type parameter set to "subs":

Bundle activeSubs = mService.getPurchases(3, "com.example.myapp",
                   "subs", continueToken);
    *
    * */
//    Bundle activeSubs = mService.getPurchases(3, "com.example.myapp",
//            IabHelper.ITEM_TYPE_SUBS, continueToken);

}