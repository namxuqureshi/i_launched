package com.dev.codingpixel.i_launched.activity.dialogues;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.DataModel;

import static android.app.Activity.RESULT_OK;
import static com.dev.codingpixel.i_launched.customui.ImageHelper.getRoundedCornerBitmap;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.utils.ViewUtils.checkRotation;

public class NDACompanyDialouge extends BaseDialogFragment<NDACompanyDialouge.OnDialogFragmentClickListener> {
    ImageView Attached_Image;
    TextView first_text, second_text;
    String ImagePAth = "", innovatorName = "";
    AlertDialog dialog;
    Button d_sign_nda;
    DataModel temp;
    DatePickerDialog.OnDateSetListener mDateSetListener;

    static OnDialogFragmentClickListener Listener;

    public interface OnDialogFragmentClickListener {
        public void onNDACompanyDialougeClick(NDACompanyDialouge dialog);

        public void onNDACompanyDialougeClick(NDACompanyDialouge dialog, DataModel e);
    }

    public static NDACompanyDialouge newInstance(OnDialogFragmentClickListener listner, String innovatorName)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        NDACompanyDialouge frag = new NDACompanyDialouge();
        Bundle args = new Bundle();
        args.putString("name", innovatorName);
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    public static NDACompanyDialouge newInstance(OnDialogFragmentClickListener listner, String innovatorName, DataModel e)//, ArrayList<BudzMapHomeDataModel> budzMapDataModels
    {
        NDACompanyDialouge frag = new NDACompanyDialouge();
        Bundle args = new Bundle();
        args.putString("name", innovatorName);
        args.putParcelable("DataModel", e);
        frag.setArguments(args);
        Listener = listner;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        final View main_dialog = factory.inflate(R.layout.nda_company_layout, null);
        dialog = new AlertDialog.Builder(getContext(), R.style.PauseDialog).create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER;
//        BUnd
        //        int top_y = getResources().getDisplayMetrics().heightPixels / 5;
//        wmlp.y = top_y;   //y position
        ImageView Cross_btn = main_dialog.findViewById(R.id.cross_btn);
        Cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        Bundle bundle = getArguments();
        this.innovatorName = bundle.getString("name");
        this.temp = bundle.getParcelable("DataModel");
        InitContetn(main_dialog, dialog);
        dialog.setView(main_dialog);
        return dialog;
    }

    public void InitContetn(View view, final AlertDialog dialog) {

        d_sign_nda = view.findViewById(R.id.d_sign_nda);
        second_text = view.findViewById(R.id.second_text);
        first_text = view.findViewById(R.id.first_text);
        //I, [Firm Name] agree that, in consideration for access to information shared with me by [Innovator] I will:
        first_text.setText(Html.fromHtml("I, <strong><b><font color=#1fbced>" + userLoggedIn.getUsername() + "</font></b></strong> agree that, in consideration for access to information shared with me<br> by <strong><b><font color=#1fbced>" + innovatorName + "</font></b></strong> I will:"));
        //1)	Keep all information provided to me and/or team representing [firm name] relating to idea in strict confidence.
        second_text.setText(Html.fromHtml("Keep all information provided to me and/or team representing <strong><b><font color=#1fbced>" + userLoggedIn.getUsername() + "</font></b></strong> relating to idea in strict confidence."));
        d_sign_nda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Listener.onNDACompanyDialougeClick(NDACompanyDialouge.this, temp);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 1200) {
            ImagePAth = data.getExtras().getString("file_path_arg");
            Log.d("paths", data.getExtras().getString("file_path_arg"));
            Bitmap bitmapOrg = BitmapFactory.decodeFile(data.getExtras().getString("file_path_arg"));
            bitmapOrg = checkRotation(bitmapOrg, data.getExtras().getString("file_path_arg"));
            bitmapOrg = Bitmap.createScaledBitmap(bitmapOrg, 300, 300, false);
            int corner_radious = (bitmapOrg.getWidth() * 10) / 100;
            Bitmap bitmap = getRoundedCornerBitmap(bitmapOrg, corner_radious);
            Drawable drawable = new BitmapDrawable(getResources(), bitmap);
            Attached_Image.setBackground(drawable);
            Attached_Image.setImageDrawable(null);
        }
    }


}