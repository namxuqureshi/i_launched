package com.dev.codingpixel.i_launched.models;

/**
 * Created by incubasyss on 30/01/2018.
 */

public class MsgChatModel {
    String message, date;
    String sender_image, receiver_image;
    boolean isSender, last;
    Integer sender_id, receiver_id, id, thread_id;
    String created_at, updated_at;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getThread_id() {
        return thread_id;
    }

    public void setThreadId(Integer thread_id) {
        this.thread_id = thread_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public MsgChatModel(String receiver_msg, String receiver_date, String receiver_image, boolean isSender) {
        this.message = receiver_msg;
        this.date = receiver_date;
        this.receiver_image = receiver_image;
        this.isSender = isSender;
        this.last = false;
    }

    public MsgChatModel(String receiver_msg, String receiver_date, String receiver_image, boolean isSender, boolean isLast) {
        this.message = receiver_msg;
        this.date = receiver_date;
        this.receiver_image = receiver_image;
        this.isSender = isSender;
        this.last = isLast;
    }

    public MsgChatModel(boolean isSender, String sender_msg, String sender_date, String sender_image) {
        this.message = sender_msg;
        this.date = sender_date;
        this.sender_image = sender_image;
        this.isSender = isSender;
        this.last = false;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public MsgChatModel() {
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public boolean isSender() {
        return isSender;
    }

    public void setSender(boolean sender) {
        isSender = sender;
    }

    public String getSender_msg() {
        return message;
    }

    public void setSender_msg(String sender_msg) {
        this.message = sender_msg;
    }

    public String getReceiver_msg() {
        return message;
    }

    public void setReceiver_msg(String receiver_msg) {
        this.message = receiver_msg;
    }

    public String getSender_date() {
        return date;
    }

    public void setSender_date(String sender_date) {
        this.date = sender_date;
    }

    public String getReceiver_date() {
        return date;
    }

    public void setReceiver_date(String receiver_date) {
        this.date = receiver_date;
    }

    public String getSender_image() {
        return sender_image;
    }

    public void setSender_image(String sender_image) {
        this.sender_image = sender_image;
    }

    public String getReceiver_image() {
        return receiver_image;
    }

    public void setReceiver_image(String receiver_image) {
        this.receiver_image = receiver_image;
    }

    public Integer getSender_id() {
        return sender_id;
    }

    public void setSenderId(Integer sender_id) {
        this.sender_id = sender_id;
    }

    public Integer getReceiver_id() {
        return receiver_id;
    }

    public void setReceiverId(Integer receiver_id) {
        this.receiver_id = receiver_id;
    }
}
