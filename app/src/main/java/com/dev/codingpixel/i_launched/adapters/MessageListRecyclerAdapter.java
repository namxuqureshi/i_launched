package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.dev.codingpixel.i_launched.models.MessageListModel;
import com.dev.codingpixel.i_launched.static_function.Constants;

import java.util.ArrayList;
import java.util.List;

import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class MessageListRecyclerAdapter extends RecyclerView.Adapter<MessageListRecyclerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    List<MessageListModel> mData = new ArrayList<>();
    private MessageListRecyclerAdapter.ItemClickListener mClickListener;

    public MessageListRecyclerAdapter(Context context, List<MessageListModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.message_list_fragment_item, parent, false);

        MessageListRecyclerAdapter.ViewHolder viewHolder = new MessageListRecyclerAdapter.ViewHolder(view);
        return viewHolder;
//        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            holder.idea_name.setVisibility(View.VISIBLE);
            holder.idea_name.setText(mData.get(position).getGetIdeas().get(0).getTitle());

            if (false) {

            } else {
                holder.unread.setVisibility(View.GONE);
            }
            holder.company_name_text.setText(mData.get(position).getGetReceiverUser().getUsername());
            if (mData.get(position).getLastMessage() != null) {
                holder.message.setText(mData.get(position).getLastMessage().getMessage());
            } else {
                holder.message.setText("");
            }
            if (DateConverter.getPrettyTime(mData.get(position).getCreatedAt()).contains("minutes")) {

                holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreatedAt()).replace("minutes", "min"));
            } else {
                holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreatedAt()));
            }
        } else {
            holder.idea_name.setVisibility(View.GONE);
            if (false) {

            } else {
                holder.unread.setVisibility(View.GONE);
            }
            holder.company_name_text.setText(mData.get(position).getGetUser().getUsername());
            if (mData.get(position).getLastMessage() != null) {
                holder.message.setText(mData.get(position).getLastMessage().getMessage());
            } else {
                holder.message.setText("");
            }
            if (DateConverter.getPrettyTime(mData.get(position).getCreatedAt()).contains("minutes")) {

                holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreatedAt()).replace("minutes", "min"));
            } else {
                holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreatedAt()));
            }
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        AppCompatTextView unread, date, company_name_text, message, idea_name;

        ViewHolder(View itemView) {
            super(itemView);
            unread = itemView.findViewById(R.id.unread);
            idea_name = itemView.findViewById(R.id.idea_name);
            date = itemView.findViewById(R.id.date);
            company_name_text = itemView.findViewById(R.id.company_name_text);
            message = itemView.findViewById(R.id.message);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
