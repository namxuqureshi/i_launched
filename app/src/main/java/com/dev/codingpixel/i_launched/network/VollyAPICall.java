package com.dev.codingpixel.i_launched.network;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.dev.codingpixel.i_launched.activity.registration.LoginActivity;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.ProgressDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import static com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication.getContext;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;


public class VollyAPICall {
    public VollyAPICall(final Context context, final boolean isLoading, String url, final JSONObject jsonBody, final String token, int method, final APIResponseListner apiResponseListner, final APIActions.ApiActions apiActions) {
        if (context != null) {
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            final ProgressDialog pd = ProgressDialog.newInstance();
            if (isLoading) {
                pd.show(((FragmentActivity) context).getSupportFragmentManager(), "pd");
            }
            final String requestBody = jsonBody.toString();
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.i("VOLLEY", response + "");

                    apiResponseListner.onRequestSuccess(response.toString(), apiActions);

                    if (isLoading) {
                        try {
                            pd.dismiss();
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        String sesClear = "";
                        String resp = "";
                        if (error != null && error.networkResponse != null) {
                            if (error.networkResponse.data != null) {
                                String responseBody = new String(error.networkResponse.data, "utf-8");
                                JSONObject jsonObject = new JSONObject(responseBody);
                                if (jsonObject.getString("errorMessage").length() > 0) {
                                    sesClear = jsonObject.getString("errorMessage");

                                }
                                resp = jsonObject.toString();
                            } else {
                                resp = "{internal server error}";
                            }
                        } else {
                            assert error != null;
                            if (error.getMessage() != null) {
                                resp = error.getMessage();
                            } else {
                                resp = "{internal server error}";
                            }
                        }
                        if (sesClear.equalsIgnoreCase("Session Expired")) {
                            if (isLoading) {
                                try {
                                    if (pd != null)
                                        pd.dismiss();
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                }
                            }
//                            ClearSharedPrefrences(context);
//                            final ProgressDialog pd = ProgressDialog.newInstance();
//                            pd.show(((FragmentActivity) context).getSupportFragmentManager(), "pd");
//                            new Handler().postDelayed(new Runnable() {
//                                public void run() {
//                                    pd.dismiss();
//                                    OneSignal.deleteTag("user_id");
//                                    setString("user_email", "", context);
//                                    setString("user_password", "", context);
//                                    setBool("is_user_login", false, context);
//                                    delete_UserValues(context);
                            userLoggedIn = null;
                            OneSignal.deleteTag("user_id");
                            SharedPrefrences.savePreference.clear();
                            Intent i = new Intent(context, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            context.startActivity(i);
//                            ((Activity) context).finish();
                            CustomToast.ShowCustomToast(getContext(), "Session Expired!! Login Again", Gravity.TOP);
//                                }
//                            }, 1000);

                        } else {
                            apiResponseListner.onRequestError(resp, apiActions);
                            if (isLoading) {
                                try {
                                    pd.dismiss();
                                } catch (IllegalStateException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (JSONException | UnsupportedEncodingException e) {
                        if (isLoading) {
                            try {
                                pd.dismiss();
                            } catch (IllegalStateException e1) {
                                e1.printStackTrace();
                            }
                        }
                        apiResponseListner.onRequestError(error.networkResponse.statusCode + "", apiActions);
                        //Handle a malformed json response
                    }
                }
            }) {
                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public byte[] getBody() {
                    try {
                        return requestBody == null ? null : requestBody.getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", requestBody, "utf-8");
                        return null;
                    }
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> param = new HashMap<>();
//                    param.put("app_key", "4m9Nv1nbyLoaZAMyAhQri9BUXBxlD3yQxbAiHsn2hwQ=");
                    param.put("app_key", "yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=");
                    if (token != null) {
                        param.put("session_token", token);
                    }
                    return param;
                }

                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
                        // Check if it is JSONObject or JSONArray
                        Object json = new JSONTokener(jsonString).nextValue();
                        JSONObject jsonObject = new JSONObject();
                        if (json instanceof JSONObject) {
                            jsonObject = (JSONObject) json;
                        } else if (json instanceof JSONArray) {
                            jsonObject.put("success", json);
                        } else {
                            String message = "{\"error\":\"Unknown Error\",\"code\":\"failed\"}";
                            jsonObject = new JSONObject(message);
                        }
                        return Response.success(jsonObject,
                                HttpHeaderParser.parseCacheHeaders(response));
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return super.getParams();
                }
            };

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(jsonObjectRequest);
        }
    }
}

