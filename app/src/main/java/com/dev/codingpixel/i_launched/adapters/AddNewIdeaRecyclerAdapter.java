package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.AutoCompleteCategoryModel;

import java.util.ArrayList;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class AddNewIdeaRecyclerAdapter extends RecyclerView.Adapter<AddNewIdeaRecyclerAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    ArrayList<AutoCompleteCategoryModel> mData = new ArrayList<>();
    private AddNewIdeaRecyclerAdapter.ItemClickListener mClickListener;

    public AddNewIdeaRecyclerAdapter(Context context, ArrayList<AutoCompleteCategoryModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.categories_fragment_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.options.setText(mData.get(holder.getAdapterPosition()).getName());
        if (mData.get(position).isClicked()) {
            holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg_blue));
        } else {
            holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg));
        }

        holder.options.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.get(holder.getAdapterPosition()).isClicked()) {
                    mData.get(holder.getAdapterPosition()).setClicked(false);
                    holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg));
                } else {
                    mData.get(holder.getAdapterPosition()).setClicked(true);
                    holder.options.setBackgroundDrawable(holder.options.getContext().getResources().getDrawable(R.drawable.btn_categories_bg_blue));
                }
                unCheckAllOther(holder.getAdapterPosition(), false);
                notifyDataSetChanged();
                if (mClickListener != null) {
                    mClickListener.onItemClick(v, position);
                }
            }
        });

    }


    public void unCheckAllOther(int position, boolean check) {
        for (int i = 0; i < mData.size(); i++) {
            if (i != position) {
                mData.get(i).setClicked(false);
            }
        }
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Button options;

        public ViewHolder(View itemView) {
            super(itemView);

            options = itemView.findViewById(R.id.options);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
