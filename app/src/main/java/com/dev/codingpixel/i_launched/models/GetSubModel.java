package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by incubasyss on 01/03/2018.
 */

public class GetSubModel implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("subscription_id")
    @Expose
    private String subscriptionId;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("activated_at")
    @Expose
    private String activatedAt;
    @SerializedName("expire_at")
    @Expose
    private String expireAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GetSubModel withId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public GetSubModel withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public GetSubModel withSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
        return this;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public GetSubModel withToken(String token) {
        this.token = token;
        return this;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public GetSubModel withPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getActivatedAt() {
        return activatedAt;
    }

    public void setActivatedAt(String activatedAt) {
        this.activatedAt = activatedAt;
    }

    public GetSubModel withActivatedAt(String activatedAt) {
        this.activatedAt = activatedAt;
        return this;
    }

    public String getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(String expireAt) {
        this.expireAt = expireAt;
    }

    public GetSubModel withExpireAt(String expireAt) {
        this.expireAt = expireAt;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public GetSubModel withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public GetSubModel withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.userId);
        dest.writeString(this.subscriptionId);
        dest.writeString(this.token);
        dest.writeString(this.packageName);
        dest.writeString(this.activatedAt);
        dest.writeString(this.expireAt);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public GetSubModel() {
    }

    protected GetSubModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subscriptionId = in.readString();
        this.token = in.readString();
        this.packageName = in.readString();
        this.activatedAt = in.readString();
        this.expireAt = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Parcelable.Creator<GetSubModel> CREATOR = new Parcelable.Creator<GetSubModel>() {
        @Override
        public GetSubModel createFromParcel(Parcel source) {
            return new GetSubModel(source);
        }

        @Override
        public GetSubModel[] newArray(int size) {
            return new GetSubModel[size];
        }
    };
}
