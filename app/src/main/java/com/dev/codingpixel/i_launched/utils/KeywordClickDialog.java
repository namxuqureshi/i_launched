package com.dev.codingpixel.i_launched.utils;

//import com.android.volley.Request;
//import com.codingpixel.healingbudz.Activity.Home.HomeActivity;
//import com.codingpixel.healingbudz.R;
//import com.codingpixel.healingbudz.adapter.KeywordDialogRecylerAdapter;
//import com.codingpixel.healingbudz.data_structure.APIActions;
//import com.codingpixel.healingbudz.network.VollyAPICall;
//import com.codingpixel.healingbudz.network.model.APIResponseListner;
//import com.codingpixel.healingbudz.network.model.URL;

//import static com.codingpixel.healingbudz.Activity.Home.HomeActivity.getKeywordDialogItemClickListner;
//import static com.codingpixel.healingbudz.Activity.splash.Splash.user;

public class KeywordClickDialog {
//    AlertDialog dialog;
//    String keyword;
//    Context context;
//    ArrayList<String> data = new ArrayList<>();
//
//    @Override
//    public void onItemSideMenuClick(View view, int position) {
//        dialog.dismiss();
//        if (!((Activity) view.getContext() instanceof HomeActivity)) {
//            ((Activity) view.getContext()).finish();
//        }
//        switch (position) {
//            case 0:
//                getKeywordDialogItemClickListner.ShowQuestionsForKeyword(this.keyword);
//                break;
//            case 1:
//                getKeywordDialogItemClickListner.ShowAnswersForKeyword(this.keyword);
//                break;
//            case 2:
//                getKeywordDialogItemClickListner.ShowGroupsForKeyword(this.keyword);
//                break;
//            case 3:
//                getKeywordDialogItemClickListner.ShowJournalsForKeyword(this.keyword);
//                break;
//            case 4:
//                getKeywordDialogItemClickListner.ShowStrainForKeyword(this.keyword);
//                break;
//            case 5:
//                getKeywordDialogItemClickListner.ShowBudzMapForKeyword(this.keyword);
//                break;
//        }
//    }
//
//    public KeywordClickDialog(String Keyword, Context c) {
//        super();
//        this.keyword = Keyword;
//        this.context = c;
//        data.add("Questions");
//        data.add("Answers");
//        data.add("Groups");
//        data.add("Journals");
//        data.add("Strain");
//        data.add("Budz Map");
//        ShowDialog();
//    }
//
//    public void ShowDialog() {
//        LayoutInflater factory = LayoutInflater.from(this.context);
//        final View main_dialog = factory.inflate(R.layout.keyword_ckicked_dialog_layout, null);
//        dialog = new AlertDialog.Builder(context).create();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(false);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
//        wmlp.gravity = Gravity.CENTER;
//        final TextView Keyword_title = main_dialog.findViewById(R.id.keyword_title);
//        ImageView Cross = main_dialog.findViewById(R.id.cross_btn);
//        Button follow_keyword = main_dialog.findViewById(R.id.follow_keyword);
//        follow_keyword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    new VollyAPICall(view.getContext(), false, URL.follow_keyword, new JSONObject().put("keyword", Keyword_title.getText().toString()), user.getSession_key(), Request.Method.POST, KeywordClickDialog.this, APIActions.ApiActions.get_key);
//                    dialog.dismiss();
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    dialog.dismiss();
//                }
//            }
//        });
//        Cross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                dialog.dismiss();
//            }
//        });
//
//
//        Keyword_title.setText(keyword);
//        RecyclerView recyclerView = main_dialog.findViewById(R.id.recyler_view);
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
//        recyclerView.setLayoutManager(layoutManager);
//
//        KeywordDialogRecylerAdapter recyler_adapter = new KeywordDialogRecylerAdapter(context, data);
//        recyler_adapter.setClickListener(this);
//        recyclerView.setAdapter(recyler_adapter);
//        dialog.setView(main_dialog);
//        dialog.show();
//    }
//
//    @Override
//    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
//        Log.d("onRequestSuccess: ", response);
//    }
//
//    @Override
//    public void onRequestError(String response, APIActions.ApiActions apiActions) {
//        Log.d("onRequestError: ", response);
//    }
}