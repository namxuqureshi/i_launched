package com.dev.codingpixel.i_launched.activity.dialogues;

import android.annotation.SuppressLint;
import android.app.Dialog;

import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.static_function.Constants;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;

import org.json.JSONException;
import org.json.JSONObject;

import static com.dev.codingpixel.i_launched.network.model.URL.update_profile;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.UIModification.HideKeyboard;


public class EditUserProfileUpdatePersonalInfoAlertDialog extends BaseDialogFragment<EditUserProfileUpdatePersonalInfoAlertDialog.OnDialogFragmentClickListener> implements APIResponseListner {
    Button Save_Btn;
    ImageView Cross_btn;
    EditText Edit_Bio, Email, Confirm_Email, Password, Confirm_Password, Zipcode, USERNAME;
    static String Dialog_type;
    static JSONObject object;
    static OnDialogFragmentClickListener Listener;
    View main_dialog;

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        Log.d("response", response);
        try {
            JSONObject object = new JSONObject(response);
//            CustomeToast.ShowCustomToast(getContext(), object.getString("errorMessage"), Gravity.BOTTOM);
            if (object.getString("status").equalsIgnoreCase("success")) {
                if (apiActions == APIActions.ApiActions.update_user_email) {
                    SaveButtonAction();
                }
            }else {
                try {
                    JSONObject jsonObject = null;
                    jsonObject = new JSONObject(response);
//                    if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                        new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!!")
                                .setContentText("Email already exist!!")
                                .setConfirmText("Ok")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
//                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        Log.d("response", response);
        try {
            JSONObject object = new JSONObject(response);
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                    new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!!")
                            .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (apiActions == APIActions.ApiActions.update_user_email) {
                SaveButtonAction();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface OnDialogFragmentClickListener {
        void onSaveButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog dialog, JSONObject data);

        void onCrossButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog dialog);
    }

    public static EditUserProfileUpdatePersonalInfoAlertDialog newInstance(OnDialogFragmentClickListener listner, String dialog_type, JSONObject data_object) {
        EditUserProfileUpdatePersonalInfoAlertDialog frag = new EditUserProfileUpdatePersonalInfoAlertDialog();
        Bundle args = new Bundle();
        frag.setArguments(args);
        Listener = listner;
        Dialog_type = dialog_type;
        object = data_object;
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater factory = LayoutInflater.from(getContext());
        main_dialog = factory.inflate(R.layout.edit_user_profile_personalinfo_dialog_layout, null);
        final AlertDialog dialog = new AlertDialog.Builder(getContext(), R.style.PauseDialog).create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        Save_Btn = main_dialog.findViewById(R.id.save_btn);
        Save_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HideKeyboard(getActivity());
                JSONObject api_object = new JSONObject();
                try {
                    String api_url = "";
                    if (Dialog_type.equalsIgnoreCase("BIO")) {
//                        if (Edit_Bio.getText().toString().length() == 0) {
//                            CustomeToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Field Cannot be Empty!!", Gravity.CENTER);
//                            return;
//                        }
                        api_object.put("about_us", Edit_Bio.getText().toString());
                        api_object.put("email", userLoggedIn.getEmail());
                        api_object.put("phone", userLoggedIn.getPhone());
                        api_object.put("username", userLoggedIn.getUsername());
                        api_url = update_profile;
                    } else if (Dialog_type.equalsIgnoreCase("Email")) {
                        api_object.put("email", Email.getText().toString());
                        api_object.put("phone", userLoggedIn.getPhone());
                        api_object.put("username", userLoggedIn.getUsername());
                        api_url = update_profile;
                        if (!Email.getText().toString().equalsIgnoreCase(Confirm_Email.getText().toString())) {
                            CustomToast.ShowCustomToast(getContext(), "Email not match!!", Gravity.BOTTOM);
                            return;
                        }
                        if (Email.getText().toString().equalsIgnoreCase(object.getString("email"))) {
//                            SaveButtonAction();
                            CustomToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Please enter new email!!", Gravity.CENTER);
                            return;
                        }
                        if (Email.getText().toString().length() == 0) {
                            CustomToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Field Cannot be Empty!!", Gravity.CENTER);
                            return;
                        }
                        userLoggedIn.setEmail(Email.getText().toString().trim());
                    } else if (Dialog_type.equalsIgnoreCase("Password")) {
                        api_object.put("password", Password.getText().toString());
                        api_url = update_profile;
                        if (!Password.getText().toString().equalsIgnoreCase(Confirm_Password.getText().toString())) {
                            CustomToast.ShowCustomToast(getContext(), "Password not match!!", Gravity.BOTTOM);
                            return;
                        }
                        if (Password.getText().toString().equalsIgnoreCase(object.getString("password"))) {
                            SaveButtonAction();
//                            CustomeToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Please enter new email!!", Gravity.CENTER);
                            return;
                        }
                        if (Password.getText().toString().length() == 0) {
                            CustomToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Field Cannot be Empty!!", Gravity.CENTER);
                            return;
                        }
                    } else if (Dialog_type.equalsIgnoreCase("Zipcode")) {
                        api_object.put("phone", Zipcode.getText().toString());
                        api_object.put("email", userLoggedIn.getEmail());
                        api_object.put("username", userLoggedIn.getUsername());
                        api_url = update_profile;
                        if (Zipcode.getText().toString().equalsIgnoreCase(object.getString("zipcode"))) {
                            SaveButtonAction();
                            return;
                        }
                        if (Zipcode.getText().toString().length() == 0) {
                            CustomToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Field Cannot be Empty!!", Gravity.CENTER);
                            return;
                        }
                        userLoggedIn.setPhone(Zipcode.getText().toString().trim());

                    } else if (Dialog_type.equalsIgnoreCase("NAME")) {
                        api_object.put("username", USERNAME.getText().toString());
                        api_object.put("phone", userLoggedIn.getPhone());
                        api_object.put("email", userLoggedIn.getEmail());

                        api_url = update_profile;
                        if (USERNAME.getText().toString().equalsIgnoreCase(object.getString("name"))) {
                            SaveButtonAction();
                            return;
                        }
                        if (USERNAME.getText().toString().length() == 0) {
                            CustomToast.ShowCustomToast(I_LaunchedApplication.getContext(), "Field Cannot be Empty!!", Gravity.CENTER);
                            return;
                        }
                    }
                    if (Dialog_type.equalsIgnoreCase("Email")) {
//                        SaveButtonAction();
                        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        api_object.put("device_id", android_id);
                        new VollyAPICall(getContext(), true, api_url, api_object, userLoggedIn.getSessionToken(), Request.Method.POST, EditUserProfileUpdatePersonalInfoAlertDialog.this, APIActions.ApiActions.update_user_email);
                    } else {
                        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getActivity().getContentResolver(),
                                Settings.Secure.ANDROID_ID);
                        api_object.put("device_id", android_id);
                        SaveButtonAction();
                        new VollyAPICall(getContext(), true, api_url, api_object, userLoggedIn.getSessionToken(), Request.Method.POST, EditUserProfileUpdatePersonalInfoAlertDialog.this, APIActions.ApiActions.update_profile_info);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Cross_btn = main_dialog.findViewById(R.id.cross_btn);
        Cross_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Animation startAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.zoomout);
//                main_dialog.startAnimation(startAnimation);
                Listener.onCrossButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog.this);
            }
        });

        Edit_Bio = main_dialog.findViewById(R.id.edit_bio);
        Email = main_dialog.findViewById(R.id.email);
        Confirm_Email = main_dialog.findViewById(R.id.confirm_email);
        Password = main_dialog.findViewById(R.id.password);
        Confirm_Password = main_dialog.findViewById(R.id.confirm_password);
        USERNAME = main_dialog.findViewById(R.id.name);
        Zipcode = main_dialog.findViewById(R.id.zipcode);
        Edit_Bio.setSelection(Edit_Bio.length());
        Zipcode.setSelection(Zipcode.length());
        Password.setSelection(Password.length());
        Password.setText("");
        Email.setText("");
        Confirm_Email.setText("");
        Confirm_Password.setText("");
        Email.setSelection(Email.length());
        LinearLayout Email_Layout, Bio_Layout, Password_Layout, Zip_Code, Name_Layout;
        Email_Layout = main_dialog.findViewById(R.id.email_layout);
        Bio_Layout = main_dialog.findViewById(R.id.bio_layout);
        Zip_Code = main_dialog.findViewById(R.id.zipcode_layout);
        Password_Layout = main_dialog.findViewById(R.id.password_layout);
        Name_Layout = main_dialog.findViewById(R.id.name_layout);
        TextView Title = main_dialog.findViewById(R.id.heading_dialog);
        if (Dialog_type.equalsIgnoreCase("BIO")) {
            Email_Layout.setVisibility(View.GONE);
            Zip_Code.setVisibility(View.GONE);
            Name_Layout.setVisibility(View.GONE);
            Password_Layout.setVisibility(View.GONE);
            Bio_Layout.setVisibility(View.VISIBLE);
            try {
                Edit_Bio.setText(object.getString("bio"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Title.setText("UPDATE BIO");
        } else if (Dialog_type.equalsIgnoreCase("Email")) {
            Email_Layout.setVisibility(View.VISIBLE);
            Bio_Layout.setVisibility(View.GONE);
            Zip_Code.setVisibility(View.GONE);
            Name_Layout.setVisibility(View.GONE);
            Password_Layout.setVisibility(View.GONE);
            try {
                Email.setHint(object.getString("email"));
//                Email.setSelection(Email.getText().length());
                Confirm_Email.setHint(object.getString("email"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Title.setText("UPDATE EMAIL");
        } else if (Dialog_type.equalsIgnoreCase("Password")) {
            Email_Layout.setVisibility(View.GONE);
            Bio_Layout.setVisibility(View.GONE);
            Zip_Code.setVisibility(View.GONE);
            Name_Layout.setVisibility(View.GONE);
            Password_Layout.setVisibility(View.VISIBLE);
            try {
                Password.setHint(object.getString("password"));
//                Password.setSelection(Password.getText().length());
                Confirm_Password.setHint(object.getString("password"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Title.setText("UPDATE PASSWORD");
        } else if (Dialog_type.equalsIgnoreCase("Zipcode")) {
            Email_Layout.setVisibility(View.GONE);
            Bio_Layout.setVisibility(View.GONE);
            Zip_Code.setVisibility(View.VISIBLE);
            Password_Layout.setVisibility(View.GONE);
            Name_Layout.setVisibility(View.GONE);
            try {
                Zipcode.setText(object.getString("zipcode"));
                Zipcode.setSelection(Zipcode.getText().length());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Title.setText("UPDATE PHONE");
        } else if (Dialog_type.equalsIgnoreCase("NAME")) {
            Email_Layout.setVisibility(View.GONE);
            Bio_Layout.setVisibility(View.GONE);
            Zip_Code.setVisibility(View.GONE);
            Password_Layout.setVisibility(View.GONE);
            Name_Layout.setVisibility(View.VISIBLE);
            try {
                USERNAME.setText(object.getString("name"));
                USERNAME.setSelection(USERNAME.getText().length());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Title.setText("UPDATE NAME");
        }
        dialog.setView(main_dialog);
//        Animation startAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.zoomout);
//        ((ViewGroup)dialog.getWindow().getDecorView())
//                .getChildAt(0).startAnimation(startAnimation);
        return dialog;
    }

    public void SaveButtonAction() {
        JSONObject data = new JSONObject();
        try {
            data.put(Constants.TYPE_KEY, Dialog_type);
            data.put("bio", Edit_Bio.getText().toString());
            data.put("email", Email.getText().toString());
            data.put("password", Password.getText().toString());
            data.put("phone", Zipcode.getText().toString());
            data.put("username", USERNAME.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Listener.onSaveButtonClick(EditUserProfileUpdatePersonalInfoAlertDialog.this, data);
    }
}