package com.dev.codingpixel.i_launched.interfaces;

import android.os.Bundle;

/**
 * Created by incubasyss on 25/01/2018.
 */

public interface TopBarData {
    public void setData(String title, String description, boolean isNotify, Bundle bundle);

    public void callTransaction(Bundle bundle, int type);

    public void setFavIconDisplay(boolean isVisible, Object object);

    public void pressedBackButtonFromFragment();

    public void callTransactionForPurchase(Bundle bundle);

    public void callSubscriptionDialogue();

    public boolean callForTrialOver();

    public void callStatusChanged(int status);
}
