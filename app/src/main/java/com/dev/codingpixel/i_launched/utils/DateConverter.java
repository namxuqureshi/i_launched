package com.dev.codingpixel.i_launched.utils;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


public class DateConverter {
    private static PrettyTime prettyTime = new PrettyTime();

    public static String convertDateForServer(String date_string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        return timeFormat.format(myDate);
    }

    public static String convertSpecial(String date_string) {
        DateFormat dateFormatsp;
        dateFormatsp = DateFormat
                .getDateInstance(DateFormat.LONG);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date mCurrentDate = calendar.getTime();
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy");

        return dateFormatsp.format(myDate);
    }

    public static String convertSpecialChatTop(String date_string) {
        date_string = UtcToLocal(date_string);
        DateFormat dateFormatsp;
//        dateFormatsp = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,
//                DateFormat.SHORT);
//        dateFormatsp = DateFormat.getDateTimeInstance(DateFormat.SHORT);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat dateFormatHalf = new SimpleDateFormat("hh:mm aa");
        Calendar calendar = Calendar.getInstance();
        Date mCurrentDate = calendar.getTime();
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MMMM. dd 'AT' hh:mm aa");
//        Date dateTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date);
        Calendar calendaras = Calendar.getInstance();
        calendaras.setTime(myDate);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);
//        DateFormat timeFormatter = new SimpleDateFormat("hh:mma");

        if (calendaras.get(Calendar.YEAR) == today.get(Calendar.YEAR) && calendaras.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
            return "Today AT " + dateFormatHalf.format(myDate);
        } else if (calendaras.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendaras.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            return "Yesterday AT " + dateFormatHalf.format(myDate);
        } else {
            return timeFormat.format(myDate);
        }

    }

    public static String convertDateShootOut(String date_string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date mCurrentDate = calendar.getTime();
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy");

        return timeFormat.format(myDate);
    }

    public static String convertDateForShooutOut(String date_string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        Date mCurrentDate = calendar.getTime();
        Date myDate = calendar.getTime();
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy");
        if (myDate.getYear() == mCurrentDate.getYear()) {
            if (myDate.getMonth() == mCurrentDate.getMonth()) {
                if (myDate.getDate() == mCurrentDate.getDate()) {
                    return "Expire Soon!";
                }
            }
        }
        if (myDate.before(mCurrentDate)) {
            return "Expired";
        } else if (myDate.after(mCurrentDate)) {
            if (myDate.getYear() == mCurrentDate.getYear()) {
                if (myDate.getMonth() == mCurrentDate.getMonth()) {
                    if (myDate.getDate() > mCurrentDate.getDate()) {
                        int dayCount = myDate.getDate() - mCurrentDate.getDate();
                        if (dayCount >= 0 && dayCount <= 2) {
                            return "Expire Soon!";
                        } else {
                            return timeFormat.format(myDate);
                        }
                    } else {
                        return timeFormat.format(myDate);
                    }
                } else {
                    return timeFormat.format(myDate);
                }
            } else {
                return timeFormat.format(myDate);
            }

        } else {
            return timeFormat.format(myDate);
        }
    }


    public static String convertDate(String date_string) {
        date_string = UtcToLocal(date_string);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MM.dd.yyyy hh:mm aa");
        return timeFormat.format(myDate);
    }


    public static String convertDate_with_month_name(String date_string) {
        date_string = UtcToLocal(date_string);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("MMM. dd, yyyy, hh:mm aa");
        return timeFormat.format(myDate);
    }

    public static String convertDate_with_complete_details(String date_string) {
        date_string = UtcToLocal(date_string);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("EEEE,dd,MMMM,yyyy, hh:mm aa");
        return timeFormat.format(myDate);
    }


    public static String getDateAsApiFormate(String date_string) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd-MMMM, yyyy HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss");
        return LocalToUtc(timeFormat.format(myDate));
    }

    public static String getCustomDateString(String date_string) {
        date_string = UtcToLocal(date_string);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        try {
            date = dateFormat.parse(date_string);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat tmp = new SimpleDateFormat("MMMM d");

        String str = tmp.format(date);
        str = str.substring(0, 1).toUpperCase() + str.substring(1);

        if (date.getDate() > 10 && date.getDate() < 14)
            str = str + "th, ";
        else {
            if (str.endsWith("1")) str = str + "st, ";
            else if (str.endsWith("2")) str = str + "nd, ";
            else if (str.endsWith("3")) str = str + "rd, ";
            else str = str + "th, ";
        }

        tmp = new SimpleDateFormat("yyyy");
        str = str + tmp.format(date);
        return str;
    }

    public static boolean checkFor5Day(String date_string) {
        date_string = UtcToLocal(date_string);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = null;
        Calendar calendar = Calendar.getInstance();
        Date mCurrentDate = calendar.getTime();
        try {
            date = dateFormat.parse(date_string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date.getYear() == mCurrentDate.getYear()) {
            if (date.getMonth() == mCurrentDate.getMonth()) {
                if (mCurrentDate.getDate() > date.getDate()) {
                    int count = mCurrentDate.getDate() - date.getDate();
                    if (count >= 5) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public static String getPrettyTime(String s) {
        try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            date = utcFormat.parse(s);
            SimpleDateFormat localTimeFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            localTimeFormate.setTimeZone(TimeZone.getDefault());
            Date dt = localTimeFormate.parse(localTimeFormate.format(date));
//            prettyTime.
//            return prettyTime.approximateDuration(dt).getQuantity() + " remaining";
            return prettyTime.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getPrettyTimeRemaining(String s) {
        try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            date = utcFormat.parse(s);
            SimpleDateFormat localTimeFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            localTimeFormate.setTimeZone(TimeZone.getDefault());
            Date dt = localTimeFormate.parse(localTimeFormate.format(date));
//            prettyTime.
            return prettyTime.approximateDuration(dt).getQuantity() + " remaining";
//            return prettyTime.format(dt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String LocalToUtc(String s) {
        try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            utcFormat.setTimeZone(TimeZone.getDefault());
            Date date = null;
            date = utcFormat.parse(s);
            SimpleDateFormat localTimeFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            localTimeFormate.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date dt = localTimeFormate.parse(localTimeFormate.format(date));
//            prettyTime.
            return localTimeFormate.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String UtcToLocal(String s) {
        try {
            DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date date = null;
            date = utcFormat.parse(s);
            SimpleDateFormat localTimeFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
            localTimeFormate.setTimeZone(TimeZone.getDefault());
            Date dt = localTimeFormate.parse(localTimeFormate.format(date));
//            prettyTime.
            return localTimeFormate.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String validateFormat(String trim) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
        Date date = null;
        try {
            date = dateFormat.parse(trim);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat tmp = new SimpleDateFormat("yyyy-MM-dd");
        return tmp.format(date);

    }
}
