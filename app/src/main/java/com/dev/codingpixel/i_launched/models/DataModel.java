package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by incubasyss on 29/01/2018.
 */

public class DataModel implements Parcelable {


    private String company_name, shortDescription, description, user_name, created_at;
    private String longDescription, status;
    private String user_image, category_title, sub_title;
    private int Days;
    private int isLaunched;
    private int company_id;
    private int id, user_id, category_id, can_share;
    private boolean isFavorite;
    private int isNdaSign;

    public int getIsNdaSign() {
        return isNdaSign;
    }

    public void setIsNdaSign(int isNdaSign) {
        this.isNdaSign = isNdaSign;
    }

    public int getIsLaunched() {
        return isLaunched;
    }

    public void setIsLaunched(int isLaunched) {
        this.isLaunched = isLaunched;
    }

    public int getCompany_id() {
        return company_id;
    }

    public void setCompany_id(int company_id) {
        this.company_id = company_id;
    }

    public String getSub_title() {
        return sub_title;
    }

    public void setCategorySubTitle(String sub_title) {
        this.sub_title = sub_title;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategoryTitle(String category_title) {
        this.category_title = category_title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }


    public String getStatus() {
        return status;
    }

    public void setIdeaStatus(String status) {
        this.status = status;
    }

    public int getDays() {
        return Days;
    }

    public void setDays(int days) {
        Days = days;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setDetailDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public DataModel() {
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCategory_title(String category_title) {
        this.category_title = category_title;
    }

    public void setSub_title(String sub_title) {
        this.sub_title = sub_title;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getCan_share() {
        return can_share;
    }

    public void setCan_share(int can_share) {
        this.can_share = can_share;
    }

    public DataModel(String company_name, String shortDescription, String description, String user_name, String created_at) {
        this.company_name = company_name;
        this.shortDescription = shortDescription;
        this.description = description;
        this.user_name = user_name;
        this.created_at = created_at;
    }

    public DataModel(String company_name, String shortDescription, String description, String user_name, String created_at, String user_image) {
        this.company_name = company_name;
        this.shortDescription = shortDescription;
        this.description = description;
        this.user_name = user_name;
        this.created_at = created_at;
        this.user_image = user_image;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setIdeaTitle(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setShortDescription(String description) {
        this.description = description;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.company_name);
        dest.writeString(this.shortDescription);
        dest.writeString(this.description);
        dest.writeString(this.user_name);
        dest.writeString(this.created_at);
        dest.writeString(this.longDescription);
        dest.writeString(this.status);
        dest.writeString(this.user_image);
        dest.writeString(this.category_title);
        dest.writeString(this.sub_title);
        dest.writeInt(this.Days);
        dest.writeInt(this.isLaunched);
        dest.writeInt(this.company_id);
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.category_id);
        dest.writeInt(this.can_share);
        dest.writeByte(this.isFavorite ? (byte) 1 : (byte) 0);
        dest.writeInt(this.isNdaSign);
    }

    protected DataModel(Parcel in) {
        this.company_name = in.readString();
        this.shortDescription = in.readString();
        this.description = in.readString();
        this.user_name = in.readString();
        this.created_at = in.readString();
        this.longDescription = in.readString();
        this.status = in.readString();
        this.user_image = in.readString();
        this.category_title = in.readString();
        this.sub_title = in.readString();
        this.Days = in.readInt();
        this.isLaunched = in.readInt();
        this.company_id = in.readInt();
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.category_id = in.readInt();
        this.can_share = in.readInt();
        this.isFavorite = in.readByte() != 0;
        this.isNdaSign = in.readInt();
    }

    public static final Parcelable.Creator<DataModel> CREATOR = new Parcelable.Creator<DataModel>() {
        @Override
        public DataModel createFromParcel(Parcel source) {
            return new DataModel(source);
        }

        @Override
        public DataModel[] newArray(int size) {
            return new DataModel[size];
        }
    };
}
