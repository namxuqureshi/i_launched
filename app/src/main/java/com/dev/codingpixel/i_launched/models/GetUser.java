package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by incubasyss on 13/02/2018.
 */
public class GetUser implements Parcelable {

    @SerializedName(Constants.ID_KEY)
    @Expose
    private Integer id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("company_name")
    @Expose
    private String companyName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public GetUser withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GetUser withUsername(String username) {
        this.username = username;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public GetUser withCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeString(this.username);
        dest.writeString(this.companyName);
    }

    public GetUser() {
    }

    protected GetUser(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.username = in.readString();
        this.companyName = in.readString();
    }

    public static final Parcelable.Creator<GetUser> CREATOR = new Parcelable.Creator<GetUser>() {
        @Override
        public GetUser createFromParcel(Parcel source) {
            return new GetUser(source);
        }

        @Override
        public GetUser[] newArray(int size) {
            return new GetUser[size];
        }
    };
}