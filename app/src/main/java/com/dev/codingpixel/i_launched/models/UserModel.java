package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by incubasyss on 02/02/2018.
 */


public class UserModel implements Parcelable {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("user_status")
    @Expose
    private Integer userStatus;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("about_us")
    @Expose
    private String aboutUs;
    @SerializedName("profile_pic")
    @Expose
    private String profilePic;
    @SerializedName("session_token")
    @Expose
    private String sessionToken;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("email_token")
    @Expose
    private String emailToken;
    @SerializedName("token_generated")
    @Expose
    private String tokenGenerated;
    @SerializedName("actual_login")
    @Expose
    private String actualLogin;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("login_attempts")
    @Expose
    private Integer loginAttempts;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("mission")
    @Expose
    private String mission;
    @SerializedName("year_founded")
    @Expose
    private String year_founded;
    @SerializedName("post_ideas")
    @Expose
    private Integer postIdeas;
    @SerializedName("favorite_ideas")
    @Expose
    private Integer favoriteIdeas;
    @SerializedName("unread")
    @Expose
    private Integer unread;
    @SerializedName("get_subscription")
    @Expose
    private GetSubModel getSubscription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserModel withId(Integer id) {
        this.id = id;
        return this;
    }

    public String getMission() {
        return mission;
    }

    public void setMission(String mission) {
        this.mission = mission;
    }

    public String getYear_founded() {
        return year_founded;
    }

    public void setYear_founded(String year_founded) {
        this.year_founded = year_founded;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public UserModel withType(Integer type) {
        this.type = type;
        return this;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public UserModel withUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserModel withUsername(String username) {
        this.username = username;
        return this;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public UserModel withCompanyName(String companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserModel withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserModel withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
    }

    public UserModel withAboutUs(String aboutUs) {
        this.aboutUs = aboutUs;
        return this;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public UserModel withProfilePic(String profilePic) {
        this.profilePic = profilePic;
        return this;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public UserModel withSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
        return this;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public UserModel withDeviceType(String deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UserModel withDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public UserModel withEmailToken(String emailToken) {
        this.emailToken = emailToken;
        return this;
    }

    public String getTokenGenerated() {
        return tokenGenerated;
    }

    public void setTokenGenerated(String tokenGenerated) {
        this.tokenGenerated = tokenGenerated;
    }

    public UserModel withTokenGenerated(String tokenGenerated) {
        this.tokenGenerated = tokenGenerated;
        return this;
    }

    public String getActualLogin() {
        return actualLogin;
    }

    public void setActualLogin(String actualLogin) {
        this.actualLogin = actualLogin;
    }

    public UserModel withActualLogin(String actualLogin) {
        this.actualLogin = actualLogin;
        return this;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public UserModel withLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public UserModel withLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public UserModel withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserModel withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Integer getPostIdeas() {
        return postIdeas;
    }

    public void setPostIdeas(Integer postIdeas) {
        this.postIdeas = postIdeas;
    }

    public UserModel withPostIdeas(Integer postIdeas) {
        this.postIdeas = postIdeas;
        return this;
    }

    public Integer getFavoriteIdeas() {
        return favoriteIdeas;
    }

    public void setFavoriteIdeas(Integer favoriteIdeas) {
        this.favoriteIdeas = favoriteIdeas;
    }

    public UserModel withFavoriteIdeas(Integer favoriteIdeas) {
        this.favoriteIdeas = favoriteIdeas;
        return this;
    }

    public Integer getUnread() {
        return unread;
    }

    public void setUnread(Integer unread) {
        this.unread = unread;
    }

    public UserModel withUnread(Integer unread) {
        this.unread = unread;
        return this;
    }

    public GetSubModel getGetSubscription() {
        return getSubscription;
    }

    public void setGetSubscription(GetSubModel getSubscription) {
        this.getSubscription = getSubscription;
    }

    public UserModel withGetSubscription(GetSubModel getSubscription) {
        this.getSubscription = getSubscription;
        return this;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.type);
        dest.writeValue(this.userStatus);
        dest.writeString(this.username);
        dest.writeString(this.companyName);
        dest.writeString(this.email);
        dest.writeString(this.phone);
        dest.writeString(this.aboutUs);
        dest.writeString(this.profilePic);
        dest.writeString(this.sessionToken);
        dest.writeString(this.deviceType);
        dest.writeString(this.deviceId);
        dest.writeString(this.emailToken);
        dest.writeString(this.tokenGenerated);
        dest.writeString(this.actualLogin);
        dest.writeString(this.lastLogin);
        dest.writeValue(this.loginAttempts);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
        dest.writeString(this.mission);
        dest.writeString(this.year_founded);
        dest.writeValue(this.postIdeas);
        dest.writeValue(this.favoriteIdeas);
        dest.writeValue(this.unread);
        dest.writeParcelable(this.getSubscription, flags);
    }

    public UserModel() {
    }

    protected UserModel(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.userStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.username = in.readString();
        this.companyName = in.readString();
        this.email = in.readString();
        this.phone = in.readString();
        this.aboutUs = in.readString();
        this.profilePic = in.readString();
        this.sessionToken = in.readString();
        this.deviceType = in.readString();
        this.deviceId = in.readString();
        this.emailToken = in.readString();
        this.tokenGenerated = in.readString();
        this.actualLogin = in.readString();
        this.lastLogin = in.readString();
        this.loginAttempts = (Integer) in.readValue(Integer.class.getClassLoader());
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
        this.mission = in.readString();
        this.year_founded = in.readString();
        this.postIdeas = (Integer) in.readValue(Integer.class.getClassLoader());
        this.favoriteIdeas = (Integer) in.readValue(Integer.class.getClassLoader());
        this.unread = (Integer) in.readValue(Integer.class.getClassLoader());
        this.getSubscription = in.readParcelable(GetSubModel.class.getClassLoader());
    }

    public static final Parcelable.Creator<UserModel> CREATOR = new Parcelable.Creator<UserModel>() {
        @Override
        public UserModel createFromParcel(Parcel source) {
            return new UserModel(source);
        }

        @Override
        public UserModel[] newArray(int size) {
            return new UserModel[size];
        }
    };
}