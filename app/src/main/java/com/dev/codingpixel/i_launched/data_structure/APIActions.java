package com.dev.codingpixel.i_launched.data_structure;

public class APIActions {
    public static enum ApiActions {
        register, login, update_profile_info, update_user_email, send_message, logout, my_ideas, my_categories, create_idea, get_idea_by_id, all_category, delete_idea, forget_password, dashboard, ideas_by_category_id, search_all_ideas, my_favourite, mark_as_ilaunched, message_list_by_idea, message_list, connect_innovator, read_all_notifications, get_all_notifications, read_notification, i_aunched, approve_ilaunch_request, reject_ilaunch_request, message_detail, message_send, add_subscription_data, ideas_by_cat_id, can_share_on_fb, mark_as_digital_sign, update_status
    }
}
