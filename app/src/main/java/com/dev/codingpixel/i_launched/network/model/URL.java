package com.dev.codingpixel.i_launched.network.model;

public class URL {
    public static String getLocatioZipcode_part1 = "http://maps.google.com/maps/api/geocode/json?components=country%3a";
    public static String getLocatioZipcode_part2 = "%7Cpostal_code:";
    public static String getLocatioZipcode_part3 = "&sensor=true";
    private static final String baseurl = "http://139.162.37.73/ilaunched/api/";

    public static final String login = baseurl + "login";
    public static final String forget_password = baseurl + "forget_password";
    public static final String logout = baseurl + "logout";
    public static final String dashboard = baseurl + "dashboard";
    public static final String register = baseurl + "register";
    public static final String my_ideas = baseurl + "my_ideas";
    public static final String i_aunched = baseurl + "i_aunched";
    public static final String search_all_ideas = baseurl + "search_all_ideas";
    public static final String search_by_category = baseurl + "search_by_category";
    public static final String my_favourite = baseurl + "my_favourite";
    public static final String ideas_by_category_id = baseurl + "ideas_by_category_id";
    public static final String mark_as_digital_sign = baseurl + "mark_as_digital_sign";
//    http://localhost/ilaunchapi/api/mark_as_digital_sign
    public static final String remove_favourite = baseurl + "remove_favourite";
    public static final String mark_favourite = baseurl + "mark_favourite";
    public static final String mark_as_ilaunched = baseurl + "mark_as_ilaunched";
    public static final String approve_ilaunch_request = baseurl + "approve_ilaunch_request";
    public static final String reject_ilaunch_request = baseurl + "reject_ilaunch_request";
    public static final String my_category = baseurl + "my_category";
    public static final String all_category = baseurl + "all_category";
    public static final String create_idea = baseurl + "create_idea";
    public static final String get_idea_by_id = baseurl + "get_idea_by_id";
    public static final String delete_idea = baseurl + "delete_idea";
    public static final String connect_innovator = baseurl + "connect_innovator";
    public static final String message_list_by_idea = baseurl + "message_list_by_idea";
    public static final String message_list = baseurl + "message_list";//
    public static final String message_detail = baseurl + "message_detail";//
    public static final String send_message = baseurl + "send_message";//
    public static final String update_profile = baseurl + "update_profile";//POST
    public static final String get_all_notifications = baseurl + "get_all_notifications";//GET
    public static final String read_all_notifications = baseurl + "read_all_notifications";//paginated GET
    public static final String read_notification = baseurl + "read_notification";//read only 1
    public static final String add_subscriptin = baseurl + "add_subscriptin";//POST 'subscription_id' => 'required','package_name' => 'required','activated_at' => 'required','expire_at' => 'required'
    public static final String update_status = baseurl + "update_status";//POST param: status= 1 OR 2
    public static final String SOCKET_URL = "http://139.162.37.73:5001/";
    public static final String ideas_by_cat_id = baseurl + "ideas_by_cat_id";
    public static final String can_share_on_fb = baseurl + "can_share_on_fb";
    public static final String contact_us = baseurl + "contact_us";
//    http://139.162.37.73/ilaunched/api/contact_us
//http://139.162.37.73/ilaunched/api/can_share_on_fb
    //SUBSCRIPTION URLS
//    public static final String GET_STATUS_SUBS_1 = "https://www.googleapis.com/androidpublisher/v2/applications/{packageName}/purchases/subscriptions/{subscriptionId}/tokens/{token}?key={yorApiKey}";//GET
    public static final String GET_STATUS_SUBS_1 = "https://www.googleapis.com/androidpublisher/v2/applications/";//GET {packageName}
    public static final String GET_STATUS_SUBS_2 = "/purchases/subscriptions/";//GET {subscriptionId}
    public static final String GET_STATUS_SUBS_3 = "/tokens/";//GET {token}
    public static final String GET_STATUS_SUBS_4 = "?key=AIzaSyCiBSAw7eDGumIJzO7O0sdgCA3pZiFcfz8";//GET {yorApiKey}

    //    public static final String GET_SUBS_CANCEL_1 = "https://www.googleapis.com/androidpublisher/v2/applications/{packageName}/purchases/subscriptions/{subscriptionId}/tokens/{token}:cancel?key={yorApiKey}";//POST
    public static final String GET_SUBS_CANCEL_1 = "https://www.googleapis.com/androidpublisher/v2/applications/";//POST {packageName}
    public static final String GET_SUBS_CANCEL_2 = "/purchases/subscriptions/";//POST {subscriptionId}
    public static final String GET_SUBS_CANCEL_3 = "/tokens/";//POST {token}
    public static final String GET_SUBS_CANCEL_4 = ":cancel?key=AIzaSyCiBSAw7eDGumIJzO7O0sdgCA3pZiFcfz8";//POST {yorApiKey}

    public static final String API_KEY_GOOGLE = "AIzaSyCiBSAw7eDGumIJzO7O0sdgCA3pZiFcfz8";
    public static final String API_KEY_GOOGLE_CLIEND_ID = "554611293504-1bhr2jkphha86nmohlfhd6ggegfmj5os.apps.googleusercontent.com";
    public static final String ACCESS_TOKEN = "4/AAB1Hnsn3gm2GGUnxAOkfdrzGovz6HpuzYb1HVHazbVsWECcmNLRCbM";
}