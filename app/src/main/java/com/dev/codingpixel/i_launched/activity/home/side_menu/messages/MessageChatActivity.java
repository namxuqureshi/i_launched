package com.dev.codingpixel.i_launched.activity.home.side_menu.messages;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.MsgChatRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.eventbus.MessageEvent;
import com.dev.codingpixel.i_launched.models.GetIdea;
import com.dev.codingpixel.i_launched.models.GetUser;
import com.dev.codingpixel.i_launched.models.MsgChatModel;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.IntentFunction;
import com.dev.codingpixel.i_launched.static_function.UIModification;
import com.dev.codingpixel.i_launched.utils.SocketIO;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.NOTIFICATION_RESULT_CODE;
import static com.dev.codingpixel.i_launched.utils.DateConverter.convertSpecialChatTop;

public class MessageChatActivity extends AppCompatActivity implements MsgChatRecyclerAdapter.ItemClickListener, APIResponseListner, SocketIO.SocketCallBack {

    RecyclerView message_chat_recycler;
    EditText edit_message;
    Button btn_send;
    ImageView back_btn, home_notification;
    List<MsgChatModel> data = new ArrayList<>();
    MsgChatRecyclerAdapter recyler_adapter;
    LinearLayoutManager layoutManager;
    int threadId = 0, ideaId = 0;
    private GetIdea getIdea = new GetIdea();
    private GetUser getUser = new GetUser();
    private GetUser getUserOther = new GetUser();
    private String time = "";
    //TOP VAR
    TextView title_text, user_name, date_message, notify_counter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIModification.FullScreenWithTransparentStatusBarChat(MessageChatActivity.this, "#c8171717");
        setContentView(R.layout.activity_message_chat);
        userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);
        threadId = getIntent().getIntExtra(Constants.THREAD_ID_KEY, 0);
        SocketIO.sharedInstance.Get(MessageChatActivity.this);
        getWindow().setBackgroundDrawableResource(R.drawable.bg_message_list_chat);
        InitView();
        InitRecyclerView();
        InitListener();
        callForDetailsListMessages();

    }

    /**
     * Conversation Details list
     */
    void callForDetailsListMessages() {
        new VollyAPICall(MessageChatActivity.this
                , true
                , URL.message_detail + "/" + threadId
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , MessageChatActivity.this, APIActions.ApiActions.message_detail);

    }

    /**
     * Setting Listener
     */
    private void InitListener() {
        btn_send.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                UIModification.HideKeyboard(MessageChatActivity.this);
                if (edit_message.getText().toString().trim().length() > 0) {
                    JSONObject object = new JSONObject();
                    try {
                        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                            object.put("user_id", getUserOther.getId());
                        } else {
                            object.put("user_id", getUser.getId());
                        }
                        object.put("other_id", userLoggedIn.getId());
                        object.put("message", edit_message.getText().toString().trim());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SocketIO.sharedInstance.Send(object, MessageChatActivity.this);
                    data.add(new MsgChatModel(true, edit_message.getText().toString().trim(), getCurrentDate(), ""));
                    recyler_adapter.notifyItemInserted((data.size()));
                    recyler_adapter.notifyDataSetChanged();
                    message_chat_recycler.scrollToPosition(recyler_adapter.getItemCount() - 1);
                    layoutManager.scrollToPosition(recyler_adapter.getItemCount() - 1);

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Constants.IDEA_ID_KEY, getIdea.getId());
                        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                            jsonObject.put("receiver_id", getUserOther.getId());
                        } else {
                            jsonObject.put("receiver_id", getUser.getId());
                        }
                        jsonObject.put("message", edit_message.getText().toString());
                        callForSendMessage(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    edit_message.setText("");
                }
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageChatActivity.super.onBackPressed();
            }
        });
        home_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                notify_counter.setVisibility(View.GONE);
                IntentFunction.GoToNotification(MessageChatActivity.this);
            }
        });
    }

    /**
     * @param jsonObject param for sending messages
     * @throws JSONException if occure
     */
    void callForSendMessage(JSONObject jsonObject) throws JSONException {
        new VollyAPICall(MessageChatActivity.this
                , false
                , URL.send_message
                , jsonObject
                , userLoggedIn.getSessionToken()
                , POST
                , MessageChatActivity.this, APIActions.ApiActions.send_message);
    }

    /**
     * setting recycler views
     */
    private void InitRecyclerView() {

        layoutManager = new LinearLayoutManager(MessageChatActivity.this);
        layoutManager.setStackFromEnd(true);
        message_chat_recycler.setLayoutManager(layoutManager);
        RecyclerView.ItemAnimator animator = message_chat_recycler.getItemAnimator();
        if (animator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) animator).setSupportsChangeAnimations(false);
        }
        recyler_adapter = new MsgChatRecyclerAdapter(MessageChatActivity.this, data);
        recyler_adapter.setClickListener(MessageChatActivity.this);
        message_chat_recycler.setAdapter(recyler_adapter);
        recyler_adapter.notifyItemInserted(data.size());
        message_chat_recycler.scrollToPosition(recyler_adapter.getItemCount() - 1);
        layoutManager.scrollToPosition(recyler_adapter.getItemCount() - 1);

    }


    /**
     * Settting ID's
     */
    private void InitView() {
        message_chat_recycler = findViewById(R.id.message_chat_recycler);
        edit_message = findViewById(R.id.edit_message);
        btn_send = findViewById(R.id.btn_send);
        home_notification = findViewById(R.id.home_notification);
        back_btn = findViewById(R.id.back_btn);
        title_text = findViewById(R.id.title_text);
        user_name = findViewById(R.id.user_name);
        date_message = findViewById(R.id.date_message);
        notify_counter = findViewById(R.id.notify_counter);
    }

    @Override
    public void onItemClick(View view, int position) {

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTIFICATION_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras().getBundle("result");
                setResult(RESULT_OK, new Intent().putExtra("result", bundle));
                finish();
                //
            }
        }

    }


    /**
     * @return for message screen time display
     */
    public static String getCurrentDate() {
        DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"), Locale.ENGLISH);
        Date mCurrentDate = calendar.getTime();
        return utcFormat.format(mCurrentDate.getTime());
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {

        try {
            JSONObject object = null;
            object = new JSONObject(response);
            if (apiActions == APIActions.ApiActions.message_detail) {
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        data.clear();
                        JSONArray jsonArray = object.getJSONArray("successData");
                        for (int index = 0; index < jsonArray.length(); index++) {
                            MsgChatModel e;
                            if (userLoggedIn.getId() == jsonArray.getJSONObject(index).getInt("sender_id")) {
                                e = new MsgChatModel(true, jsonArray.getJSONObject(index).getString("message"), jsonArray.getJSONObject(index).getString("created_at"), "");
                                e.setSenderId(jsonArray.getJSONObject(index).getInt("sender_id"));
                            } else {
                                e = new MsgChatModel(jsonArray.getJSONObject(index).getString("message"), jsonArray.getJSONObject(index).getString("created_at"), "", false);
                                e.setReceiverId(jsonArray.getJSONObject(index).getInt("sender_id"));
                            }
                            e.setCreated_at(jsonArray.getJSONObject(index).getString("created_at"));
                            e.setUpdated_at(jsonArray.getJSONObject(index).getString("updated_at"));
                            e.setId(jsonArray.getJSONObject(index).getInt("id"));
                            e.setThreadId(jsonArray.getJSONObject(index).getInt(Constants.THREAD_ID_KEY));
                            data.add(e);
                            recyler_adapter.notifyItemInserted(data.size());
                            message_chat_recycler.scrollToPosition(recyler_adapter.getItemCount() - 1);
                            getIdea.setTitle(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONArray("get_ideas").getJSONObject(0).getString("title"));
                            getIdea.setId(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONArray("get_ideas").getJSONObject(0).getInt("id"));
                            getUser.setUsername(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONObject("get_user").getString("username"));
                            getUser.setId(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONObject("get_user").getInt("id"));
                            getUserOther.setUsername(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONObject("get_receiver_user").getString("username"));
                            getUserOther.setId(jsonArray.getJSONObject(index).getJSONArray("get_detail").getJSONObject(0).getJSONObject("get_receiver_user").getInt("id"));
                            time = jsonArray.getJSONObject(index).getString("created_at");
                        }
                        if (jsonArray.length() < 1) {
                            getIdea.setTitle(object.getJSONObject("header_detail").getJSONArray("get_ideas").getJSONObject(0).getString("title"));
                            getIdea.setId(object.getJSONObject("header_detail").getJSONArray("get_ideas").getJSONObject(0).getInt("id"));
                            getUser.setUsername(object.getJSONObject("header_detail").getJSONObject("get_user").getString("username"));
                            getUser.setId(object.getJSONObject("header_detail").getJSONObject("get_user").getInt("id"));
                            getUserOther.setUsername(object.getJSONObject("header_detail").getJSONObject("get_receiver_user").getString("username"));
                            getUserOther.setId(object.getJSONObject("header_detail").getJSONObject("get_receiver_user").getInt("id"));
                            time = object.getJSONObject("header_detail").getString("created_at");
                        }
                        setTopView();

                    }
                }
            } else if (apiActions == APIActions.ApiActions.message_send) {
                Log.d("onRequestSuccess: ", response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     * Setting Top Views Values
     */
    private void setTopView() {
        title_text.setText(getIdea.getTitle());
        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            user_name.setText(getUserOther.getUsername());
        } else {
            user_name.setText(getUser.getUsername());
        }
        if (!time.equalsIgnoreCase("")) {
            date_message.setText(convertSpecialChatTop(time));
        } else {
            date_message.setText("");
        }

    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param event var for giving message to this activity
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (home_notification != null && notify_counter != null) {
            if (event.isNotify) {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell_blue));
                notify_counter.setVisibility(View.GONE);
//                userNotify.setUnread(userNotify.getUnread() + 1);
//                notify_counter.setText("" + userNotify.getUnread());
            } else {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                notify_counter.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void GetCallBack(final Object... args) {
        MessageChatActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                JSONObject json_data = (JSONObject) args[0];
                try {
                    if (json_data.getInt("user_id") == userLoggedIn.getId()) {
                        MsgChatModel dataModel = new MsgChatModel();

                        if (userLoggedIn.getId() == json_data.getInt("other_id")) {
                            dataModel.setSender(true);
                        } else {
                            dataModel.setSender(false);
                        }
                        dataModel.setMessage(json_data.getString("message"));
                        dataModel.setDate(getCurrentDate());
                        data.add(dataModel);
                        recyler_adapter.notifyItemInserted(data.size());
                        message_chat_recycler.scrollToPosition(recyler_adapter.getItemCount() - 1);
                        message_chat_recycler.scrollToPosition(recyler_adapter.getItemCount() - 1);
                    }
                } catch (JSONException e) {
                    return;
                }

            }
        });

    }

    @Override
    public void SendCallBack() {

    }
}
