package com.dev.codingpixel.i_launched.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.models.DataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.utils.DateConverter;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.android.volley.Request.Method.GET;
import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

/**
 * Created by incubasyss on 24/01/2018.
 */

public class HomeRecyclerAdapter extends RecyclerView.Adapter<HomeRecyclerAdapter.ViewHolder> implements APIResponseListner {
    private LayoutInflater mInflater;
    ArrayList<DataModel> mData = new ArrayList<>();
    private HomeRecyclerAdapter.ItemClickListener mClickListener;
    private ItemClickListenerPositionore mClickListenerPos;
    private Context mContext;

    public HomeRecyclerAdapter(Context context, ArrayList<DataModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.home_fragment_item, parent, false);
        HomeRecyclerAdapter.ViewHolder viewHolder = new HomeRecyclerAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (mData.get(position).getSub_title().equalsIgnoreCase("null")) {
            holder.company_name.setText(mData.get(position).getCategory_title());
        } else {
            holder.company_name.setText(mData.get(position).getSub_title());
        }
        if (!mData.get(position).getStatus().equalsIgnoreCase("false")) {
            holder.social_share.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setImageDrawable(mContext.getDrawable(R.drawable.ic_launched_btn));
            holder.btween_view.setVisibility(View.VISIBLE);
            holder.btn_fb_share.setVisibility(View.GONE);
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                if (mData.get(position).getCompany_id() == userLoggedIn.getId()) {
                    holder.btn_fb_share.setVisibility(View.VISIBLE);
                } else {
                    holder.btn_fb_share.setVisibility(View.GONE);
                }
            } else {
                if (mData.get(position).getCan_share() == 1) {
                    holder.btn_fb_share.setVisibility(View.VISIBLE);
                } else {
                    holder.btn_fb_share.setVisibility(View.GONE);
                }
            }
        } else {
            holder.social_share.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setVisibility(View.VISIBLE);
            holder.btn_ilaunched.setImageDrawable(mContext.getDrawable(R.drawable.i_launched_non));
            holder.btween_view.setVisibility(View.VISIBLE);
            holder.btn_fb_share.setVisibility(View.GONE);

        }

        holder.company_name_text.setText(mData.get(position).getShortDescription());
        holder.user_name.setText(mData.get(position).getUser_name());
        holder.description.setText(mData.get(position).getDescription());

        if (DateConverter.getPrettyTime(mData.get(position).getCreated_at()).contains("minutes")) {

            holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreated_at()).replace("minutes", "min"));
        } else {
            holder.date.setText(DateConverter.getPrettyTime(mData.get(position).getCreated_at()));
        }

        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            holder.start_tick_main.setVisibility(View.VISIBLE);
            if (mData.get(position).isFavorite()) {
                holder.start_tick.setImageDrawable(holder.start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_fill));
            } else {
                holder.start_tick.setImageDrawable(holder.start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_unfill));
            }
        } else {
            holder.start_tick_main.setVisibility(View.GONE);
        }
        holder.btn_ilaunched.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (mData.get(position).getIsLaunched() == 0 && userLoggedIn.getUserStatus() < Constants.PAID_USER) {
//                    if (mClickListenerPos != null) {
//                        mClickListenerPos.onItemClickPos(v, position);
//                    }
//                }
                if (mData.get(position).getIsLaunched() == 0) {
                    if (mClickListenerPos != null) {
                        mClickListenerPos.onItemClickPos(v, position);
                    }
                }

            }
        });
        holder.btn_fb_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (userLoggedIn.getType() == Constants.TYPE_COMPANY && mData.get(position).getCan_share() == 0) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                            .setTitleText("Are you sure?")
                            .setContentText("You want to allow your innovator to share this Idea on Facebook?")
                            .setCustomImage(R.drawable.share_ic)
                            .setConfirmText("Yes")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    try {
                                        new VollyAPICall(v.getContext()
                                                , false
                                                , URL.can_share_on_fb
                                                , new JSONObject().put("idea_id", mData.get(position).getId()).put("can_share", 1)
                                                , userLoggedIn.getSessionToken()
                                                , POST
                                                , HomeRecyclerAdapter.this, APIActions.ApiActions.can_share_on_fb);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mData.get(position).setCan_share(1);
                                    notifyItemChanged(position);
                                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                                            .setContentTitle("Idea Launched using i-Launched Application!!")
                                            .setContentDescription("Idea Launched using i-Launched Application!!")
                                            .setRef("Idea Launched using i-Launched Application!!")
                                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                                            .build();
                                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                                }
                            }).setCancelText("No")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    try {
                                        new VollyAPICall(v.getContext()
                                                , false
                                                , URL.can_share_on_fb
                                                , new JSONObject().put("idea_id", mData.get(position).getId()).put("can_share", 2)
                                                , userLoggedIn.getSessionToken()
                                                , POST
                                                , HomeRecyclerAdapter.this, APIActions.ApiActions.can_share_on_fb);
                                        mData.get(position).setCan_share(2);
                                        notifyItemChanged(position);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                                            .setContentTitle("Idea Launched using i-Launched Application!!")
                                            .setContentDescription("Idea Launched using i-Launched Application!!")
                                            .setRef("Idea Launched using i-Launched Application!!")
                                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                                            .build();
                                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);

                                }
                            }).show();
                } else if (userLoggedIn.getType() == Constants.TYPE_USER && mData.get(position).getCan_share() == 1) {
                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                            .setContentTitle("Idea Launched using i-Launched Application!!")
                            .setContentDescription("Idea Launched using i-Launched Application!!")
                            .setRef("Idea Launched using i-Launched Application!!")
                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                            .build();
                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                } else if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                    ShareLinkContent shareContent = new ShareLinkContent.Builder()
                            .setContentTitle("Idea Launched using i-Launched Application!!")
                            .setContentDescription("Idea Launched using i-Launched Application!!")
                            .setRef("Idea Launched using i-Launched Application!!")
                            .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
                            .build();
                    ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
                    shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);
                }
//                ShareLinkContent shareContent = new ShareLinkContent.Builder()
//                        .setContentTitle("Idea Launched using i-Launched Application!!")
//                        .setContentDescription("Idea Launched using i-Launched Application!!")
//                        .setRef("Idea Launched using i-Launched Application!!")
//                        .setContentUrl(Uri.parse(v.getContext().getString(R.string.fb_share_url)))
//                        .build();
//                ShareDialog shareDialog = new ShareDialog((Activity) v.getContext());
//                shareDialog.show(shareContent, ShareDialog.Mode.AUTOMATIC);


            }
        });
        holder.start_tick_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mData.get(position).isFavorite()) {
                    mData.get(position).setFavorite(false);
                    notifyDataSetChanged();
                    new VollyAPICall(v.getContext()
                            , false
                            , URL.remove_favourite + "/" + mData.get(position).getId()
                            , new JSONObject()
                            , userLoggedIn.getSessionToken()
                            , GET
                            , HomeRecyclerAdapter.this, APIActions.ApiActions.ideas_by_category_id);
                    holder.start_tick.setImageDrawable(holder.start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_unfill));
                } else {
                    mData.get(position).setFavorite(true);
                    notifyDataSetChanged();
                    new VollyAPICall(v.getContext()
                            , false
                            , URL.mark_favourite + "/" + mData.get(position).getId()
                            , new JSONObject()
                            , userLoggedIn.getSessionToken()
                            , GET
                            , HomeRecyclerAdapter.this, APIActions.ApiActions.ideas_by_category_id);
                    holder.start_tick.setImageDrawable(holder.start_tick.getContext().getResources().getDrawable(R.drawable.ic_star_fill));
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {

    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject object = new JSONObject(response);
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                    new SweetAlertDialog(mContext, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Error!!")
                            .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout main_view, start_tick_main;
        ImageView user_image, start_tick;
        AppCompatTextView user_name, date, company_name, company_name_text, description;
        LinearLayout social_share;
        ImageView btn_fb_share, btn_ilaunched;
        View btween_view;


        public ViewHolder(View itemView) {
            super(itemView);
            user_image = itemView.findViewById(R.id.user_image);
            start_tick_main = itemView.findViewById(R.id.start_tick_main);
            main_view = itemView.findViewById(R.id.main_view);
            btween_view = itemView.findViewById(R.id.btween_view);
            user_name = itemView.findViewById(R.id.user_name);
            date = itemView.findViewById(R.id.date);
            start_tick = itemView.findViewById(R.id.start_tick);
            company_name = itemView.findViewById(R.id.company_name);
            company_name_text = itemView.findViewById(R.id.company_name_text);
            description = itemView.findViewById(R.id.description);
            description.setOnClickListener(this);
            btn_fb_share = itemView.findViewById(R.id.btn_fb_share);
            btn_ilaunched = itemView.findViewById(R.id.btn_ilaunched);
            social_share = itemView.findViewById(R.id.social_share);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickListener != null) {
                mClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }


    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void setClickListenerPosition(ItemClickListenerPositionore itemClickListener) {
        this.mClickListenerPos = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface ItemClickListenerPositionore {
        void onItemClickPos(View view, int position);
    }
}
