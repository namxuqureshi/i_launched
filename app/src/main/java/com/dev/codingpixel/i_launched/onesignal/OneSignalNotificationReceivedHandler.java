package com.dev.codingpixel.i_launched.onesignal;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;
import com.dev.codingpixel.i_launched.activity.home.HomeActivity;
import com.dev.codingpixel.i_launched.eventbus.MessageEvent;
import com.dev.codingpixel.i_launched.models.Notification;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.Gson;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.math.BigInteger;


//This will be called when a notification is received while your app is running.
public class OneSignalNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;
        String customKey;
        JSONObject jsonObject = notification.payload.additionalData;
        Notification receivedData = new Gson().fromJson(jsonObject.toString(), Notification.class);
        String activityToBeOpened = "";
        if (!notification.isAppInFocus) {
            if (jsonObject != null) {
                activityToBeOpened = jsonObject.optString("activityToBeOpened", null);
                if (receivedData != null) {
                    Bundle bundle = new Bundle();
                    if (receivedData.getType().equalsIgnoreCase("idea")) {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, true);
                        bundle.putBoolean(Constants.IS_MSG_KEY, false);
                    } else {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, false);
                        bundle.putBoolean(Constants.IS_MSG_KEY, true);
                    }
                    bundle.putParcelable("Notification", receivedData);
                    bundle.putInt(Constants.POST_ID_KEY, receivedData.getPostId());
                    bundle.putString(Constants.TYPE_KEY, receivedData.getType());
                    EventBus.getDefault().post(new MessageEvent(true));

                }
            }
        } else {
            if (receivedData != null) {
                Bundle bundle = new Bundle();
                if (receivedData.getType().equalsIgnoreCase("idea")) {
                    bundle.putBoolean(Constants.IS_IDEA_KEY, true);
                    bundle.putBoolean(Constants.IS_MSG_KEY, false);
                } else {
                    bundle.putBoolean(Constants.IS_IDEA_KEY, false);
                    bundle.putBoolean(Constants.IS_MSG_KEY, true);
                }
                bundle.putParcelable("Notification", receivedData);
                bundle.putInt(Constants.POST_ID_KEY, receivedData.getPostId());
                bundle.putString(Constants.TYPE_KEY, receivedData.getType());
                ShowCustomeNotification(notification.payload.title, notification.payload.body, jsonObject.toString(), bundle);

            }

        }
        if (data != null) {
            //While sending a Push notification from OneSignal dashboard
            // you can send an addtional data named "customkey" and retrieve the value of it and do necessary operation
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
        }
    }

    public void ShowCustomeNotification(String title, String msg, String data, Bundle bundle) {
        EventBus.getDefault().post(new MessageEvent(true));
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(I_LaunchedApplication.getContext());

//                .setSound();
        Intent resultIntent = new Intent(I_LaunchedApplication.getContext(), HomeActivity.class);
        resultIntent.putExtra("activityToBeOpened", "group_invitation");
        resultIntent.putExtra("data", String.valueOf(data));
        resultIntent.putExtra("NotificationBundle", bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(I_LaunchedApplication.getContext());
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setLights(Color.BLUE, 500, 500);
        mBuilder.setDefaults(android.app.Notification.DEFAULT_VIBRATE);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(msg);
        mBuilder.setSound(alarmSound);
//        mBuilder.setStyle(new NotificationCompat.InboxStyle());
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) I_LaunchedApplication.getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(new BigInteger("222222", 16).intValue(), mBuilder.build());
    }
}