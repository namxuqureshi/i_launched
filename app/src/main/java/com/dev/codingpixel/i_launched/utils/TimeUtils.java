package com.dev.codingpixel.i_launched.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * TimeUtils
 *
 * @author <a href="http://www.trinea.cn" target="_blank">Trinea</a> 2013-8-24
 */
public class TimeUtils {

    public static final SimpleDateFormat DEFAULT_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static final SimpleDateFormat DATE_FORMAT_DATE = new SimpleDateFormat("yyyy-MM-dd");

    private TimeUtils() {
        throw new AssertionError();
    }

    /**
     * long time to string
     *
     * @param timeInMillis
     * @param dateFormat
     * @return
     */
    public static String getTime(long timeInMillis, SimpleDateFormat dateFormat) {
        return dateFormat.format(new Date(timeInMillis));
    }

    /**
     * long time to string, format is {@link #DEFAULT_DATE_FORMAT}
     *
     * @param timeInMillis
     * @return
     */
    public static String getTime(long timeInMillis) {
        return getTime(timeInMillis, DEFAULT_DATE_FORMAT);
    }

    public static String add6Month(String date_received) {
        Date myDate = null;
        try {
            myDate = DEFAULT_DATE_FORMAT.parse(date_received);
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.MONTH, 6);
            return getTime(cal.getTimeInMillis(), DEFAULT_DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }
    public static String addAMonth(String date_received) {
        Date myDate = null;
        try {
            myDate = DEFAULT_DATE_FORMAT.parse(date_received);
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.MONTH, 1);
            return getTime(cal.getTimeInMillis(), DEFAULT_DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }

    public static String addYear(String date_received) {
        Date myDate = null;
        try {
            myDate = DEFAULT_DATE_FORMAT.parse(date_received);
            Calendar cal = Calendar.getInstance();
            cal.setTime(myDate);
            cal.add(Calendar.MONTH, 12);
            return getTime(cal.getTimeInMillis(), DEFAULT_DATE_FORMAT);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }


    public static Date getTimeSort(long timeInMillis, SimpleDateFormat dateFormat) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeInMillis);
        Date date = null;
        try {
            date = dateFormat.parse(dateFormat.format(timeInMillis));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getTimeSort(String dateString, SimpleDateFormat dateFormat) {

        Date myDate = null;
        try {
            myDate = dateFormat.parse(dateString);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(myDate.getTime());
        Date date = null;
        try {
            date = dateFormat.parse(dateFormat.format(myDate.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * get current time in milliseconds
     *
     * @return
     */
    public static long getCurrentTimeInLong() {
        return System.currentTimeMillis();
    }

    /**
     * get current time in milliseconds, format is {@link #DEFAULT_DATE_FORMAT}
     *
     * @return
     */
    public static String getCurrentTimeInString() {
        return getTime(getCurrentTimeInLong());
    }

    /**
     * get current time in milliseconds
     *
     * @return
     */
    public static String getCurrentTimeInString(SimpleDateFormat dateFormat) {
        return getTime(getCurrentTimeInLong(), dateFormat);
    }


    public static String inDaysDifference(String expireAt) {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();
        Date expire;
        try {
            expire = DEFAULT_DATE_FORMAT.parse(expireAt);
            long diff = expire.getTime() - date.getTime();
            return String.valueOf(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";


    }


    public static boolean isOlder(String expireAt) {
        Calendar calendar = Calendar.getInstance();

        Date date = calendar.getTime();
        Date expire;
        try {

            expire = DEFAULT_DATE_FORMAT.parse(expireAt);
            return expire.compareTo(date) < 0;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }
}