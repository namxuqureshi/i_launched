package com.dev.codingpixel.i_launched.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by incubasyss on 01/03/2018.
 */

public class UserModelNew {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("user_status")
    @Expose
    private Integer userStatus;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("company_name")
    @Expose
    private Object companyName;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("about_us")
    @Expose
    private Object aboutUs;
    @SerializedName("profile_pic")
    @Expose
    private Object profilePic;
    @SerializedName("session_token")
    @Expose
    private String sessionToken;
    @SerializedName("device_type")
    @Expose
    private Object deviceType;
    @SerializedName("device_id")
    @Expose
    private Object deviceId;
    @SerializedName("email_token")
    @Expose
    private String emailToken;
    @SerializedName("token_generated")
    @Expose
    private String tokenGenerated;
    @SerializedName("actual_login")
    @Expose
    private String actualLogin;
    @SerializedName("last_login")
    @Expose
    private String lastLogin;
    @SerializedName("login_attempts")
    @Expose
    private Integer loginAttempts;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("post_ideas")
    @Expose
    private Integer postIdeas;
    @SerializedName("favorite_ideas")
    @Expose
    private Integer favoriteIdeas;
    @SerializedName("unread")
    @Expose
    private Integer unread;
    @SerializedName("get_subscription")
    @Expose
    private Object getSubscription;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserModelNew withId(Integer id) {
        this.id = id;
        return this;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public UserModelNew withType(Integer type) {
        this.type = type;
        return this;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public UserModelNew withUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserModelNew withUsername(String username) {
        this.username = username;
        return this;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public UserModelNew withCompanyName(Object companyName) {
        this.companyName = companyName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserModelNew withEmail(String email) {
        this.email = email;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserModelNew withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Object getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(Object aboutUs) {
        this.aboutUs = aboutUs;
    }

    public UserModelNew withAboutUs(Object aboutUs) {
        this.aboutUs = aboutUs;
        return this;
    }

    public Object getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(Object profilePic) {
        this.profilePic = profilePic;
    }

    public UserModelNew withProfilePic(Object profilePic) {
        this.profilePic = profilePic;
        return this;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public UserModelNew withSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
        return this;
    }

    public Object getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(Object deviceType) {
        this.deviceType = deviceType;
    }

    public UserModelNew withDeviceType(Object deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    public Object getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Object deviceId) {
        this.deviceId = deviceId;
    }

    public UserModelNew withDeviceId(Object deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public UserModelNew withEmailToken(String emailToken) {
        this.emailToken = emailToken;
        return this;
    }

    public String getTokenGenerated() {
        return tokenGenerated;
    }

    public void setTokenGenerated(String tokenGenerated) {
        this.tokenGenerated = tokenGenerated;
    }

    public UserModelNew withTokenGenerated(String tokenGenerated) {
        this.tokenGenerated = tokenGenerated;
        return this;
    }

    public String getActualLogin() {
        return actualLogin;
    }

    public void setActualLogin(String actualLogin) {
        this.actualLogin = actualLogin;
    }

    public UserModelNew withActualLogin(String actualLogin) {
        this.actualLogin = actualLogin;
        return this;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    public UserModelNew withLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public Integer getLoginAttempts() {
        return loginAttempts;
    }

    public void setLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
    }

    public UserModelNew withLoginAttempts(Integer loginAttempts) {
        this.loginAttempts = loginAttempts;
        return this;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public UserModelNew withCreatedAt(String createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserModelNew withUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Integer getPostIdeas() {
        return postIdeas;
    }

    public void setPostIdeas(Integer postIdeas) {
        this.postIdeas = postIdeas;
    }

    public UserModelNew withPostIdeas(Integer postIdeas) {
        this.postIdeas = postIdeas;
        return this;
    }

    public Integer getFavoriteIdeas() {
        return favoriteIdeas;
    }

    public void setFavoriteIdeas(Integer favoriteIdeas) {
        this.favoriteIdeas = favoriteIdeas;
    }

    public UserModelNew withFavoriteIdeas(Integer favoriteIdeas) {
        this.favoriteIdeas = favoriteIdeas;
        return this;
    }

    public Integer getUnread() {
        return unread;
    }

    public void setUnread(Integer unread) {
        this.unread = unread;
    }

    public UserModelNew withUnread(Integer unread) {
        this.unread = unread;
        return this;
    }

    public Object getGetSubscription() {
        return getSubscription;
    }

    public void setGetSubscription(Object getSubscription) {
        this.getSubscription = getSubscription;
    }

    public UserModelNew withGetSubscription(Object getSubscription) {
        this.getSubscription = getSubscription;
        return this;
    }

}
