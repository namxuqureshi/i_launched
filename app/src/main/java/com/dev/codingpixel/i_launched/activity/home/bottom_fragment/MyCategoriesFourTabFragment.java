package com.dev.codingpixel.i_launched.activity.home.bottom_fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.AllCategoriesRecyclerAdapter;
import com.dev.codingpixel.i_launched.adapters.AllCategoriesTabFourRecyclerAdapter;
import com.dev.codingpixel.i_launched.adapters.CategoryAutocompleteListAdapter;
import com.dev.codingpixel.i_launched.adapters.MyCategoriesRecyclerAdapter;
import com.dev.codingpixel.i_launched.adapters.MyCategoriesTabFourRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.models.AutoCompleteCategoryModel;
import com.dev.codingpixel.i_launched.models.MyCategoriesDataModel;
import com.dev.codingpixel.i_launched.models.MySubCategoriesDataModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.static_function.UIModification;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.Request.Method.GET;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.all_category;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.LOAD_SEARCH_CATE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyCategoriesFourTabFragment extends Fragment implements MyCategoriesTabFourRecyclerAdapter.ItemClickListener,  APIResponseListner, CategoryAutocompleteListAdapter.ItemClickListener, AllCategoriesTabFourRecyclerAdapter.ItemClickListener {

    public static final String TAG = "MyCategoriesFragment";
    private RecyclerView categories_fragment_recycler, all_categories_fragment_recycler;
    private BackInterface callBack;
    private ImageView not_found;
    private TopBarData topBarData;
    RelativeLayout search_categories;
    MyCategoriesTabFourRecyclerAdapter recyler_adapter;
    AllCategoriesTabFourRecyclerAdapter recyler_adapter_all;
    ArrayList<MyCategoriesDataModel> data = new ArrayList<>();
    ArrayList<AutoCompleteCategoryModel> dataSearch = new ArrayList<>();
    AutoCompleteTextView NameOrEmail;

    boolean isAll = false;

    public MyCategoriesFourTabFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            isAll = bundle.getBoolean(Constants.IS_ALL_KEY, false);
        }
        FindId(view);
        setTopView();
        Listener();
        if (isAll) {
            InitRecyclerViewAll(view);
            InitRecyclerViewMy(view);
            callForAllCategory();
        } else {
            InitRecyclerViewAll(view);
            InitRecyclerViewMy(view);
            callForAllCategory();
//            InitRecyclerViewMy(view);
//            callForMyCategory();
        }

    }

    /**
     * For company side getting all data and doing filtration offline
     */
    void callForAllCategory() {
        new VollyAPICall(getContext()
                , true
                , URL.all_category
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , MyCategoriesFourTabFragment.this, all_category);
    }

    /**
     * for user side getting list.
     */
    void callForMyCategory() {
        new VollyAPICall(getContext()
                , true
                , URL.my_category
                , new JSONObject()
                , userLoggedIn.getSessionToken()
                , GET
                , MyCategoriesFourTabFragment.this, APIActions.ApiActions.my_categories);
    }

    /**
     * Listener Set HERE
     */
    private void Listener() {
        NameOrEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isAll) {
                    if (s.length() > 0) {
                        all_categories_fragment_recycler.setVisibility(View.VISIBLE);
                        categories_fragment_recycler.setVisibility(View.GONE);
                    } else {
                        all_categories_fragment_recycler.setVisibility(View.GONE);
                        categories_fragment_recycler.setVisibility(View.VISIBLE);
                        recyler_adapter.filter(s.toString());
                    }
                    recyler_adapter_all.filter(s.toString());
                } else {
                    recyler_adapter.filter(s.toString());
                }

            }
        });
    }

    /**
     * Setting Top View
     */
    public void setTopView() {
        if (topBarData != null) {
            if (isAll) {
                topBarData.setData("All Categories", "Your new product and service ideas await! Search below.", true, new Bundle());
            } else {
                topBarData.setData("My Categories", "Your new product and service ideas await! Search below.", true, new Bundle());
            }

        }
    }

    /**
     * @param view setting and getting ID's
     */
    private void FindId(View view) {
        categories_fragment_recycler = view.findViewById(R.id.categories_fragment_recycler);
        search_categories = view.findViewById(R.id.search_categories);
        search_categories.setVisibility(View.VISIBLE);
        all_categories_fragment_recycler = view.findViewById(R.id.all_categories_fragment_recycler);
        NameOrEmail = view.findViewById(R.id.search_text_auto);


    }

    /**
     * @param view for user side and company side
     */
    public void InitRecyclerViewMy(final View view) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(view.getContext(), 2);
        categories_fragment_recycler.setLayoutManager(layoutManager);
        recyler_adapter = new MyCategoriesTabFourRecyclerAdapter(view.getContext(), data);
        recyler_adapter.setClickListener(MyCategoriesFourTabFragment.this);
        categories_fragment_recycler.setAdapter(recyler_adapter);
    }

    /**
     * @param view only for company side
     */
    public void InitRecyclerViewAll(final View view) {
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(view.getContext(), 2);
        all_categories_fragment_recycler.setLayoutManager(layoutManager);
        recyler_adapter_all = new AllCategoriesTabFourRecyclerAdapter(view.getContext(), dataSearch);
        recyler_adapter_all.setClickListener(MyCategoriesFourTabFragment.this);
        all_categories_fragment_recycler.setAdapter(recyler_adapter_all);
    }

    @Override
    public void onItemClick(View view, int position, MyCategoriesDataModel myCategoriesDataModel) {
        UIModification.HideKeyboard((Activity) view.getContext());
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.AUTO_COM_CAT_KEY, myCategoriesDataModel);
            bundle.putBoolean(Constants.IS_SEARCH_CAT_KEY, true);
            if (myCategoriesDataModel.isSubHas()) {
                bundle.putInt(Constants.ID_KEY, myCategoriesDataModel.getCategory_id());
            } else {
                bundle.putInt(Constants.ID_KEY, myCategoriesDataModel.getId());
            }
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                bundle.putInt(Constants.ID_KEY, myCategoriesDataModel.getId());
            }
            if (userLoggedIn.getType() != Constants.TYPE_USER) {
                topBarData.callTransaction(bundle, LOAD_SEARCH_CATE);
            }
        }
    }

    @Override
    public void onItemClick(View view, int position, AutoCompleteCategoryModel dataModelObject) {
        UIModification.HideKeyboard((Activity) view.getContext());
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable(Constants.AUTO_COM_CAT_KEY, dataModelObject);
            bundle.putBoolean(Constants.IS_SEARCH_CAT_KEY, true);
            if (dataModelObject.isSub()) {
                bundle.putInt(Constants.ID_KEY, dataModelObject.getCategorie_id());
            } else {
                bundle.putInt(Constants.ID_KEY, dataModelObject.getId());
            }
            if (userLoggedIn.getType() != Constants.TYPE_USER) {
                topBarData.callTransaction(bundle, LOAD_SEARCH_CATE);
            }

        }
    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }


    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == APIActions.ApiActions.my_categories) {
            try {
                JSONObject object = new JSONObject(response);
                dataSearch.clear();
                data.clear();
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        JSONArray jsonArray = object.getJSONArray("successData");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MyCategoriesDataModel e = new MyCategoriesDataModel();
                            AutoCompleteCategoryModel search;
                            if (jsonArray.getJSONObject(i).isNull("sub_cat_id")) {
                                e.setCategoryTitle(jsonArray.getJSONObject(i).getString("category_title"));
                                e.setCategoryId(-1);
                                e.setSubHas(false);
                            } else {
                                e.setCategoryTitle(jsonArray.getJSONObject(i).getString("sub_title"));
                                e.setCategoryId(jsonArray.getJSONObject(i).getInt("sub_cat_id"));
                                e.setSubHas(true);
                            }

                            e.setId(jsonArray.getJSONObject(i).getInt("category_id"));
                            search = new AutoCompleteCategoryModel(e);
                            dataSearch.add(search);
                            data.add(e);
                        }

                        recyler_adapter.notifyDataSetChanged();
                        NameOrEmail.setText("");
                        if (data.size() > 0) {
                        } else {
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (apiActions == all_category) {
            try {
                JSONObject object = new JSONObject(response);
                dataSearch.clear();
                data.clear();
                if (!object.isNull("status")) {
                    if (object.getString("status").equalsIgnoreCase("success")) {
                        JSONArray jsonArray = object.getJSONArray("successData");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            MyCategoriesDataModel e = new MyCategoriesDataModel();
                            e.setId(jsonArray.getJSONObject(i).getInt("id"));
                            e.setCreated_at(jsonArray.getJSONObject(i).getString("created_at"));
                            e.setUpdated_at(jsonArray.getJSONObject(i).getString("updated_at"));
                            e.setCategoryTitle(jsonArray.getJSONObject(i).getString("category_title"));
                            e.setSubHas(false);
                            List<MySubCategoriesDataModel> sub_category = new ArrayList<>();
                            JSONObject subArray = jsonArray.getJSONObject(i);
                            JSONArray inArray = subArray.getJSONArray("sub_category");
                            for (int inner = 0; inner < inArray.length(); inner++) {
                                MySubCategoriesDataModel eSub = new MySubCategoriesDataModel();
                                e.setSubHas(true);
                                eSub.setId(inArray.getJSONObject(inner).getInt("id"));
                                eSub.setCategorie_id(inArray.getJSONObject(inner).getInt("categorie_id"));
                                eSub.setCreated_at(inArray.getJSONObject(inner).getString("created_at"));
                                eSub.setUpdated_at(inArray.getJSONObject(inner).getString("updated_at"));
                                eSub.setCategory_title(inArray.getJSONObject(inner).getString("sub_title"));
                                sub_category.add(eSub);
                                dataSearch.add(new AutoCompleteCategoryModel(eSub, e.getCategory_title()));
                            }
                            e.setSub_category(sub_category);
                            data.add(e);
                            dataSearch.add(new AutoCompleteCategoryModel(e));
                        }
                        recyler_adapter_all.notifyDataSetChanged();
                        recyler_adapter.notifyDataSetChanged();
                        NameOrEmail.setText("");

                        if (data.size() > 0) {
                        } else {
                        }
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
//                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setContentText("Add Categories Here")
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCategroyTextClick(View view, int position, AutoCompleteCategoryModel categoryDataModel) {

    }
}
