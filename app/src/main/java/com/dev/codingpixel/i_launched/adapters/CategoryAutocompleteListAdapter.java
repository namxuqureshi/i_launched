package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.AutoCompleteCategoryModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryAutocompleteListAdapter extends ArrayAdapter<AutoCompleteCategoryModel> {
    private final Context mContext;
    private LayoutInflater mInflater;
    private final List<AutoCompleteCategoryModel> mCategoryDataModels;
    private final List<AutoCompleteCategoryModel> mCategoryDataModels_All;
    private final List<AutoCompleteCategoryModel> mCategoryDataModels_Suggestion;
    private final int mLayoutResourceId;
    int filter_type = 0;
    private ItemClickListener onTextClickListner;

    public interface ItemClickListener {
        void onCategroyTextClick(View view, int position, AutoCompleteCategoryModel categoryDataModel);
    }

    public CategoryAutocompleteListAdapter(Context context, int resource, List<AutoCompleteCategoryModel> categoryDataModels, ItemClickListener onTextClickListner, int filter_type) {
        super(context, resource, categoryDataModels);
        this.mInflater = LayoutInflater.from(getContext());
        this.mContext = context;
        this.filter_type = filter_type;
        this.mLayoutResourceId = resource;
        this.mCategoryDataModels = new ArrayList<>(categoryDataModels);
        this.mCategoryDataModels_All = new ArrayList<>(categoryDataModels);
        this.mCategoryDataModels_Suggestion = new ArrayList<>();
        this.onTextClickListner = onTextClickListner;
    }

    public int getCount() {
        return mCategoryDataModels.size();
    }

    public AutoCompleteCategoryModel getItem(int position) {
        return mCategoryDataModels.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            if (convertView == null) {
                convertView = this.mInflater.inflate(mLayoutResourceId, parent, false);
            }
            final AutoCompleteCategoryModel CategoryNewDataModel = getItem(position);
            TextView name = convertView.findViewById(R.id.bud_name);
            TextView parent_name = convertView.findViewById(R.id.parent_name);
            if (filter_type == 0) {
                name.setText(CategoryNewDataModel.getName());
            } else if (filter_type == 1) {
                name.setText(CategoryNewDataModel.getName());
            }
            if (CategoryNewDataModel.isSub()) {
                parent_name.setVisibility(View.VISIBLE);
                parent_name.setText(CategoryNewDataModel.getCategory_title());
            } else {
                parent_name.setVisibility(View.GONE);
            }

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (filter_type == 0) {
                        notifyDataSetChanged();
                        onTextClickListner.onCategroyTextClick(view, position, CategoryNewDataModel);
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            public String convertResultToString(Object resultValue) {
                if (filter_type == 0) {
                    return ((AutoCompleteCategoryModel) resultValue).getName();
                } else {
                    return ((AutoCompleteCategoryModel) resultValue).getName();
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    mCategoryDataModels_Suggestion.clear();
                    for (AutoCompleteCategoryModel autoCompleteCategoryModel : mCategoryDataModels_All) {
                        switch (filter_type) {
                            case 0:
                                if (autoCompleteCategoryModel.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                    mCategoryDataModels_Suggestion.add(autoCompleteCategoryModel);
                                }
                                break;
                            case 1:
                                if (autoCompleteCategoryModel.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                    mCategoryDataModels_Suggestion.add(autoCompleteCategoryModel);
                                }
                                break;
                            case 2:
                                if (autoCompleteCategoryModel.getName().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                                    mCategoryDataModels_Suggestion.add(autoCompleteCategoryModel);
                                }
                                break;
                        }

                    }
                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mCategoryDataModels_Suggestion;
                    filterResults.count = mCategoryDataModels_Suggestion.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mCategoryDataModels.clear();
                if (results != null && results.count > 0) {
                    // avoids unchecked cast warning when using mCategoryDataModels.addAll((ArrayList<GroupsInviteNewBudDataModel>) results.values);
                    List<?> result = (List<?>) results.values;
                    for (Object object : result) {
                        if (object instanceof AutoCompleteCategoryModel) {
                            mCategoryDataModels.add((AutoCompleteCategoryModel) object);
                        }
                    }
                } else if (constraint == null) {
                    // no filter, add entire original list back in
                    mCategoryDataModels.addAll(mCategoryDataModels_All);
                }
                notifyDataSetChanged();
            }
        };
    }
}