package com.dev.codingpixel.i_launched.interfaces;

/**
 * Created by incubasyss on 10/01/2018.
 */

public interface FragmentToActivityComm {
    public boolean checkValue();
}
