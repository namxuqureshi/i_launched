package com.dev.codingpixel.i_launched.activity.application;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import com.dev.codingpixel.i_launched.utils.SocketIO;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class Foreground implements Application.ActivityLifecycleCallbacks {

    private static Foreground instance;

    public static void init(Application app) {
        if (instance == null) {
            instance = new Foreground();
            app.registerActivityLifecycleCallbacks(instance);
        }
    }

    public static Foreground get() {
        return instance;
    }

    private Foreground() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Log.e("res", "reas");
    }

    @Override
    public void onActivityStarted(Activity activity) {
        Log.e("res", "reas");
    }

    @Override
    public void onActivityResumed(Activity activity) {

        Log.e("Resume Application", "false");
    }

    @Override
    public void onActivityPaused(Activity activity) {
        Log.d("res", "reas");
    }

    @Override
    public void onActivityStopped(Activity activity) {

        Log.e("Stoped Application", "true");
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Log.e("res", "reas");
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Log.e("Destroy Applicatio", "true");
    }


}
