package com.dev.codingpixel.i_launched.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by incubasyss on 29/01/2018.
 */

public class MyCategoriesDataModel implements Parcelable {
    private boolean isClicked;

    int id;
    private int user_id;
    private int category_id;
    private String category_title;
    String created_at;
    String updated_at;
    private List<MySubCategoriesDataModel> sub_category;
    private boolean isSubHas;

    public int getCategory_id() {
        return category_id;
    }

    public void setCategoryId(int category_id) {
        this.category_id = category_id;
    }

    public boolean isSubHas() {
        return isSubHas;
    }

    public void setSubHas(boolean subHas) {
        isSubHas = subHas;
    }

    public List<MySubCategoriesDataModel> getSub_category() {
        return sub_category;
    }

    public void setSub_category(List<MySubCategoriesDataModel> sub_category) {
        this.sub_category = sub_category;
    }

    public MyCategoriesDataModel() {
        isClicked = false;
    }

    public MyCategoriesDataModel(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setClicked(boolean clicked) {
        isClicked = clicked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCategory_title() {
        return category_title;
    }

    public void setCategoryTitle(String category_title) {
        this.category_title = category_title;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.isClicked ? (byte) 1 : (byte) 0);
        dest.writeInt(this.id);
        dest.writeInt(this.user_id);
        dest.writeInt(this.category_id);
        dest.writeString(this.category_title);
        dest.writeString(this.created_at);
        dest.writeString(this.updated_at);
        dest.writeList(this.sub_category);
        dest.writeByte(this.isSubHas ? (byte) 1 : (byte) 0);
    }

    protected MyCategoriesDataModel(Parcel in) {
        this.isClicked = in.readByte() != 0;
        this.id = in.readInt();
        this.user_id = in.readInt();
        this.category_id = in.readInt();
        this.category_title = in.readString();
        this.created_at = in.readString();
        this.updated_at = in.readString();
        this.sub_category = new ArrayList<MySubCategoriesDataModel>();
        in.readList(this.sub_category, MySubCategoriesDataModel.class.getClassLoader());
        this.isSubHas = in.readByte() != 0;
    }

    public static final Parcelable.Creator<MyCategoriesDataModel> CREATOR = new Parcelable.Creator<MyCategoriesDataModel>() {
        @Override
        public MyCategoriesDataModel createFromParcel(Parcel source) {
            return new MyCategoriesDataModel(source);
        }

        @Override
        public MyCategoriesDataModel[] newArray(int size) {
            return new MyCategoriesDataModel[size];
        }
    };
}
