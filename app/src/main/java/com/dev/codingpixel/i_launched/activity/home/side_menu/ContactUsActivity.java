package com.dev.codingpixel.i_launched.activity.home.side_menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.adapters.MsgChatRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.CustomToast;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.eventbus.MessageEvent;
import com.dev.codingpixel.i_launched.models.GetIdea;
import com.dev.codingpixel.i_launched.models.GetUser;
import com.dev.codingpixel.i_launched.models.MsgChatModel;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.IntentFunction;
import com.dev.codingpixel.i_launched.static_function.UIModification;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.android.volley.Request.Method.POST;
import static com.dev.codingpixel.i_launched.data_structure.APIActions.ApiActions.send_message;
import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.NOTIFICATION_RESULT_CODE;
import static com.dev.codingpixel.i_launched.static_function.StaticObjects.emailPattern;

public class ContactUsActivity extends AppCompatActivity implements APIResponseListner {

    RecyclerView message_chat_recycler;
    EditText message, subject, email, name;
    RelativeLayout btn_send;
    ImageView back_btn, home_notification;
    List<MsgChatModel> data = new ArrayList<>();
    MsgChatRecyclerAdapter recyler_adapter;
    LinearLayoutManager layoutManager;
    int threadId = 0, ideaId = 0;
    private GetIdea getIdea = new GetIdea();
    private GetUser getUser = new GetUser();
    private GetUser getUserOther = new GetUser();
    private String time = "";
    //TOP VAR
    TextView title_text, user_name, date_message, notify_counter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UIModification.FullScreenWithTransparentStatusBarChat(ContactUsActivity.this, "#c8171717");
        setContentView(R.layout.activity_contact_us);
        InitView();
        InitListener();

    }

    void callForSendMessage(JSONObject jsonObject) throws JSONException {
        new VollyAPICall(ContactUsActivity.this
                , true
                , URL.contact_us
                , jsonObject
                , userLoggedIn.getSessionToken()
                , POST
                , ContactUsActivity.this, send_message);
    }

    private void InitListener() {
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ContactUsActivity.super.onBackPressed();
            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkState()) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        /*1: name (required),
2: email (required)
3: subject (required)
4: message (required)*/
                        jsonObject.put("message", message.getText().toString());
                        jsonObject.put("name", name.getText().toString());
                        jsonObject.put("email", email.getText().toString());
                        jsonObject.put("subject", subject.getText().toString());
                        callForSendMessage(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        home_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                notify_counter.setVisibility(View.GONE);
                IntentFunction.GoToNotification(ContactUsActivity.this);
            }
        });

    }

    private boolean checkState() {
        if (name.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(ContactUsActivity.this, "Name Field is Required!!", Gravity.CENTER);
            return false;
        }
        if (email.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(ContactUsActivity.this, "Email Field is Required!!", Gravity.CENTER);
            return false;

        }
        if (email.getText().toString().trim().length() > 0) {
            if (!email.getText().toString().trim().matches(emailPattern)) {
                CustomToast.ShowCustomToast(ContactUsActivity.this, "Incorrect email format. Please provide a valid email address!!", Gravity.CENTER);
                return false;
            }
        }
        if (subject.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(ContactUsActivity.this, "Subject Field is Required!!", Gravity.CENTER);
            return false;
        }
        if (message.getText().toString().trim().length() == 0) {
            CustomToast.ShowCustomToast(ContactUsActivity.this, "Message Field is Required!!", Gravity.CENTER);
            return false;

        }


//        if (Notes.getText().toString().trim().length() == 0) {
//            CustomeToast.ShowCustomToast(AddMoreInfoAboutStrain.this, "Notes Field is Required!!", Gravity.CENTER);
//            return false;
//        }
        return true;
    }

    private void InitView() {
        back_btn = findViewById(R.id.back_btn);
        message = findViewById(R.id.message);
        subject = findViewById(R.id.subject);
        email = findViewById(R.id.email);
        name = findViewById(R.id.name);
        btn_send = findViewById(R.id.send);
        home_notification = findViewById(R.id.home_notification);
        notify_counter = findViewById(R.id.notify_counter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == NOTIFICATION_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras().getBundle("result");
                setResult(RESULT_OK, new Intent().putExtra("result", bundle));
                finish();
                //
            }
        }

    }


    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == send_message) {
            message.setText("");
            name.setText("");
            email.setText("");
            subject.setText("");
            Log.d("Tag", "onRequestSuccess: " + response);
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                    new SweetAlertDialog(this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("Success")
                            .setContentText(jsonObject.getString("successMessage").replace(",", "\n"))
                            .setConfirmText("Ok")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Setting Top Views Values
     */

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param event var for giving message to this activity
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        if (home_notification != null && notify_counter != null) {
            if (event.isNotify) {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell_blue));
                notify_counter.setVisibility(View.GONE);
//                userNotify.setUnread(userNotify.getUnread() + 1);
//                notify_counter.setText("" + userNotify.getUnread());
            } else {
                home_notification.setImageDrawable(getResources().getDrawable(R.drawable.ic_bell));
                notify_counter.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


}
