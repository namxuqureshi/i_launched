package com.dev.codingpixel.i_launched.onesignal;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;
import com.dev.codingpixel.i_launched.activity.splash.SplashActivity;
import com.dev.codingpixel.i_launched.models.Notification;
import com.dev.codingpixel.i_launched.models.UserModel;
import com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.Gson;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;

public class OneSignalNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String activityToBeOpened;
        userLoggedIn = SharedPrefrences.savePreference.get(Constants.USER_SESSION_KEY, UserModel.class, null);

        //While sending a Push notification from OneSignal dashboard
        // you can send an addtional data named "activityToBeOpened" and retrieve the value of it and do necessary operation
        //If key is "activityToBeOpened" and value is "AnotherActivity", then when a user clicks
        //on the notification, AnotherActivity will be opened.
        //Else, if we have not set any additional data MainActivity is opened.
        if (data != null) {
            Notification receivedData = new Gson().fromJson(data.toString(), Notification.class);
            activityToBeOpened = data.optString("activityToBeOpened", null);
            if (receivedData != null) {
                Log.i("OneSignalExample", "customkey set with value: " + activityToBeOpened);
                try {
                    Intent intent = new Intent(I_LaunchedApplication.getContext(), SplashActivity.class);
                    Bundle bundle = new Bundle();
                    if (receivedData.getType().equalsIgnoreCase("idea")) {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, true);
                        bundle.putBoolean(Constants.IS_MSG_KEY, false);
                    } else {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, false);
                        bundle.putBoolean(Constants.IS_MSG_KEY, true);
                    }
                    bundle.putParcelable("Notification", receivedData);
                    bundle.putInt(Constants.POST_ID_KEY, receivedData.getPostId());
                    bundle.putString(Constants.TYPE_KEY, receivedData.getType());
                    intent.putExtra("NotificationBundle", bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                    I_LaunchedApplication.getContext().startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }
}