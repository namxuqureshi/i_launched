package com.dev.codingpixel.i_launched.onesignal;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.application.I_LaunchedApplication;
import com.dev.codingpixel.i_launched.activity.home.HomeActivity;
import com.dev.codingpixel.i_launched.activity.splash.SplashActivity;
import com.dev.codingpixel.i_launched.eventbus.MessageEvent;
import com.dev.codingpixel.i_launched.models.Notification;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.google.gson.Gson;
import com.onesignal.NotificationExtenderService;
import com.onesignal.OSNotificationDisplayedResult;
import com.onesignal.OSNotificationReceivedResult;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.math.BigInteger;

public class OnSignalNotificationExtenderService extends NotificationExtenderService {
    @Override
    protected boolean onNotificationProcessing(final OSNotificationReceivedResult receivedResult) {

        final JSONObject jsonObject = receivedResult.payload.additionalData;
        Notification receivedData = new Gson().fromJson(jsonObject.toString(), Notification.class);
        String activityToBeOpened = "";
        if (receivedResult.isAppInFocus) {
            EventBus.getDefault().post(new MessageEvent(true));
            if (jsonObject != null) {
                activityToBeOpened = jsonObject.optString("activityToBeOpened", null);
                if (receivedData != null) {
                    Bundle bundle = new Bundle();
                    if (receivedData.getType().equalsIgnoreCase("idea")) {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, true);
                        bundle.putBoolean(Constants.IS_MSG_KEY, false);
                    } else {
                        bundle.putBoolean(Constants.IS_IDEA_KEY, false);
                        bundle.putBoolean(Constants.IS_MSG_KEY, true);
                    }
                    bundle.putParcelable("Notification", receivedData);
                    bundle.putInt(Constants.POST_ID_KEY, receivedData.getPostId());
                    bundle.putString(Constants.TYPE_KEY, receivedData.getType());
                }
            }
            return true;
        } else {
            if (receivedData != null) {
                final Bundle bundle = new Bundle();
                if (receivedData.getType().equalsIgnoreCase("idea")) {
                    bundle.putBoolean(Constants.IS_IDEA_KEY, true);
                    bundle.putBoolean(Constants.IS_MSG_KEY, false);
                } else {
                    bundle.putBoolean(Constants.IS_IDEA_KEY, false);
                    bundle.putBoolean(Constants.IS_MSG_KEY, true);
                }
                bundle.putParcelable("Notification", receivedData);
                bundle.putInt(Constants.POST_ID_KEY, receivedData.getPostId());
                bundle.putString(Constants.TYPE_KEY, receivedData.getType());
                ShowCustomeNotification(receivedResult.payload.title, receivedResult.payload.body, jsonObject.toString(), bundle);
//                OverrideSettings overrideSettings = new OverrideSettings();
//                overrideSettings.extender = new NotificationCompat.Extender() {
//                    @Override
//                    public NotificationCompat.Builder extend(NotificationCompat.Builder mBuilder) {
//                        // Sets the background notification color to Red on Android 5.0+ devices.
//                        Bitmap icon = BitmapFactory.decodeResource(I_LaunchedApplication.getContext().getResources(),
//                                R.mipmap.ic_launcher);
//                        mBuilder.setLargeIcon(icon);
//                        mBuilder.addExtras(bundle);
//                        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
//                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
//                        stackBuilder.addParentStack(HomeActivity.class);
//                        Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
//                        resultIntent.putExtra("activityToBeOpened", "group_invitation");
//                        resultIntent.putExtra("data", String.valueOf(jsonObject.toString()));
//                        resultIntent.putExtra("NotificationBundle", bundle);
//                        resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        stackBuilder.addNextIntent(resultIntent);
//                        PendingIntent resultPendingIntent =
//                                stackBuilder.getPendingIntent(
//                                        0,
//                                        PendingIntent.FLAG_UPDATE_CURRENT
//                                );
//                        mBuilder.setLights(Color.BLUE, 500, 500);
//                        mBuilder.setDefaults(android.app.Notification.DEFAULT_VIBRATE);
//                        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
//                        mBuilder.setAutoCancel(true);
//                        mBuilder.setContentTitle(receivedResult.payload.title);
//                        mBuilder.setContentText(receivedResult.payload.body);
//                        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//                        mBuilder.setSound(alarmSound);
////        mBuilder.setStyle(new NotificationCompat.InboxStyle());
//                        mBuilder.setContentIntent(resultPendingIntent);
//                        return mBuilder.setColor(new BigInteger("222222", 16).intValue());
//                    }
//                };
//                try {
//                    OSNotificationDisplayedResult displayedResult = displayNotification(overrideSettings);
////                Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);
//                } catch (IllegalAccessError e) {
//                    e.printStackTrace();
//                }
            }


////            Log.d("OneSignalExample", "Notification displayed with id: " + displayedResult.androidNotificationId);

            return false;
        }
    }

    public void ShowCustomeNotification(String title, String msg, String data, Bundle bundle) {
        EventBus.getDefault().post(new MessageEvent(true));
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext());

//                .setSound();
        Intent resultIntent = new Intent(this, HomeActivity.class);
        resultIntent.putExtra("activityToBeOpened", "group_invitation");
        resultIntent.putExtra("data", String.valueOf(data));
        resultIntent.putExtra("NotificationBundle", bundle);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setLights(Color.BLUE, 500, 500);
        mBuilder.setDefaults(android.app.Notification.DEFAULT_VIBRATE);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(title);
        mBuilder.setContentText(msg);
        mBuilder.setSound(alarmSound);
//        mBuilder.setStyle(new NotificationCompat.InboxStyle());
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(new BigInteger("222222", 16).intValue(), mBuilder.build());
    }
}
