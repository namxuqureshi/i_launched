package com.dev.codingpixel.i_launched.activity.home.side_menu;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.activity.dialogues.CancelSubscriptionDialouge;
import com.dev.codingpixel.i_launched.adapters.ProfileRecyclerAdapter;
import com.dev.codingpixel.i_launched.customui.customalerts.SweetAlertDialog;
import com.dev.codingpixel.i_launched.data_structure.APIActions;
import com.dev.codingpixel.i_launched.interfaces.BackInterface;
import com.dev.codingpixel.i_launched.interfaces.TopBarData;
import com.dev.codingpixel.i_launched.network.VollyAPICall;
import com.dev.codingpixel.i_launched.network.model.APIResponseListner;
import com.dev.codingpixel.i_launched.network.model.URL;
import com.dev.codingpixel.i_launched.static_function.Constants;
import com.dev.codingpixel.i_launched.utils.TimeUtils;
import com.dev.codingpixel.i_launched.utils.subscription.util.IabHelper;
import com.dev.codingpixel.i_launched.utils.subscription.util.IabResult;
import com.dev.codingpixel.i_launched.utils.subscription.util.Inventory;
import com.dev.codingpixel.i_launched.utils.subscription.util.Purchase;

import org.json.JSONException;
import org.json.JSONObject;

import static com.dev.codingpixel.i_launched.sharedprefrences.SharedPrefrences.userLoggedIn;
import static com.dev.codingpixel.i_launched.static_function.Constants.base64EncodedPublicKey;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdCompany;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdUser;
import static com.dev.codingpixel.i_launched.static_function.Constants.subscriptionIdUser_2;

/**
 * A simple {@link Fragment} subclass.
 */
public class MangeSubscriptionFragment extends Fragment implements ProfileRecyclerAdapter.ItemClickListener, CancelSubscriptionDialouge.OnDialogFragmentClickListener, APIResponseListner {
    public static final String TAG = "MenageSubscription";
    private RecyclerView profile_fragment_recycler;
    private BackInterface callBack;
    private ImageView not_found;
    TextView detail_lines, below_text, sub_name, sub_price, sub_remaining, sub_remaining_second, sub_name_second, sub_price_second;
    LinearLayout detail_view;
    LinearLayout second_option, first_option;
    RelativeLayout main, sub;
    private TopBarData topBarData;
    Button cancel_btn;
    boolean isSub = false;
    String expiry = "";
    RadioButton first_check, second_check;
    boolean isFirstOption = false, isSecondOption = false;


    ///SUBSCRIPTION


    // Does the user have an active subscription to the infinite gas plan?
    boolean mSubscribedToInfiniteGas = false;

    // SKU for our subscription (provide HERE for id which make in Google App Developer Console)

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;


    // The helper object
    IabHelper mHelper;


    public MangeSubscriptionFragment() {
        super();
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manager_subscription, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FindId(view);
        if (userLoggedIn.getUserStatus() == Constants.PAID_USER) {
            cancel_btn.setText(getString(R.string.cancel_value));
            isSub = true;
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                sub_remaining.setText(String.format("%s days remaining", TimeUtils.inDaysDifference(userLoggedIn.getGetSubscription().getExpireAt())));
                second_option.setVisibility(View.GONE);
            } else {
                if (userLoggedIn.getGetSubscription().getSubscriptionId().equalsIgnoreCase(subscriptionIdUser)) {
                    sub_remaining.setText(String.format("%s days remaining", TimeUtils.inDaysDifference(userLoggedIn.getGetSubscription().getExpireAt())));
                    first_option.setVisibility(View.GONE);
                    second_option.setVisibility(View.GONE);
                    first_check.setChecked(true);
                } else if (userLoggedIn.getGetSubscription().getSubscriptionId().equalsIgnoreCase(subscriptionIdUser_2)) {
                    sub_remaining_second.setText(String.format("%s days remaining", TimeUtils.inDaysDifference(userLoggedIn.getGetSubscription().getExpireAt())));
                    second_option.setVisibility(View.GONE);
                    first_option.setVisibility(View.GONE);
                    second_check.setChecked(true);
                } else if (userLoggedIn.getGetSubscription().getPackageName().equalsIgnoreCase("com.codingpixel.ilaunched_25_ideas.sub.allaccess")) {
                    sub_remaining.setText(String.format("%s days remaining", TimeUtils.inDaysDifference(userLoggedIn.getGetSubscription().getExpireAt())));
                    first_option.setVisibility(View.GONE);
                    second_option.setVisibility(View.GONE);
                    first_check.setChecked(true);
                } else {
                    sub_remaining_second.setText(String.format("%s days remaining", TimeUtils.inDaysDifference(userLoggedIn.getGetSubscription().getExpireAt())));
                    second_option.setVisibility(View.GONE);
                    first_option.setVisibility(View.GONE);
                    second_check.setChecked(true);
                }

            }

            if (TimeUtils.isOlder(userLoggedIn.getGetSubscription().getExpireAt())) {
                isSub = false;
                cancel_btn.setText(getString(R.string.buy_value));
            }

        } else {
            cancel_btn.setText(getString(R.string.buy_value));
            isSub = false;
        }
        ClickListener();
        setTopView();
        setSubscriptionDetail(view);

    }

    /**
     * @param v view for getting context
     *          Call For Subscription
     */
    private void setSubscriptionDetail(View v) {


        // Some sanity checks to see if the developer (that's you!) really followed the
        // instructions to run this sample (don't put these checks on your app!)
        try {
            if (base64EncodedPublicKey.contains("CONSTRUCT_YOUR")) {
                throw new RuntimeException("Please put your app's public key in MainActivity.java. See README.");
            }
//            if (v.getContext().getPackageName().startsWith("com.example")) {
//                throw new RuntimeException("Please change the sample's package name! See README.");
//            }
        } catch (RuntimeException e) {
            e.printStackTrace();

        }
        // Create the helper, passing it our context and the public key to verify signatures with
        Log.d(TAG, "Creating IAB helper.");
        mHelper = new IabHelper((AppCompatActivity) v.getContext(), base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set this to false).
        mHelper.enableDebugLogging(true);

        // Start setup. This is asynchronous and the specified listener
        // will be called once setup completes.
        Log.d(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) return;

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                Log.d(TAG, "Setup successful. Querying inventory.");
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mHelper == null) {
            mHelper = new IabHelper((AppCompatActivity) getContext(), base64EncodedPublicKey);
            mHelper.enableDebugLogging(true);
            mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
                public void onIabSetupFinished(IabResult result) {
                    Log.d(TAG, "Setup finished.");

                    if (!result.isSuccess()) {
                        // Oh noes, there was a problem.
                        complain("Problem setting up in-app billing: " + result);
                        return;
                    }

                    // Have we been disposed of in the meantime? If so, quit.
                    if (mHelper == null) return;

                    // IAB is fully set up. Now, let's get an inventory of stuff we own.
                    Log.d(TAG, "Setup successful. Querying inventory.");
                    mHelper.queryInventoryAsync(mGotInventoryListener);
                }
            });
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // very important:
        Log.d(TAG, "Destroying helper.");
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

    /**
     * @param object params for saving subscribe detail
     *               FUNC FOR SAVING DATA TO SERVER FOR FURTHER USE
     */
    private void callAddSubscriptionData(JSONObject object) {
        new VollyAPICall(getContext()
                , true
                , URL.add_subscriptin
                , object
                , userLoggedIn.getSessionToken()
                , Request.Method.POST
                , MangeSubscriptionFragment.this
                , APIActions.ApiActions.add_subscription_data);
    }


    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                Purchase infiniteGasPurchase = inventory.getPurchase(subscriptionIdCompany);
                mSubscribedToInfiniteGas = (infiniteGasPurchase != null &&
                        verifyDeveloperPayload(infiniteGasPurchase));
            } else if (userLoggedIn.getType() == Constants.TYPE_USER) {
                Purchase infiniteGasPurchase = inventory.getPurchase(subscriptionIdUser);
                mSubscribedToInfiniteGas = (infiniteGasPurchase != null &&
                        verifyDeveloperPayload(infiniteGasPurchase));
                Purchase infiniteGasPurchase_2 = inventory.getPurchase(subscriptionIdUser_2);
                mSubscribedToInfiniteGas = (infiniteGasPurchase_2 != null &&
                        verifyDeveloperPayload(infiniteGasPurchase_2));
            }

            Log.d(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;
//            updateUi();
            if (result.isFailure()) {
                complain("Error purchasing: " + result);

                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");

                return;
            }

            Log.d(TAG, "Purchase successful.");
            if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                if (purchase.getSku().equals(subscriptionIdCompany)) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("subscription_id", subscriptionIdCompany);
                        jsonObject.put("package_name", purchase.getPackageName());
                        jsonObject.put("activated_at", TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("expire_at", TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime())));
                        expiry = TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("token", purchase.getToken());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callAddSubscriptionData(jsonObject);
                    alert("Thank you for subscribing to Idea Subscription!", "Success!!");


                }
            } else if (userLoggedIn.getType() == Constants.TYPE_USER) {
                if (purchase.getSku().equals(subscriptionIdUser)) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("subscription_id", subscriptionIdUser);
                        jsonObject.put("package_name", purchase.getPackageName());
                        jsonObject.put("activated_at", TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("expire_at", TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime())));
                        expiry = TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("token", purchase.getToken());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callAddSubscriptionData(jsonObject);
                    alert("Thank you for subscribing to Idea Subscription!", "Success!!");


                } else if (purchase.getSku().equals(subscriptionIdUser_2)) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("subscription_id", subscriptionIdUser_2);
                        jsonObject.put("package_name", purchase.getPackageName());
                        jsonObject.put("activated_at", TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("expire_at", TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime())));
                        expiry = TimeUtils.addAMonth(TimeUtils.getTime(purchase.getPurchaseTime()));
                        jsonObject.put("token", purchase.getToken());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    callAddSubscriptionData(jsonObject);
                    alert("Thank you for subscribing to Idea Subscription!", "Success!!");


                }
            }
        }
    };


    private void updateUi() {
        isSub = true;
        cancel_btn.setText(getString(R.string.cancel_value));

    }

    void complain(String message) {
        Log.e(TAG, "**** Subscription Error: " + message);
//        alert("Error: " + "Subscription Failed!!", "Alert!!");
    }

    /**
     * @param message text for display in alert view
     *                Dialogue for Alert
     */
    void alert(String message) {
        new SweetAlertDialog(getContext())
                .setTitleText("Alert!")
                .setContentText(message)
                .show();
    }

    /**
     * @param message text to display
     * @param title   title text to display
     */
    void alert(String message, String title) {
        new SweetAlertDialog(getContext())
                .setTitleText(title)
                .setContentText(message)
                .show();

    }

    /**
     * Setting Top Views
     */
    public void setTopView() {
        if (topBarData != null) {
            Bundle bundle = new Bundle();
            bundle.putBoolean(Constants.IS_BACK_KEY, true);
            bundle.putBoolean(Constants.IS_TAG_KEY, true);
            bundle.putString(Constants.TAG_KEY, TAG);
            topBarData.setData("Manage Subscription"
                    , "Thank you for subscribing."
                    , true
                    , bundle);
        }
        if (isSub) {
            cancel_btn.setText(getString(R.string.cancel_value));
            sub_remaining.setVisibility(View.VISIBLE);
        } else {
            cancel_btn.setText(getString(R.string.buy_value));
            sub_remaining.setVisibility(View.VISIBLE);
            sub_remaining.setText("per month");
            sub_remaining_second.setText("12 month");
        }

    }

    /**
     * Click Listeners
     */
    private void ClickListener() {
        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cancel_btn.getText().toString().trim().equalsIgnoreCase(getString(R.string.cancel_value))) {
//                    cancel_btn.setText(getString(R.string.buy_value));
                    //SET CALL BACK FOR R.string.cancel_value AND UPDATE DB CALL FOR UNPAID USER

                    CancelSubscriptionDialouge cancelSubscriptionDialouge = CancelSubscriptionDialouge.newInstance(MangeSubscriptionFragment.this);
                    cancelSubscriptionDialouge.show(getChildFragmentManager(), "pd");

                } else {
                    //SET CALL BACK PURCHASE SUBSCRIPTION AND UPDATE DB CALL FOR PAID USER
                    if (!mHelper.subscriptionsSupported()) {
                        complain("Subscriptions not supported on your device yet. Sorry!");
                        return;
                    }

        /*
         *        verifyDeveloperPayload() for more info. Since this is a SAMPLE, we just use
         *        an empty string, but on a production app you should carefully generate this. */
                    String payload = "";
                    Log.d(TAG, "Launching purchase flow for infinite gas subscription.");
                    if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
                        mHelper.launchPurchaseFlow((AppCompatActivity) v.getContext(),
                                subscriptionIdCompany, IabHelper.ITEM_TYPE_SUBS,
                                RC_REQUEST, mPurchaseFinishedListener, payload);
                    } else if (userLoggedIn.getType() == Constants.TYPE_USER) {
                        mHelper.launchPurchaseFlow((AppCompatActivity) v.getContext(),
                                subscriptionIdUser, IabHelper.ITEM_TYPE_SUBS,
                                RC_REQUEST, mPurchaseFinishedListener, payload);
//                        if (isFirstOption) {
//                            mHelper.launchPurchaseFlow((AppCompatActivity) v.getContext(),
//                                    subscriptionIdUser, IabHelper.ITEM_TYPE_SUBS,
//                                    RC_REQUEST, mPurchaseFinishedListener, payload);
//                        } else if (isSecondOption) {
//                            mHelper.launchPurchaseFlow((AppCompatActivity) v.getContext(),
//                                    subscriptionIdUser_2, IabHelper.ITEM_TYPE_SUBS,
//                                    RC_REQUEST, mPurchaseFinishedListener, payload);
//                        } else {
//                            CustomToast.ShowCustomToast(v.getContext(), "Please Select Option First!", Gravity.CENTER);
//                        }

                    }
                    //Call in-App Here Functionality
                }
//                setTopView();
            }
        });
//        *The ideas that are i-Launched wont be counted in this \n*The subscription renews each month


    }

    /**
     * @param view for getting and setting var ID"s
     */
    private void FindId(View view) {
        detail_lines = view.findViewById(R.id.detail_lines);
        below_text = view.findViewById(R.id.below_text);
        cancel_btn = view.findViewById(R.id.cancel_btn);
        detail_view = view.findViewById(R.id.detail_view);
        second_option = view.findViewById(R.id.second_option);
        first_option = view.findViewById(R.id.first_option);
        main = view.findViewById(R.id.main);
        sub = view.findViewById(R.id.sub);
        sub_name = view.findViewById(R.id.sub_name);
        sub_name_second = view.findViewById(R.id.sub_name_second);
        sub_price = view.findViewById(R.id.sub_price);
        sub_price_second = view.findViewById(R.id.sub_price_second);
        sub_remaining = view.findViewById(R.id.sub_remaining);
        sub_remaining_second = view.findViewById(R.id.sub_remaining_second);
        first_check = view.findViewById(R.id.first_check);
        second_check = view.findViewById(R.id.second_check);
        first_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() != Constants.PAID_USER) {
                    isSecondOption = false;
                    isFirstOption = true;
                    second_check.setChecked(false);
                }
            }
        });
        second_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() != Constants.PAID_USER) {
                    isSecondOption = true;
                    isFirstOption = false;
                    first_check.setChecked(false);
                }
            }
        });
        first_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() != Constants.PAID_USER) {
                    isSecondOption = false;
                    isFirstOption = true;
                    first_check.setChecked(true);
                    second_check.setChecked(false);
                }
            }
        });
        second_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userLoggedIn.getUserStatus() != Constants.PAID_USER) {
                    isSecondOption = true;
                    isFirstOption = false;
                    first_check.setChecked(false);
                    second_check.setChecked(true);
                }
            }
        });

        if (userLoggedIn.getType() == Constants.TYPE_COMPANY) {
            sub_price.setText("$29.99");
            first_check.setVisibility(View.GONE);
            sub_name.setText("Unlimited Ideas");
//            Subscription will renew at the same subscription option currently selected
            detail_lines.setText(Html.fromHtml("<font color=#1fbced>*</font>The monthly subscription will auto-renew unless cancelled. If cancelled, the monthly fee is not prorated."));
        } else {
            sub_name.setText("25 Ideas Allowed");
            second_option.setVisibility(View.GONE);
            first_check.setVisibility(View.GONE);
            sub_price.setText("$2.99");
//            detail_lines.setText(Html.fromHtml("<font color=#1fbced>*</font>The ideas that are i-Launched wont be counted in this<br><font color=#1fbced>*</font>The subscription renews each month"));
            detail_lines.setText(Html.fromHtml("<font color=#1fbced>*</font>The monthly subscription will auto-renew unless cancelled. If cancelled, the monthly fee is not prorated."));
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    @Override
    public void onItemClick(View view, int position) {

    }

    public void SetTopData(TopBarData topBarData) {
        this.topBarData = topBarData;
    }

    public void SetBackListener(BackInterface homeActivity) {
        this.callBack = homeActivity;
    }

    @Override
    public void onConfirmButtonClick(CancelSubscriptionDialouge dialog) {
        dialog.dismiss();
        topBarData.pressedBackButtonFromFragment();
        isSub = false;


    }

    @Override
    public void onRequestSuccess(String response, APIActions.ApiActions apiActions) {
        if (apiActions == APIActions.ApiActions.add_subscription_data) {
            isSub = true;
            setTopView();
            if (!expiry.equalsIgnoreCase("")) {
                sub_remaining.setText(TimeUtils.inDaysDifference(expiry));
            }
            topBarData.callStatusChanged(Constants.PAID_USER);
//            topBarData.pressedBackButtonFromFragment();
        }
    }

    @Override
    public void onRequestError(String response, APIActions.ApiActions apiActions) {
        try {
            JSONObject jsonObject = null;
            jsonObject = new JSONObject(response);
            if (jsonObject.getString("status").equalsIgnoreCase("error")) {
                new SweetAlertDialog(getContext(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Error!!")
                        .setContentText(jsonObject.getString("errorMessage").replace(",", "\n"))
                        .setConfirmText("Ok")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .show();
//                CustomToast.ShowCustomToast(LoginActivity.this, jsonObject.getString("errorMessage").replace(",", "\n"), Gravity.CENTER);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
