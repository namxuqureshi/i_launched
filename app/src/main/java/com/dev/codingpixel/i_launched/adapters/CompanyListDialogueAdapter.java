package com.dev.codingpixel.i_launched.adapters;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.codingpixel.i_launched.R;
import com.dev.codingpixel.i_launched.models.CompanyDataModel;

import java.util.ArrayList;

/**
 * Created by incubasyss on 23/01/2018.
 */

public class CompanyListDialogueAdapter extends RecyclerView.Adapter<CompanyListDialogueAdapter.ViewHolder> {
    private LayoutInflater mInflater;
    ArrayList<CompanyDataModel> mData = new ArrayList<>();
    private CompanyListDialogueAdapter.ItemClickListener mClickListener;

    public CompanyListDialogueAdapter(Context context, ArrayList<CompanyDataModel> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    // inflates the cell layout from xml when needed
    @Override
    public CompanyListDialogueAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.company_recyler_item, parent, false);
        RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) view.getLayoutParams();
        lp.height = parent.getMeasuredHeight() / 3;
        view.setLayoutParams(lp);
        CompanyListDialogueAdapter.ViewHolder viewHolder = new CompanyListDialogueAdapter.ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final CompanyListDialogueAdapter.ViewHolder holder, final int position) {
        final CompanyDataModel temp = mData.get(position);
        if (temp.isTick()) {
            holder.select_company.setImageDrawable(holder.select_company.getContext().getResources().getDrawable(R.drawable.ic_tick_login_signup));
        } else {
            holder.select_company.setImageDrawable(null);
        }
        holder.company_name.setText(mData.get(position).getName());
        holder.select_company_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (temp.isTick()) {
                    temp.setTick(false);
                    mData.get(position).setTick(false);
                    holder.select_company.setImageDrawable(null);
                } else {
                    temp.setTick(true);
                    mData.get(position).setTick(true);
                    holder.select_company.setImageDrawable(holder.select_company.getContext().getResources().getDrawable(R.drawable.ic_tick_login_signup));
                }
                unCheckOther(position, false);
                if (mClickListener != null) mClickListener.onItemClick(v, position);
                notifyDataSetChanged();
            }
        });

    }

    private void unCheckOther(int position, boolean state) {
        for (int i = 0; i < mData.size(); i++) {
            if (position != i) {
                mData.get(i).setTick(state);
            }
        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return this.mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnTouchListener {
        TextView company_name;
        RelativeLayout select_company_check;
        ImageView select_company;

        public ViewHolder(View itemView) {
            super(itemView);
            select_company_check = itemView.findViewById(R.id.select_company_check);
            select_company = itemView.findViewById(R.id.select_company);
            company_name = itemView.findViewById(R.id.company_name);
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }

        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    break;
            }
            return false;
        }
    }

    // allows clicks events to be caught
    public void setClickListener(CompanyListDialogueAdapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}